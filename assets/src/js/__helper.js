;
var helper = {};


/*
  "Безопасный" запуск callback.
*/

helper.runCallback = function(callback) {
  if (typeof callback === 'function') {
    callback.apply(callback, Array.prototype.slice.call(arguments, 1));
  }
}


/*
  Экранирование символов
*/

helper.escapeHtml = function(text) {
  var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };

  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}


/*
  Определение браузера
*/

helper.detectBrowser = function(){
  var ua = navigator.userAgent, tem,
  M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];

  if (/trident/i.test(M[1])) {
    tem =  /\brv[ :]+(\d+)/g.exec(ua) || [];

    return 'Internet Explorer ' + (tem[1] || '');
  }

  if (M[1] === 'Chrome') {
    tem = ua.match(/\b(OPR|Edge)\/(\d+)/);

    if (tem != null) {
      return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
  }

  M = M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];

  if ((tem = ua.match(/version\/(\d+)/i)) != null) {
    M.splice(1, 1, tem[1]);
  }

  return M.join(' ');
}


/*
  Загрузка картинок
*/

helper.isImageLoaded = function(elImg) {
  return !(!elImg.complete || (typeof elImg.naturalWidth !== "undefined" && elImg.naturalWidth === 0));
}

helper.loadImage = function(src, callback) {
  var elImg;

  if (src instanceof Element) {
    if (src.tagName.toLowerCase() === 'img') {
      elImg = src;
    }
    else {
      var backgroundImage = this.style.backgroundImage;
      if (! backgroundImage) return;

      src = backgroundImage.slice(4, -1).replace(/"/g, "");
    }
  }

  if (typeof src === 'string') {
    elImg = document.createElement('img');
    elImg.src = src;
  }

  if (elImg) {
    var handler = function() {
      callback();
      this.removeEventListener('load', handler);
    }

    elImg.addEventListener('load', handler);

    if (helper.isImageLoaded(elImg)) {
      callback();
    }
  }
}
;