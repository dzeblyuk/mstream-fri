;
function animate(options) {
  var start = performance.now();
  requestAnimationFrame(function animate(time) {
    var timeFraction = (time - start) / options.duration;
    if (timeFraction > 1) timeFraction = 1;

    var progress = options.timing(timeFraction);
    options.draw(progress);

    if (timeFraction < 1) {
      requestAnimationFrame(animate);
    }
    else {
      helper.runCallback(options.callback);
    }
  });
}

var zoomOut = function(options) {
  animate({
    duration: options.duration || 300,
    timing: function(timeFraction) {
      return (1 - Math.cos(timeFraction * Math.PI)) / 2;
    },
    draw: function(progress) {
      var transform = 'scale(' + (1 - progress * 0.1).toFixed(4) + ')';
      options.el.style.transform = transform;
      options.el.style.webkitTransform = transform;
    },
    callback: options.callback
  });
}



var zoomIn = function(options) {
  animate({
    duration: options.duration || 300,
    timing: function(timeFraction) {
      return (1 - Math.cos(timeFraction * Math.PI)) / 2;
    },
    draw: function(progress) {
      var transform = 'scale(' + (0.9 + progress * 0.1).toFixed(4) + ')';
      options.el.style.transform = transform;
      options.el.style.webkitTransform = transform;
    },
    callback: options.callback
  });
}


var rotator = function(el, defaultConfig) {
  var __swiper;
  var axis = 'horizontal';

  var init = function(direction, callback) {
    axis = direction || 'horizontal';

    var swiper = getSwiper();

    var config = Object.assign({}, defaultConfig, {
      direction: axis,
      mousewheel: false,
      keyboard: false,
      allowTouchMove: false,
      init: false,
      initialSlide: swiper ? swiper.activeIndex : 0
    });

    swiper = new Swiper(el, config);
    swiper.once('init', function() {
      setSwiper(swiper);
      setTimeout(function() {
        helper.runCallback(callback);
      }, 50)
    });

    swiper.init();
  }

  var getSwiper = function() {
    return __swiper;
  }

  var setSwiper = function(swiper) {
    __swiper = swiper
  }

  var destroy = function(callback) {
    var swiper = getSwiper();

    swiper.once('destroy', callback);
    swiper.destroy(false, true);
  }

  var rotate = function(changeMethod, callback) {
    var swiper = getSwiper();

    swiper.once('slideChangeTransitionEnd', callback);

    swiper[changeMethod](500);
  }

  init();

  return {
    rotate: function (direction, callback) {
      var newAxis, changeMethod = 'slidePrev';

      switch(direction) {
        case 'top':
          newAxis = 'vertical';
          changeMethod = 'slidePrev';
          break;

        case 'right':
          newAxis = 'horizontal';
          changeMethod = 'slideNext';
          break;

        case 'bottom':
          newAxis = 'vertical';
          changeMethod = 'slideNext';
          break;

        default:
        case 'left':
          newAxis = 'horizontal';
          changeMethod = 'slidePrev';
          break;
      }

      var doRotate = function() {
        rotate(changeMethod, callback);
      }

      if (newAxis != axis) {
        destroy(function(activeIndex) {
          init(newAxis, doRotate, activeIndex);
        });
      }
      else {
        doRotate();
      }
    },

    addSlide: function(page, position, callback) {
      var appendMethod;
      var slide = makeEl('div', tpl__rotatorSlide());
      slide.appendChild(page);

      switch(position) {
        case 'top':
        case 'left':
          appendMethod = 'prependSlide';
          break;
        case 'right':
        case 'bottom':
          appendMethod = 'appendSlide';
          break;
      }

      var swiper = getSwiper();
      swiper[appendMethod](slide);

      swiper.once('update', callback);

      swiper.update();
    },

    remove: function(page, callback) {
      callback = callback || function(){};
      var swiper = getSwiper();


      for (var i = 0; i < swiper.slides.length; i++) {
        if (swiper.slides[i] === page.parentNode) {
          swiper.removeSlide(i);
          swiper.once('update', callback);
          swiper.update();
          break;
        }
      }
    }
  }
}
;