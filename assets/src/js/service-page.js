;
var servicesPageController = function (data, core) {
  var page;
  var swiperHandler;

  var init = function(p) {
    page = page || p;
    if (! page) return;

    var links = page.querySelectorAll('.js-inner-link');

    swiperHandler = new swiperController(page.querySelector('.js-page-cube'));
    loadImages();
    initOrderBtns();
    inputFillCheck('.js-input');
    core.initInnerLinks(page);
    core.menu.initMenuBtn(page);
  }


  /*
    Загрузка изображений.
  */

  var loadImages = function() {
    [].forEach.call(page.querySelectorAll('.js-image-loader'), function(el) {
      if (el.classList.contains('is-loaded')) return;
      var imageSrc = el.getAttribute('data-src');
      helper.loadImage(imageSrc, function() {
        el.src = imageSrc;
        el.classList.add('is-loaded');
        el.removeAttribute('data-src');
      });
    });
  }


  /*
    Переключение между блоками страницы.
  */

  var swiperController = function(el) {
    var swiper = false;
    var timeout;
    var breakpoint = 768;

    var init = function() {
      if (swiper) return;

      swiper = new Swiper(el, core.swiperConfig());
      el.classList.add('is-active');
      core.initInnerLinks(page);
    }

    var destroy = function() {
      if (!swiper) return;

      swiper.destroy(true, true);
      el.classList.remove('is-active');
      swiper = false;
    }

    var checkInitIsNeed = function() {
      (window.innerWidth < breakpoint  ? destroy() : init());
    }

    var onResize = function() {
      timeout = setTimeout(function(){
        checkInitIsNeed();
      }, 50);
    }

    window.addEventListener('resize', onResize);
    checkInitIsNeed();

    return {
      destroy: function() {
        window.removeEventListener('resize', onResize);
        clearTimeout(timeout);
        destroy();
      },
      isOn: function() {
        return !! swiper;
      },
      moveTo: function(el) {
        if (!swiper) return;
        var founded = false;

        for (var i = 0; i < swiper.slides.length; i++) {
          if (swiper.slides[i] === el) {
            founded = true;
            break;
          }
        }

        if (founded) {
          swiper.slideTo(i, 700);
        }
      }
    }
  }


  var initOrderBtns = function() {
    var elContacts = page.querySelector('.js-contacts');
    var elInput = page.querySelector('.js-service-id');

    [].forEach.call(page.querySelectorAll('.js-order-btn'), function(el) {
      el.addEventListener('click', function() {
        if (!page) return;
        elInput.value = el.getAttribute('data-id');

        swiperHandler.isOn() ?
          swiperHandler.moveTo(elContacts) :
          scrollToElem(elContacts);
      });
    });
  }

  var scrollToElem = function(el) {
    var scrollContainer = page.querySelector('.js-scroll-container');
    var start = scrollContainer.scrollTop;
    var fin = findObjectPosition(el);
    var diff = Math.abs(fin - start - 60);

    animate({
      duration: 700,
      timing: function(timeFraction) {
        return (1 - Math.cos(timeFraction * Math.PI)) / 2;
      },
      draw: function(progress) {
        scrollContainer.scrollTop = start + diff * progress;
      }
    });
  }


  /*
    Разрушение страницы.
  */

  var destroyPage = function(callback) {
    swiperHandler.destroy();
    helper.runCallback(callback);
  }


  /*
    Делаем ссылку красивой.
  */

  function prettyLink(link) {
    link = link.replace('http://', '').replace('/', '');
    return link.charAt(0).toUpperCase() + link.slice(1);
  }


  /*
    Формирование страницы.
  */

  var makePage = function() {
    data.rows.forEach(function(item) {
      if (!item.link) return;
      item.prettyLink = prettyLink(item.link);
    });

    page = makeEl('div', tpl__servicePage(data, core.getMenu()));

    return page;
  }

  return {
    init: init,
    ready: function(){},
    getPageData: function() {
      return data;
    },
    getPageEl: function() {
      return page;
    },
    makePage: makePage,
    destroyPage: destroyPage,
  }
}
;