;
var homepageController = function(data, core) {
  var page;

  var currentId;
  var canLoadImages = true;
  var images = [];
  var elColorChanger;


  /*
    Формирование функции смены слайда.
  */

  var slideChanger = function(item) {
    return function() {
      currentId = item.id;
      changeLetterColor(item.color);

      helper.loadImage(item.image, function() {
        if (item.id === currentId) {
          page.style.backgroundImage = 'url(' + item.image + ')';
        }
      });
    }
  }


  /*
    Изменение цвета буквы.
  */

  var initColorChanger = function() {
    elColorChanger = page.querySelector('.js-color-changer');
    var elSiteName = page.querySelector('.js-site-name');
    if (!elSiteName) return;

    elColorChanger = document.createElement('span');
    elColorChanger.innerText = elSiteName.innerText.slice(0, 1);
    elSiteName.innerText = elSiteName.innerText.slice(1);

    elSiteName.insertBefore(elColorChanger, elSiteName.firstChild);
  }

  var changeLetterColor = function(color) {
    if (elColorChanger) {
      elColorChanger.style.color = color;
    }
  }


  /*
    Инициализация слайдера
  */

  var initPortfolioSlider = function(page) {
    canLoadImages = true;

    var sliderControls = page.querySelectorAll('.js-bg-item');

    [].forEach.call(sliderControls, function(el) {
      var id = el.getAttribute('data-id');

      var item = data.menu.find(function(item) {
        return item.id == id;
      });

      var onClick = slideChanger(item);

      el.addEventListener('mouseover', function(e) {
        onClick();

        [].forEach.call(sliderControls, function(ele) {
          ele.classList.remove('is-active');
        });

        el.classList.add('is-active');
      });

      addImageToChain(item.image);
    });

    loadImagesChain();
  }


  /*
    Дозагрузка картинок слайдера.
  */

  var addImageToChain = function(src) {
    var imageNotExist = ! images.find(function(item) {
      return item.src === src;
    });

    if (imageNotExist) {
      images.push({
        loaded: false,
        src: src,
      });
    }
  }

  var loadImagesChain = function() {
    if (!canLoadImages) return;

    var image = images.find(function(item){
      return !item.loaded;
    });

    if (image) {
      var elImg = document.createElement('img');
      elImg.src = image.src;

      helper.loadImage(elImg, function() {
        image.loaded = 1;
        image.el = elImg;
        loadImagesChain();
      });
    }
  }


  /*
    Красивое появление ссылок на другие страницы
  */

  var initServicesNav = function() {
    var elServiceNav = page.querySelector('.js-services-nav');

    if (elServiceNav) {
      elServiceNav.classList.add('is-ready');
    }
  }


  /*
    Инициализация домашней страницы
  */

  var init = function(p) {
    page = page || p;

    initColorChanger(page);
    initPortfolioSlider(page);
    core.initInnerLinks(page);
    core.menu.initMenuBtn(page);
  }


  /*
    Вызывается как только страница должна быть
  */

  var ready = function() {
    initServicesNav(page);
  }


  /*
    Создание страницы
  */

  var makePage = function() {
    page = makeEl('div', tpl__homepage(data, core.getMenu()));

    return page;
  }


  /*
    Разрушение страницы
  */

  var destroyPage = function(callback) {
    canLoadImages = false;

    helper.runCallback(callback);
  }

  return {
    init: init,
    ready: ready,
    getPageData: function() {
      return data;
    },
    getPageEl: function() {
      return page;
    },
    makePage: makePage,
    destroyPage: destroyPage,
  }
}
;