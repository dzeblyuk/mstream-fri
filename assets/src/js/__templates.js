;
/*
  Страница услуг.
*/

var tpl__servicePage = function(data, menu) {
  return (
    '<article class="services-page js-page" style="color: ' + data.color + '">' +
      tpl__header() +
      '<div class="container">' +
        '<div class="services-page__container swiper-container js-page-cube">' +
          '<div class="swiper-wrapper js-scroll-container">' +
            '<section class="services-page__row swiper-slide">' +
              '<div class="services-page__content">' +
                tpl__rowHead(data.title, data.text) +
              '</div>' +
            '</section>' +

            data.rows.reduce(function(a, item) {
              return a + (
                '<section class="services-page__row swiper-slide">' +
                  '<div class="services-page__content">' +
                    tpl__row(item) +
                  '</div>' +
                '</section>'
              );
            }, '') +

            '<section class="services-page__row swiper-slide">' +
              '<div class="services-page__content">' +
                tpl__serviceMenu(menu, data.slug) +
              '</div>' +
            '</section>' +

            '<section class="services-page__row services-page__row--contacts swiper-slide js-contacts">' +
              '<div class="services-page__content">' +
                tpl__contacts(core) +
              '</div>' +
            '</section>' +

          '</div>' +
        '</div>' +
      '</div>' +
    '</article>'
  );
}


/*
  Шапка страницы услуг.
*/

var tpl__header = function() {
  return (
    '<div class="header">' +
      '<div class="container">' +
        '<div class="header__layout g-flex g-flex--justify">' +
          '<div class="header__logo">' +
            '<a class="js-inner-link" href="/">' +
              '<svg class="logo">' +
                '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-logo"></use>' +
              '</svg>' +
            '</a>' +
          '</div>' +
          '<div class="header__menu-button">' +
            tpl__menuBtn() +
          '</div>' +
        '</div>' +
      '</div>' +
    '</div>'
  );
}


/*
  Первый блок страниц услуг
*/

var tpl__rowHead = function(title, text) {
  return (
    '<div class="row-head">' +
      '<div class="container">' +
        '<h1 class="row-head__title title">' +
          title +
        '</h1>' +
        '<div class="row-head__text text">' +
          text +
        '</div>' +
      '</div>' +
    '</div>'
  );
}


/*
  Блок страниц услуг с картинками
*/

var tpl__row = function(data) {
  return (
    '<div class="row row--' + data.imagePosition + '">' +
      '<div class="row__layout">' +
        '<div class="row__image-wrap">' +
          '<img class="row__image image-loader js-image-loader" data-src="' + data.image + '">' +
        '</div>' +
        '<div class="row__content">' +
          '<div class="row-content">' +
            (data.label ? '<div class="row-content__label">' + data.label + '</div>' : '') +
            '<h2 class="row-content__title title">' + data.title + '</h2>' +
            '<div class="row-content__text text">' + data.text + '</div>' +
            (data.link ?
              ('<div class="row-content__link">' +
                '<a class="link text" target="_blank" href="' + data.link + '">' +
                  data.prettyLink +
                '</a>' +
              '</div>') :
              ('<div class="row-content__button">' +
                '<span class="btn js-order-btn" data-id="' + data.id + '">' +
                  '<span class="btn__label">Заказать</span>' +
                '</span>' +
              '</div>')
            ) +
          '</div>' +
        '</div>' +
      '</div>' +
    '</div>'
  );
}


/*
  Блок - меню услуг
*/

var tpl__serviceMenu = function(menu, doNotUseSlug) {
  return (
    '<div class="row-head">' +
      '<div class="container">' +
        '<h2 class="services-menu__title title">Все услуги</h2>' +
        '<div class="services-menu__layout g-flex">' +
          menu.reduce(function(a, item) {
            if (item.slug === doNotUseSlug) return a;

            return a + (
              '<div class="services-menu__item">' +
                '<a class="image-link js-inner-link" href="' + item.slug + '">' +
                  '<img class="image-link__image" src="' + item.image + '" alt="' + helper.escapeHtml(item.title) + '">' +
                  '<div class="image-link__title">' +
                    item.title +
                  '</div>' +
                '</a>' +
              '</div>'
            );
          }, '') +
        '</div>' +
      '</div>' +
    '</div>'
  );
}


/*
  Блок контактов. Включает в себя шаблон формы.
*/

var tpl__contacts = function(core) {
  return (
    '<div class="contacts" itemscope itemtype="http://schema.org/Organization">' +
      '<div class="container">' +
        '<h2 class="contacts__title title">Контакты</h2>' +
        '<div class="contacts__layout">' +
          '<div class="contacts-layout g-flex">' +
            '<div class="contacts-layout__item">' +
              '<div class="contacts-item">' +
                '<div class="contacts-item__icon">' +
                  '<svg>' +
                    '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-phone"></use>' +
                  '</svg>' +
                '</div>' +
                '<a class="contacts-item__data" href="tel:"' + core.siteData('phone').replace(/\s/gi, "") + '" itemprop="telephone">' +
                  core.siteData('phone').replace(/\s/gi, "&thinsp;") +
                '</a>' +
              '</div>' +
            '</div>' +

            '<div class="contacts-layout__item">' +
              '<div class="contacts-item">' +
                '<div class="contacts-item__icon">' +
                  '<svg>' +
                    '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-message"></use>' +
                  '</svg>' +
                '</div>' +
                '<a class="contacts-item__data" href="mailto:"' + core.siteData('email') + '" itemprop="email">' +
                  core.siteData('email') +
                '</a>' +
              '</div>' +
            '</div>' +
          '</div>' +
        '</div>' +

        '<div class="contacts__form">' +
          tpl__form(core) +
        '</div>' +

        '<div class="contacts__address">' +
          '<div class="contacts-adress" itemscope itemprop="address" itemtype="http://schema.org/PostalAddress">' +
            '<span itemprop="addressLocality">' + core.siteData('addressLocality') + '</span>' +
            '<span itemprop="streetAddress">' + core.siteData('streetAddress') + '</span>' +
          '</div>' +
        '</div>' +
      '</div>' +
    '</div>'
  );
}


/*
  Шаблон формы заказа.
*/

var tpl__form = function(core) {
  return (
    '<form class="order-form js-form" action="" method="POST">' +
      '<input class="js-service-id" type="hidden" name="service">' +
      '<h3 class="order-form__title">Начать работу</h3>' +
      '<div class="order-form__info">Оставьте свои данные и наш менеджер свяжется с Вами</div>' +
      '<div class="order-form__layout g-flex">' +
        '<div class="order-form__cell">' +
          '<div class="order-form__item">' +
            '<label class="input-item">' +
              '<input class="input-item__input js-input" id="order-form-name" type="text" name="name" minlength="2" maxlength="50" required>' +
              '<label class="input-item__label" for="order-form-name">Ваше имя</label>' +
              '<span class="input-item__error"></span>' +
            '</label>' +
          '</div>' +

          '<div class="order-form__item">' +
            '<label class="input-item">' +
              '<input class="input-item__input js-input" id="order-form-email" type="text" name="email" minlength="6" maxlength="50" required>' +
              '<label class="input-item__label" for="order-form-email">E-mail</label>' +
              '<span class="input-item__error"></span>' +
            '</label>' +
          '</div>' +
        '</div>' +

        '<div class="order-form__cell">' +
          '<label class="input-item">' +
            '<textarea class="input-item__textarea js-input" id="order-form-comment" name="comment" maxlength="2048"></textarea>' +
            '<label class="input-item__label" for="order-form-comment">Комментарий</label>' +
            '<span class="input-item__error"></span>' +
          '</label>' +
        '</div>' +
      '</div>' +

      '<div class="order-form__layout g-flex">' +
        '<div class="order-form__cell order-form__cell--btn-left">' +
          '<button class="btn-lighted" type="submit">' +
            '<span class="btn-lighted__label">отправить данные</span>' +
          '</button>' +
        '</div>' +

        '<div class="order-form__cell order-form__cell--btn-right">' +
          '<a class="btn-empty" href="' + core.siteData('brief') + '" target="_blank">' +
            '<span class="btn-empty__label">скачать бриф</span>' +
          '</a>' +
        '</div>' +
      '</div>' +
    '</form>'
  );
}


/*
  Кнопка меню.
*/

var tpl__menuBtn = function() {
  return (
    '<div class="menu-btn js-menu-btn">' +
      'M <span class="menu-btn__stairs">E</span> N U' +
    '</div>'
  );
}


/*
  Шаблон главной страниц.
*/

var tpl__homepage = function(data, menu) {
  return (
    '<div class="homepage js-page">' +
      tpl__servicesHomeNav(menu) +
      '<div class="container">' +
        '<div class="homepage__layout">' +
          '<div class="homepage__content">' +
            '<div class="homepage__head">' +
              '<div class="homepage-head">' +
                '<div class="homepage-head__agency">' +
                  data.agency +
                '</div>' +
                '<div class="homepage-head__name js-site-name">' +
                  data.title +
                '</div>' +
                '<div class="homepage-head__slogan">' +
                  data.slogan +
                '</div>' +
              '</div>' +
            '</div>' +

            '<div class="homepage__slider-menu">' +
              tpl__portfolioMenu(data.menu) +
            '</div>' +

            '<div class="homepage__menu-btn">' +
              tpl__menuBtn() +
            '</div>' +
          '</div>' +
        '</div>' +
      '</div>' +
    '</div>'
  );
}


/*
  Навигация на главной странице.
*/

var tpl__servicesHomeNav = function(menu) {
  return (
    '<div class="nav services-nav js-services-nav">' +
      menu.reduce(function(a, item) {
        return a + (
          '<div class="services-nav__item services-nav__item--' + item.position + '">' +
            '<a class="services-nav__link js-inner-link" href="' + item.slug + '">' +
              item.title +
            '</a>' +
          '</div>'
        );
      }, '') +
    '</div>'
  );
}


/*
  Ссылки портфолио на главной.
*/

var tpl__portfolioMenu = function(menu) {
  return (
    '<div class="portfolio-menu">' +
      menu.reduce(function(a, item) {
        return a + (
          '<div class="portfolio-menu__item">' +
            '<a class="portfolio-menu__link js-bg-item" href="' + item.link + '" data-id="' + item.id + '" target="_blank">' +
              item.title +
            '</a>' +
          '</div>'
        );
      }, '') +
    '</div>'
  );
}


var tpl__rotatorSlide = function() {
  return '<div class="pages-rotator__page swiper-slide"></div>';
}
;