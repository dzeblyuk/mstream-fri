;
'use strict';


/*
  Ядрышко.
*/

var core = {
  isReady: false,

  canChangePage: false,

  initChain: new callbacksChain(),

  __siteData: {},

  homepageController: false,
  currentPageController: false,

  dataUrls: {
    'config': '/data/config.json',
    'pages': '/data/pages.json',
  },

  browser: helper.detectBrowser(),
}


/*
  Получение различной информации сайта/компании.
*/

core.siteData = function(identif) {
  return core.__siteData[identif];
}


/*
  Мероприятия. Точнее события.
*/

core.events = {
  __list: {},

  on: function(eventName, fn) {
    var _ = this;

    if (typeof eventName === 'string' && typeof fn === 'function') {
      if (! _.__list[eventName]) {
        _.__list[eventName] = [];
      }

      _.__list[eventName].push(fn);

      return function() {
        var index = _.__list[eventName].indexOf(fn);
        delete _.__list[eventName][index];

        if (_.__list[eventName].length === 0) {
          _.__list[eventName].splice(index, 1);
        }
        else {
          _.__list[eventName].sort();
        }
      }
    }
  },

  one: function(eventName, fn) {
    var destroyer = this.on(eventName, function() {
      helper.runCallback(fn);
      destroyer();
    });
  },

  trigger: function() {
    var _ = this;

    var eventName = arguments[0],
      args = Array.prototype.slice.call(arguments, 1);

    if (eventName in _.__list && 'length' in _.__list[eventName]) {
      _.__list[eventName].forEach(function(e) {
        e.apply(e, args);
      });
    }
  }
}


/*
  Хранилище.
*/

core.storage = {
  __data: {},
  __keys: {},

  isAvailable: 'localStorage' in window && window['localStorage'] !== null,

  add: function(identif, data) {
    this.__data[identif] = data;

    if (!this.isAvailable) return;
    if (typeof data === 'function') return;

    if (typeof data !== 'string') {
      data = JSON.stringify(data);
    }

    localStorage.setItem(identif, JSON.stringify(data));
  },

  get: function(identif) {
    if (identif in this.__data) {
      return ((typeof this.__data[identif] === 'string') ?
        this.__data[identif] :
        JSON.parse(JSON.stringify(this.__data[identif]))
      );
    }

    if (!this.isAvailable) return;

    var data = localStorage.getItem(identif);

    try {
      data = JSON.parse(data);
    } catch(e){}

    this.__data[identif] = data;

    return data;
  },


  /*
    Достаем данные из хранилища.
    Если данных нет, или хранилище не доступно - используем getDataFunc для получения данных.
  */

  wrapGetter: function(identif, getDataFunc, callback) {
    var _ = this;

    var data = this.get(identif);

    if (data) {
      helper.runCallback(callback, data);
    }
    else {
      try {
        getDataFunc(function(data) {
          _.add(identif, data);
          helper.runCallback(callback, data);
        });
      }
      catch(e) {
        console.log(e);
      }
    }
  }
}


/*
  Запрос на сервер.
*/

core.request = function(url, dataType, callback) {
  var request = new XMLHttpRequest();

  request.open('POST', url, true);
  request.setRequestHeader("X-Requested-With", "XMLHttpRequest");

  request.onload = function(){
    if (this.status >= 200 && this.status < 400) {
      var responseData = this.responseText;

      if (dataType === 'json') {
        responseData = JSON.parse(responseData);
      }

      helper.runCallback(callback, responseData);
    } else {
      throw new Error(this.statusText);
    }
  }

  request.send();
}


/*
  Получение контроллеров для страниц.
  Таким образом, для разных страниц будут свои подключаемые скрипты.
*/

core.controllers = {
  __list: {},

  add: function(identif, controller) {
    this.__list[identif] = controller;
  },

  get: function(identif) {
    if (identif in this.__list) {
      return this.__list[identif];
    }

    return false;
  }
}


/*
  Роуты.
  Здесь хранятся зависимости - какой контроллер, на какой странице использовать.
*/

core.routes = {
  __list: {},

  add: function(path, controllerIdentif) {
    this.__list[path] = controllerIdentif;
  },

  get: function(path) {
    if (path in this.__list) {
      return this.__list[path];
    }

    return false;
  }
}


/*
  Эвенты срабатывающие при готовности ядра.
*/

core.ready = function (callback) {
  if (core.isReady) {
    helper.runCallback(callback);
  }
  else {
    core.events.one('ready', callback);
  }
}


/*
  Получение контроллера для урла.
*/

core.getPageController = function(url) {
  var controller = core.controllers.get(core.routes.get(getPath(url)));

  if (controller) {
    return function(data) {
      return new controller(data, core);
    }
  }

  return false;
}

core.controllerExist = function(url) {
  return !! core.controllers.get(core.routes.get(getPath(url)));
}

var getPath = function(url) {
  var l = document.location;
  return (url ? url.replace(l.origin, '') : l.pathname).replace('.html', '');
}


/*
  Загрузка данных с сервера.
*/

var downloadData = function(identif, callback) {
  core.request(core.dataUrls[identif], 'json', callback);
}


/*
  Поиск данных в теле страницы.
*/

var findDataOnPage = function(identif) {
  if (!identif) false;

  return window['__' + identif] || false;
}


/*
  Загрузка данных.
*/

core.loadData = function(identif, callback) {
  var data;

  if (data = findDataOnPage(identif)) {
    helper.runCallback(callback, data);
  }
  else {
    downloadData(identif, callback);
  }
}


/*
  Для ie отключаем кубический эффект - не тянет.
*/

core.swiperConfig = function() {
  var config = {
    direction: 'vertical',
    mousewheel: true,
    pagination: false,
  }

  if (core.browser.indexOf('Internet Explorer') === -1) {
    config.effect = 'cube';
    config.cubeEffect = {
      shadow: false,
      slideShadows: false,
    };
  }

  return config;
}


/*
  Изменение мета-данных страницы.
*/

core.setMeta = function (metaData) {
  var meta = document.getElementsByTagName("meta");
  var name, property;

  for (var i = 0; i < meta.length; i++) {
    name = meta[i].name.toLowerCase();
    if (name in metaData) {
      meta[i].content = metaData[name];
    }

    property = meta[i].attributes.property;

    if (property) {
      property = property.name.toLowerCase();
      if (property in metaData) {
        meta[i].content = metaData[property];
      }
    }
  }

  var title = document.getElementsByTagName("title")[0];

  if ('title' in metaData) {
    title.innerHTML = metaData['title'];
  }
}


/*
  Изменение url страницы.
*/

core.changeUrl = (function() {
  var isAvailable = !!(window.history && history.pushState);

  return function(url) {
    if (url !== window.location.href) {
      if (isAvailable) {
        history.pushState({}, '', url);
      } else {
        window.location.href = url;
      }
    }
  }
}());


/*
  Инициализация поворачивателя.
*/

core.initRotator = function() {
  core.rotator = new rotator(document.querySelector('.js-pages-rotator'), core.swiperConfig());
}


/*
  Запуск инициализации.
  Выполняем последовательно.
  В скорости не должны потерять, а так - проще контролировать процесс.
*/

core.init = function() {
  core.initRotator();

  core.initChain.run(function() {
    core.isReady = true;
    core.events.trigger('ready');
  });
}


/*********

  Инициализация ядра.

*********/


/*
  Чистим хранилище, если данные сайта были изменены.
*/

core.initChain.add(function(callback) {
  var cacheId = findDataOnPage('cacheId');

  if (cacheId && core.storage.isAvailable) {
    if (cacheId !== core.storage.get('cacheId')) {
      localStorage.clear();
    }
  }

  helper.runCallback(callback);
});


/*
  Добавление контроллеров и роутов.
  Происходит асинхронно, т.к. скрипты ниже сразу могут быть не видны.
*/

core.initChain.add(function(callback) {
  setTimeout(function() {
    /*
      Регистрируем имеющиеся контроллеры.
    */
    core.controllers.add('home', homepageController);
    core.controllers.add('services', servicesPageController);

    core.routes.add('/', 'home');

    helper.runCallback(callback);
  }, 1);
});


/*
  Загрузка конфига.
*/

core.initChain.add(function(callback) {
  var dataIdentif = 'siteData';

  core.storage.wrapGetter(dataIdentif, function(callback){
    core.loadData(dataIdentif, callback);
  }, function(data) {
    core.__siteData = data;

    callback();
  });
});


/*
  Загрузка данных о страницах.
*/

core.initChain.add(function(callback) {
  var dataIdentif = 'pages';

  core.storage.wrapGetter(dataIdentif, function(c) {
    core.loadData(dataIdentif, c);
  }, function(data) {
    data.forEach(function(el) {
      if (el.slug !== '/') {
        core.routes.add(el.slug, 'services');
      }
    });

    callback();
  });
});


/*
  Запускаем велосипед.
*/

var getPageData = function(url) {
  return core.storage.get('pages').find(function(item) {
    return item.slug === url;
  });
}

core.ready(function() {
  var url = document.location.pathname.replace('.html', '');
  core.menu.init();

  if (!core.controllerExist(url)) {
    return;
  }

  core.currentPageController = core.getPageController(url)(getPageData(url));

  core.currentPageController.init(document.querySelector('.js-page'));
  core.currentPageController.ready();

  core.initHomePage(function() {
    core.canChangePage = true;
  });
});

document.addEventListener('DOMContentLoaded', function() {
  core.init();
});



/*
  Инициализация внутренней навигации.
*/

core.initInnerLinks = function(container) {
  var links = container.querySelectorAll('.js-inner-link');
  [].forEach.call(links, function(el) {
    if (el.classList.contains('is-ready')) return;

    el.addEventListener('click', function(e) {
      e.preventDefault();

      core.changePage(this.href);
    });

    el.classList.add('is-ready');
  });
}




/*
  Меню
*/

core.getMenu = function() {
  return core.storage.get('pages').reduce(function(result, item) {
    if (item.useInMenu) {
      result.push({
        slug: item.slug,
        image: item.servicesMenuImage,
        title: item.title,
        position: item.pagePosition,
        menuLabel: item.menuLabel
      });
    }

    return result;
  }, []);
}


/*
  Блокирование окна на время анимации.
*/

var blockWindow = function() {
  document.querySelector('html, body').classList.add('is-blocked');
  core.canChangePage = false;
}

var unblockWindow = function() {
  document.querySelector('html, body').classList.remove('is-blocked');
  core.canChangePage = true;
}


core.menu = {
  elMenu: null,
  init: function() {
    var _ = this;
    _.elMenu = document.querySelector('.js-main-menu');
    var elClose = document.querySelector('.js-menu-close');

    if (!(_.elMenu && elClose)) return;

    elClose.addEventListener('click', _.close.bind(_));

    _.initMenuBtn();
    _.initMenuLinks();
  },

  initMenuBtn: function(container) {
    var _ = this;

    container = container || document;
    var elOpenMenu = container.querySelector('.js-menu-btn');

    if (!elOpenMenu) return;

    elOpenMenu.addEventListener('click', _.open.bind(_));
  },

  initMenuLinks: function() {
    var _ = this;

    [].forEach.call(document.querySelectorAll('.js-menu-link'), function(el){
      el.addEventListener('click', function(e) {
        e.preventDefault();

        var url = this.href;
        _.close(function() {
          core.changePage(url);
        });
      });
    });
  },

  open: function() {
    this.elMenu.classList.add('is-showed');
  },

  close: function(callback) {
    var _ = this;

    var handler = function() {
      helper.runCallback(callback);
      _.elMenu.removeEventListener('transitionend', handler);
    }

    _.elMenu.addEventListener('transitionend', handler);
    _.elMenu.classList.remove('is-showed');
  }
}


/*
  Получение обратного направления.
*/

var invertDirection = function(position) {
  var result;

  switch(position) {
    case 'top':
        result = 'bottom';
      break;

    case 'right':
        result = 'left';
      break;

    case 'bottom':
        result = 'top';
      break;

    case 'left':
    default:
        result = 'right';
      break;
  }

  return result;
}


/*
  Для красивого отображения поворота, нам нужна готовая главная страница
*/

core.initHomePage = function(callback) {
  if (core.homepageController) {
    helper.runCallback(callback);
    return;
  }

  var currentPageData = core.currentPageController.getPageData();
  if (currentPageData.slug === '/') {
    core.homepageController = core.currentPageController;
    helper.runCallback(callback);
    return;
  }

  var homepageData = getPageData('/');
  core.homepageController = core.getPageController('/')(homepageData);
  var position = invertDirection(currentPageData.pagePosition);

  core.homepageController.makePage();
  core.homepageController.init();
  core.rotator.addSlide(core.homepageController.getPageEl(), position, callback);
}


/*
  Смена страницы.
*/

core.changePage = function(url) {
  var newPageData, newController;
  if (!core.canChangePage) return;

  if (url.indexOf(window.location.origin) !== 0) {
    window.location.href = url;
  }

  var pathname = getPath(url);

  if (pathname === window.location.pathname) return;

  core.canChangePage = false;

  if (pathname === '/' && core.homepageController) {
    newPageData = core.homepageController.getPageData();
    newController = core.homepageController;
  }
  else {
    if (!core.controllerExist(pathname)) {
      window.location.href = pathname;
      return;
    }

    newPageData = getPageData(pathname);

    newController = core.getPageController(pathname)(newPageData);
  }

  var currentPageData = core.currentPageController.getPageData();

  var animationChain = new callbacksChain();

  var elScaler = document.querySelector('.js-page-scaler');


  /*
    Блокируем экран.
    Показываем главную страницу, если она была скрыта.
  */

  animationChain.add(function(callback) {
    blockWindow();
    core.homepageController.getPageEl().classList.remove('is-hidden');
    helper.runCallback(callback);
  });


  /*
    Отдаляем куб.
  */

  animationChain.add(function(callback) {
    zoomOut({
      el: elScaler,
      callback: callback
    })
  });


  /*
    Если конечная страница - не главная, то создаем ее.
  */

  if (newPageData.slug !== '/') {
    animationChain.add(function(callback) {
      var page = newController.makePage();

      core.rotator.addSlide(page, newPageData.pagePosition, function() {
        newController.init();
        helper.runCallback(callback);
      });
    });
  }


  /*
    Меняем урл
  */

  animationChain.add(function(callback) {
    core.changeUrl(url);
    helper.runCallback(callback);
  });


  /*
    Если текущая страница - не главная, надо для начала повернуть на главную, при любой конечной странице.
    Затем удаляем текущую.
  */

  if (currentPageData.slug !== '/') {
    var direction = invertDirection(currentPageData.pagePosition);

    animationChain.add(function(callback) {
      core.rotator.rotate(direction, callback);
    });

    animationChain.add(function(callback) {
      core.rotator.remove(core.currentPageController.getPageEl(), callback);
    });
  }


  /*
    Если конечная страница - не главная - вертим на нее.
  */

  if (newPageData.slug !== '/') {
    animationChain.add(function(callback) {
      core.rotator.rotate(newPageData.pagePosition, callback)
    });
  }


  /*
    Приближаем куб обратно.
  */

  animationChain.add(function(callback) {
    zoomIn({
      el: elScaler,
      callback: callback
    });
  });

  /*
    Разбираемся к чему пришли.
  */

  animationChain.run(function(){
    newController.ready();

    core.currentPageController = newController;
    core.initHomePage(function() {
      unblockWindow();

      if (newPageData.slug !== '/') {
        core.homepageController.getPageEl().classList.add('is-hidden');
      }
    });
  })
}
;