;
window.onload = function() {
  setTimeout(function() {
    window.onpopstate = function() {
      window.location.reload();
    }
  }, 0);
}

var callbacksChain = function() {
  var list = [];

  return {
    add: function(callback) {
      if (callback instanceof Array ) {
        callback.forEach(function(item) {
          this.add(item);
        });
        return;
      }

      if (typeof callback !== 'function') return;

      list.push(callback);
    },
    run: function(callback) {
      list.reverse().reduce(function(a, func) {
        return function() {
          func(a);
        }
      }, callback)();
    }
  }
}

inputFillCheck = function(querySelector) {
  [].forEach.call(document.querySelectorAll(querySelector), function(inputEl) {
    inputEl.addEventListener( 'focus', onInputFocus );
    inputEl.addEventListener( 'blur', onInputBlur );
    inputEl.addEventListener( 'change', onInputChange );

    onInputChange({target:inputEl});
  });

  function onInputFocus( ev ) {
    ev.target.parentNode.classList.add('input-item--filled');
    ev.target.parentNode.classList.add('input-item--focus');
  }

  function onInputBlur( ev ) {
    ev.target.parentNode.classList.remove('input-item--focus');
    if (ev.target.value.trim() === '') {
      ev.target.parentNode.classList.remove('input-item--filled');
    }
  }

  function onInputChange( ev ) {
    if (ev.target.value.trim() !== '') {
      ev.target.parentNode.classList.add('input-item--filled');
    }
  }
}



var findObjectPosition = function(obj) {
  var curtop = 0;
  if (obj.offsetParent) {
    do {
      curtop += obj.offsetTop;
    } while (obj = obj.offsetParent);

    return [curtop];
  }
}

var makeEl = function(tagName, HTMLContent) {
  var el = document.createElement(tagName);
  el.innerHTML = HTMLContent;
  return el.firstChild;
}
;