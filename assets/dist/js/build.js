/**
 * Swiper 4.1.0
 * Most modern mobile touch slider and framework with hardware accelerated transitions
 * http://www.idangero.us/swiper/
 *
 * Copyright 2014-2018 Vladimir Kharlampidi
 *
 * Released under the MIT License
 *
 * Released on: February 8, 2018
 */
!function(e,t){"object"==typeof exports&&"undefined"!=typeof module?module.exports=t():"function"==typeof define&&define.amd?define(t):e.Swiper=t()}(this,function(){"use strict";var e=function(e){for(var t=0;t<e.length;t+=1)this[t]=e[t];return this.length=e.length,this};function t(t,i){var s=[],a=0;if(t&&!i&&t instanceof e)return t;if(t)if("string"==typeof t){var n,r,o=t.trim();if(o.indexOf("<")>=0&&o.indexOf(">")>=0){var l="div";for(0===o.indexOf("<li")&&(l="ul"),0===o.indexOf("<tr")&&(l="tbody"),0!==o.indexOf("<td")&&0!==o.indexOf("<th")||(l="tr"),0===o.indexOf("<tbody")&&(l="table"),0===o.indexOf("<option")&&(l="select"),(r=document.createElement(l)).innerHTML=o,a=0;a<r.childNodes.length;a+=1)s.push(r.childNodes[a])}else for(n=i||"#"!==t[0]||t.match(/[ .<>:~]/)?(i||document).querySelectorAll(t.trim()):[document.getElementById(t.trim().split("#")[1])],a=0;a<n.length;a+=1)n[a]&&s.push(n[a])}else if(t.nodeType||t===window||t===document)s.push(t);else if(t.length>0&&t[0].nodeType)for(a=0;a<t.length;a+=1)s.push(t[a]);return new e(s)}function i(e){for(var t=[],i=0;i<e.length;i+=1)-1===t.indexOf(e[i])&&t.push(e[i]);return t}t.fn=e.prototype,t.Class=e,t.Dom7=e;"resize scroll".split(" ");var s={addClass:function(e){if(void 0===e)return this;for(var t=e.split(" "),i=0;i<t.length;i+=1)for(var s=0;s<this.length;s+=1)void 0!==this[s].classList&&this[s].classList.add(t[i]);return this},removeClass:function(e){for(var t=e.split(" "),i=0;i<t.length;i+=1)for(var s=0;s<this.length;s+=1)void 0!==this[s].classList&&this[s].classList.remove(t[i]);return this},hasClass:function(e){return!!this[0]&&this[0].classList.contains(e)},toggleClass:function(e){for(var t=e.split(" "),i=0;i<t.length;i+=1)for(var s=0;s<this.length;s+=1)void 0!==this[s].classList&&this[s].classList.toggle(t[i]);return this},attr:function(e,t){var i=arguments;if(1===arguments.length&&"string"==typeof e)return this[0]?this[0].getAttribute(e):void 0;for(var s=0;s<this.length;s+=1)if(2===i.length)this[s].setAttribute(e,t);else for(var a in e)this[s][a]=e[a],this[s].setAttribute(a,e[a]);return this},removeAttr:function(e){for(var t=0;t<this.length;t+=1)this[t].removeAttribute(e);return this},data:function(e,t){var i;if(void 0!==t){for(var s=0;s<this.length;s+=1)(i=this[s]).dom7ElementDataStorage||(i.dom7ElementDataStorage={}),i.dom7ElementDataStorage[e]=t;return this}if(i=this[0]){if(i.dom7ElementDataStorage&&e in i.dom7ElementDataStorage)return i.dom7ElementDataStorage[e];var a=i.getAttribute("data-"+e);return a||void 0}},transform:function(e){for(var t=0;t<this.length;t+=1){var i=this[t].style;i.webkitTransform=e,i.transform=e}return this},transition:function(e){"string"!=typeof e&&(e+="ms");for(var t=0;t<this.length;t+=1){var i=this[t].style;i.webkitTransitionDuration=e,i.transitionDuration=e}return this},on:function(){for(var e=[],i=arguments.length;i--;)e[i]=arguments[i];var s,a=e[0],n=e[1],r=e[2],o=e[3];function l(e){var i=e.target;if(i){var s=e.target.dom7EventData||[];if(s.unshift(e),t(i).is(n))r.apply(i,s);else for(var a=t(i).parents(),o=0;o<a.length;o+=1)t(a[o]).is(n)&&r.apply(a[o],s)}}function d(e){var t=e&&e.target?e.target.dom7EventData||[]:[];t.unshift(e),r.apply(this,t)}"function"==typeof e[1]&&(a=(s=e)[0],r=s[1],o=s[2],n=void 0),o||(o=!1);for(var h,p=a.split(" "),c=0;c<this.length;c+=1){var u=this[c];if(n)for(h=0;h<p.length;h+=1)u.dom7LiveListeners||(u.dom7LiveListeners=[]),u.dom7LiveListeners.push({type:a,listener:r,proxyListener:l}),u.addEventListener(p[h],l,o);else for(h=0;h<p.length;h+=1)u.dom7Listeners||(u.dom7Listeners=[]),u.dom7Listeners.push({type:a,listener:r,proxyListener:d}),u.addEventListener(p[h],d,o)}return this},off:function(){for(var e=[],t=arguments.length;t--;)e[t]=arguments[t];var i,s=e[0],a=e[1],n=e[2],r=e[3];"function"==typeof e[1]&&(s=(i=e)[0],n=i[1],r=i[2],a=void 0),r||(r=!1);for(var o=s.split(" "),l=0;l<o.length;l+=1)for(var d=0;d<this.length;d+=1){var h=this[d];if(a){if(h.dom7LiveListeners)for(var p=0;p<h.dom7LiveListeners.length;p+=1)n?h.dom7LiveListeners[p].listener===n&&h.removeEventListener(o[l],h.dom7LiveListeners[p].proxyListener,r):h.dom7LiveListeners[p].type===o[l]&&h.removeEventListener(o[l],h.dom7LiveListeners[p].proxyListener,r)}else if(h.dom7Listeners)for(var c=0;c<h.dom7Listeners.length;c+=1)n?h.dom7Listeners[c].listener===n&&h.removeEventListener(o[l],h.dom7Listeners[c].proxyListener,r):h.dom7Listeners[c].type===o[l]&&h.removeEventListener(o[l],h.dom7Listeners[c].proxyListener,r)}return this},trigger:function(){for(var e=[],t=arguments.length;t--;)e[t]=arguments[t];for(var i=e[0].split(" "),s=e[1],a=0;a<i.length;a+=1)for(var n=0;n<this.length;n+=1){var r=void 0;try{r=new window.CustomEvent(i[a],{detail:s,bubbles:!0,cancelable:!0})}catch(e){(r=document.createEvent("Event")).initEvent(i[a],!0,!0),r.detail=s}this[n].dom7EventData=e.filter(function(e,t){return t>0}),this[n].dispatchEvent(r),this[n].dom7EventData=[],delete this[n].dom7EventData}return this},transitionEnd:function(e){var t,i=["webkitTransitionEnd","transitionend"],s=this;function a(n){if(n.target===this)for(e.call(this,n),t=0;t<i.length;t+=1)s.off(i[t],a)}if(e)for(t=0;t<i.length;t+=1)s.on(i[t],a);return this},outerWidth:function(e){if(this.length>0){if(e){var t=this.styles();return this[0].offsetWidth+parseFloat(t.getPropertyValue("margin-right"))+parseFloat(t.getPropertyValue("margin-left"))}return this[0].offsetWidth}return null},outerHeight:function(e){if(this.length>0){if(e){var t=this.styles();return this[0].offsetHeight+parseFloat(t.getPropertyValue("margin-top"))+parseFloat(t.getPropertyValue("margin-bottom"))}return this[0].offsetHeight}return null},offset:function(){if(this.length>0){var e=this[0],t=e.getBoundingClientRect(),i=document.body,s=e.clientTop||i.clientTop||0,a=e.clientLeft||i.clientLeft||0,n=e===window?window.scrollY:e.scrollTop,r=e===window?window.scrollX:e.scrollLeft;return{top:t.top+n-s,left:t.left+r-a}}return null},css:function(e,t){var i;if(1===arguments.length){if("string"!=typeof e){for(i=0;i<this.length;i+=1)for(var s in e)this[i].style[s]=e[s];return this}if(this[0])return window.getComputedStyle(this[0],null).getPropertyValue(e)}if(2===arguments.length&&"string"==typeof e){for(i=0;i<this.length;i+=1)this[i].style[e]=t;return this}return this},each:function(e){if(!e)return this;for(var t=0;t<this.length;t+=1)if(!1===e.call(this[t],t,this[t]))return this;return this},html:function(e){if(void 0===e)return this[0]?this[0].innerHTML:void 0;for(var t=0;t<this.length;t+=1)this[t].innerHTML=e;return this},text:function(e){if(void 0===e)return this[0]?this[0].textContent.trim():null;for(var t=0;t<this.length;t+=1)this[t].textContent=e;return this},is:function(i){var s,a,n=this[0];if(!n||void 0===i)return!1;if("string"==typeof i){if(n.matches)return n.matches(i);if(n.webkitMatchesSelector)return n.webkitMatchesSelector(i);if(n.msMatchesSelector)return n.msMatchesSelector(i);for(s=t(i),a=0;a<s.length;a+=1)if(s[a]===n)return!0;return!1}if(i===document)return n===document;if(i===window)return n===window;if(i.nodeType||i instanceof e){for(s=i.nodeType?[i]:i,a=0;a<s.length;a+=1)if(s[a]===n)return!0;return!1}return!1},index:function(){var e,t=this[0];if(t){for(e=0;null!==(t=t.previousSibling);)1===t.nodeType&&(e+=1);return e}},eq:function(t){if(void 0===t)return this;var i,s=this.length;return new e(t>s-1?[]:t<0?(i=s+t)<0?[]:[this[i]]:[this[t]])},append:function(){for(var t,i=[],s=arguments.length;s--;)i[s]=arguments[s];for(var a=0;a<i.length;a+=1){t=i[a];for(var n=0;n<this.length;n+=1)if("string"==typeof t){var r=document.createElement("div");for(r.innerHTML=t;r.firstChild;)this[n].appendChild(r.firstChild)}else if(t instanceof e)for(var o=0;o<t.length;o+=1)this[n].appendChild(t[o]);else this[n].appendChild(t)}return this},prepend:function(t){var i,s;for(i=0;i<this.length;i+=1)if("string"==typeof t){var a=document.createElement("div");for(a.innerHTML=t,s=a.childNodes.length-1;s>=0;s-=1)this[i].insertBefore(a.childNodes[s],this[i].childNodes[0])}else if(t instanceof e)for(s=0;s<t.length;s+=1)this[i].insertBefore(t[s],this[i].childNodes[0]);else this[i].insertBefore(t,this[i].childNodes[0]);return this},next:function(i){return this.length>0?i?this[0].nextElementSibling&&t(this[0].nextElementSibling).is(i)?new e([this[0].nextElementSibling]):new e([]):this[0].nextElementSibling?new e([this[0].nextElementSibling]):new e([]):new e([])},nextAll:function(i){var s=[],a=this[0];if(!a)return new e([]);for(;a.nextElementSibling;){var n=a.nextElementSibling;i?t(n).is(i)&&s.push(n):s.push(n),a=n}return new e(s)},prev:function(i){if(this.length>0){var s=this[0];return i?s.previousElementSibling&&t(s.previousElementSibling).is(i)?new e([s.previousElementSibling]):new e([]):s.previousElementSibling?new e([s.previousElementSibling]):new e([])}return new e([])},prevAll:function(i){var s=[],a=this[0];if(!a)return new e([]);for(;a.previousElementSibling;){var n=a.previousElementSibling;i?t(n).is(i)&&s.push(n):s.push(n),a=n}return new e(s)},parent:function(e){for(var s=[],a=0;a<this.length;a+=1)null!==this[a].parentNode&&(e?t(this[a].parentNode).is(e)&&s.push(this[a].parentNode):s.push(this[a].parentNode));return t(i(s))},parents:function(e){for(var s=[],a=0;a<this.length;a+=1)for(var n=this[a].parentNode;n;)e?t(n).is(e)&&s.push(n):s.push(n),n=n.parentNode;return t(i(s))},closest:function(t){var i=this;return void 0===t?new e([]):(i.is(t)||(i=i.parents(t).eq(0)),i)},find:function(t){for(var i=[],s=0;s<this.length;s+=1)for(var a=this[s].querySelectorAll(t),n=0;n<a.length;n+=1)i.push(a[n]);return new e(i)},children:function(s){for(var a=[],n=0;n<this.length;n+=1)for(var r=this[n].childNodes,o=0;o<r.length;o+=1)s?1===r[o].nodeType&&t(r[o]).is(s)&&a.push(r[o]):1===r[o].nodeType&&a.push(r[o]);return new e(i(a))},remove:function(){for(var e=0;e<this.length;e+=1)this[e].parentNode&&this[e].parentNode.removeChild(this[e]);return this},add:function(){for(var e=[],i=arguments.length;i--;)e[i]=arguments[i];var s,a;for(s=0;s<e.length;s+=1){var n=t(e[s]);for(a=0;a<n.length;a+=1)this[this.length]=n[a],this.length+=1}return this},styles:function(){return this[0]?window.getComputedStyle(this[0],null):{}}};Object.keys(s).forEach(function(e){t.fn[e]=s[e]});var a,n,r,o="undefined"==typeof window?{navigator:{userAgent:""},location:{},history:{},addEventListener:function(){},removeEventListener:function(){},getComputedStyle:function(){return{}},Image:function(){},Date:function(){},screen:{}}:window,l={deleteProps:function(e){var t=e;Object.keys(t).forEach(function(e){try{t[e]=null}catch(e){}try{delete t[e]}catch(e){}})},nextTick:function(e,t){return void 0===t&&(t=0),setTimeout(e,t)},now:function(){return Date.now()},getTranslate:function(e,t){var i,s,a;void 0===t&&(t="x");var n=o.getComputedStyle(e,null);return o.WebKitCSSMatrix?((s=n.transform||n.webkitTransform).split(",").length>6&&(s=s.split(", ").map(function(e){return e.replace(",",".")}).join(", ")),a=new o.WebKitCSSMatrix("none"===s?"":s)):i=(a=n.MozTransform||n.OTransform||n.MsTransform||n.msTransform||n.transform||n.getPropertyValue("transform").replace("translate(","matrix(1, 0, 0, 1,")).toString().split(","),"x"===t&&(s=o.WebKitCSSMatrix?a.m41:16===i.length?parseFloat(i[12]):parseFloat(i[4])),"y"===t&&(s=o.WebKitCSSMatrix?a.m42:16===i.length?parseFloat(i[13]):parseFloat(i[5])),s||0},parseUrlQuery:function(e){var t,i,s,a,n={},r=e||o.location.href;if("string"==typeof r&&r.length)for(a=(i=(r=r.indexOf("?")>-1?r.replace(/\S*\?/,""):"").split("&").filter(function(e){return""!==e})).length,t=0;t<a;t+=1)s=i[t].replace(/#\S+/g,"").split("="),n[decodeURIComponent(s[0])]=void 0===s[1]?void 0:decodeURIComponent(s[1])||"";return n},isObject:function(e){return"object"==typeof e&&null!==e&&e.constructor&&e.constructor===Object},extend:function(){for(var e=[],t=arguments.length;t--;)e[t]=arguments[t];for(var i=Object(e[0]),s=1;s<e.length;s+=1){var a=e[s];if(void 0!==a&&null!==a)for(var n=Object.keys(Object(a)),r=0,o=n.length;r<o;r+=1){var d=n[r],h=Object.getOwnPropertyDescriptor(a,d);void 0!==h&&h.enumerable&&(l.isObject(i[d])&&l.isObject(a[d])?l.extend(i[d],a[d]):!l.isObject(i[d])&&l.isObject(a[d])?(i[d]={},l.extend(i[d],a[d])):i[d]=a[d])}}return i}},d="undefined"==typeof document?{addEventListener:function(){},removeEventListener:function(){},activeElement:{blur:function(){},nodeName:""},querySelector:function(){return{}},querySelectorAll:function(){return[]},createElement:function(){return{style:{},setAttribute:function(){},getElementsByTagName:function(){return[]}}},location:{hash:""}}:document,h=(r=d.createElement("div"),{touch:o.Modernizr&&!0===o.Modernizr.touch||!!("ontouchstart"in o||o.DocumentTouch&&d instanceof o.DocumentTouch),pointerEvents:!(!o.navigator.pointerEnabled&&!o.PointerEvent),prefixedPointerEvents:!!o.navigator.msPointerEnabled,transition:(n=r.style,"transition"in n||"webkitTransition"in n||"MozTransition"in n),transforms3d:o.Modernizr&&!0===o.Modernizr.csstransforms3d||(a=r.style,"webkitPerspective"in a||"MozPerspective"in a||"OPerspective"in a||"MsPerspective"in a||"perspective"in a),flexbox:function(){for(var e=r.style,t="alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(" "),i=0;i<t.length;i+=1)if(t[i]in e)return!0;return!1}(),observer:"MutationObserver"in o||"WebkitMutationObserver"in o,passiveListener:function(){var e=!1;try{var t=Object.defineProperty({},"passive",{get:function(){e=!0}});o.addEventListener("testPassiveListener",null,t)}catch(e){}return e}(),gestures:"ongesturestart"in o}),p=function(e){void 0===e&&(e={});var t=this;t.params=e,t.eventsListeners={},t.params&&t.params.on&&Object.keys(t.params.on).forEach(function(e){t.on(e,t.params.on[e])})},c={components:{configurable:!0}};p.prototype.on=function(e,t){var i=this;return"function"!=typeof t?i:(e.split(" ").forEach(function(e){i.eventsListeners[e]||(i.eventsListeners[e]=[]),i.eventsListeners[e].push(t)}),i)},p.prototype.once=function(e,t){var i=this;if("function"!=typeof t)return i;return i.on(e,function s(){for(var a=[],n=arguments.length;n--;)a[n]=arguments[n];t.apply(i,a),i.off(e,s)})},p.prototype.off=function(e,t){var i=this;return e.split(" ").forEach(function(e){void 0===t?i.eventsListeners[e]=[]:i.eventsListeners[e].forEach(function(s,a){s===t&&i.eventsListeners[e].splice(a,1)})}),i},p.prototype.emit=function(){for(var e=[],t=arguments.length;t--;)e[t]=arguments[t];var i,s,a,n=this;return n.eventsListeners?("string"==typeof e[0]||Array.isArray(e[0])?(i=e[0],s=e.slice(1,e.length),a=n):(i=e[0].events,s=e[0].data,a=e[0].context||n),(Array.isArray(i)?i:i.split(" ")).forEach(function(e){if(n.eventsListeners[e]){var t=[];n.eventsListeners[e].forEach(function(e){t.push(e)}),t.forEach(function(e){e.apply(a,s)})}}),n):n},p.prototype.useModulesParams=function(e){var t=this;t.modules&&Object.keys(t.modules).forEach(function(i){var s=t.modules[i];s.params&&l.extend(e,s.params)})},p.prototype.useModules=function(e){void 0===e&&(e={});var t=this;t.modules&&Object.keys(t.modules).forEach(function(i){var s=t.modules[i],a=e[i]||{};s.instance&&Object.keys(s.instance).forEach(function(e){var i=s.instance[e];t[e]="function"==typeof i?i.bind(t):i}),s.on&&t.on&&Object.keys(s.on).forEach(function(e){t.on(e,s.on[e])}),s.create&&s.create.bind(t)(a)})},c.components.set=function(e){this.use&&this.use(e)},p.installModule=function(e){for(var t=[],i=arguments.length-1;i-- >0;)t[i]=arguments[i+1];var s=this;s.prototype.modules||(s.prototype.modules={});var a=e.name||Object.keys(s.prototype.modules).length+"_"+l.now();return s.prototype.modules[a]=e,e.proto&&Object.keys(e.proto).forEach(function(t){s.prototype[t]=e.proto[t]}),e.static&&Object.keys(e.static).forEach(function(t){s[t]=e.static[t]}),e.install&&e.install.apply(s,t),s},p.use=function(e){for(var t=[],i=arguments.length-1;i-- >0;)t[i]=arguments[i+1];var s=this;return Array.isArray(e)?(e.forEach(function(e){return s.installModule(e)}),s):s.installModule.apply(s,[e].concat(t))},Object.defineProperties(p,c);var u={updateSize:function(){var e,t,i=this.$el;e=void 0!==this.params.width?this.params.width:i[0].clientWidth,t=void 0!==this.params.height?this.params.height:i[0].clientHeight,0===e&&this.isHorizontal()||0===t&&this.isVertical()||(e=e-parseInt(i.css("padding-left"),10)-parseInt(i.css("padding-right"),10),t=t-parseInt(i.css("padding-top"),10)-parseInt(i.css("padding-bottom"),10),l.extend(this,{width:e,height:t,size:this.isHorizontal()?e:t}))},updateSlides:function(){var e=this.params,t=this.$wrapperEl,i=this.size,s=this.rtl,a=this.wrongRTL,n=t.children("."+this.params.slideClass),r=this.virtual&&e.virtual.enabled?this.virtual.slides.length:n.length,o=[],d=[],p=[],c=e.slidesOffsetBefore;"function"==typeof c&&(c=e.slidesOffsetBefore.call(this));var u=e.slidesOffsetAfter;"function"==typeof u&&(u=e.slidesOffsetAfter.call(this));var v=r,f=this.snapGrid.length,m=this.snapGrid.length,g=e.spaceBetween,w=-c,b=0,y=0;if(void 0!==i){var x,C;"string"==typeof g&&g.indexOf("%")>=0&&(g=parseFloat(g.replace("%",""))/100*i),this.virtualSize=-g,s?n.css({marginLeft:"",marginTop:""}):n.css({marginRight:"",marginBottom:""}),e.slidesPerColumn>1&&(x=Math.floor(r/e.slidesPerColumn)===r/this.params.slidesPerColumn?r:Math.ceil(r/e.slidesPerColumn)*e.slidesPerColumn,"auto"!==e.slidesPerView&&"row"===e.slidesPerColumnFill&&(x=Math.max(x,e.slidesPerView*e.slidesPerColumn)));for(var T,S=e.slidesPerColumn,E=x/S,M=E-(e.slidesPerColumn*E-r),k=0;k<r;k+=1){C=0;var P=n.eq(k);if(e.slidesPerColumn>1){var L=void 0,z=void 0,I=void 0;"column"===e.slidesPerColumnFill?(I=k-(z=Math.floor(k/S))*S,(z>M||z===M&&I===S-1)&&(I+=1)>=S&&(I=0,z+=1),L=z+I*x/S,P.css({"-webkit-box-ordinal-group":L,"-moz-box-ordinal-group":L,"-ms-flex-order":L,"-webkit-order":L,order:L})):z=k-(I=Math.floor(k/E))*E,P.css("margin-"+(this.isHorizontal()?"top":"left"),0!==I&&e.spaceBetween&&e.spaceBetween+"px").attr("data-swiper-column",z).attr("data-swiper-row",I)}"none"!==P.css("display")&&("auto"===e.slidesPerView?(C=this.isHorizontal()?P.outerWidth(!0):P.outerHeight(!0),e.roundLengths&&(C=Math.floor(C))):(C=(i-(e.slidesPerView-1)*g)/e.slidesPerView,e.roundLengths&&(C=Math.floor(C)),n[k]&&(this.isHorizontal()?n[k].style.width=C+"px":n[k].style.height=C+"px")),n[k]&&(n[k].swiperSlideSize=C),p.push(C),e.centeredSlides?(w=w+C/2+b/2+g,0===b&&0!==k&&(w=w-i/2-g),0===k&&(w=w-i/2-g),Math.abs(w)<.001&&(w=0),y%e.slidesPerGroup==0&&o.push(w),d.push(w)):(y%e.slidesPerGroup==0&&o.push(w),d.push(w),w=w+C+g),this.virtualSize+=C+g,b=C,y+=1)}if(this.virtualSize=Math.max(this.virtualSize,i)+u,s&&a&&("slide"===e.effect||"coverflow"===e.effect)&&t.css({width:this.virtualSize+e.spaceBetween+"px"}),h.flexbox&&!e.setWrapperSize||(this.isHorizontal()?t.css({width:this.virtualSize+e.spaceBetween+"px"}):t.css({height:this.virtualSize+e.spaceBetween+"px"})),e.slidesPerColumn>1&&(this.virtualSize=(C+e.spaceBetween)*x,this.virtualSize=Math.ceil(this.virtualSize/e.slidesPerColumn)-e.spaceBetween,this.isHorizontal()?t.css({width:this.virtualSize+e.spaceBetween+"px"}):t.css({height:this.virtualSize+e.spaceBetween+"px"}),e.centeredSlides)){T=[];for(var O=0;O<o.length;O+=1)o[O]<this.virtualSize+o[0]&&T.push(o[O]);o=T}if(!e.centeredSlides){T=[];for(var A=0;A<o.length;A+=1)o[A]<=this.virtualSize-i&&T.push(o[A]);o=T,Math.floor(this.virtualSize-i)-Math.floor(o[o.length-1])>1&&o.push(this.virtualSize-i)}0===o.length&&(o=[0]),0!==e.spaceBetween&&(this.isHorizontal()?s?n.css({marginLeft:g+"px"}):n.css({marginRight:g+"px"}):n.css({marginBottom:g+"px"})),l.extend(this,{slides:n,snapGrid:o,slidesGrid:d,slidesSizesGrid:p}),r!==v&&this.emit("slidesLengthChange"),o.length!==f&&(this.params.watchOverflow&&this.checkOverflow(),this.emit("snapGridLengthChange")),d.length!==m&&this.emit("slidesGridLengthChange"),(e.watchSlidesProgress||e.watchSlidesVisibility)&&this.updateSlidesOffset()}},updateAutoHeight:function(){var e,t=[],i=0;if("auto"!==this.params.slidesPerView&&this.params.slidesPerView>1)for(e=0;e<Math.ceil(this.params.slidesPerView);e+=1){var s=this.activeIndex+e;if(s>this.slides.length)break;t.push(this.slides.eq(s)[0])}else t.push(this.slides.eq(this.activeIndex)[0]);for(e=0;e<t.length;e+=1)if(void 0!==t[e]){var a=t[e].offsetHeight;i=a>i?a:i}i&&this.$wrapperEl.css("height",i+"px")},updateSlidesOffset:function(){for(var e=this.slides,t=0;t<e.length;t+=1)e[t].swiperSlideOffset=this.isHorizontal()?e[t].offsetLeft:e[t].offsetTop},updateSlidesProgress:function(e){void 0===e&&(e=this.translate||0);var t=this.params,i=this.slides,s=this.rtl;if(0!==i.length){void 0===i[0].swiperSlideOffset&&this.updateSlidesOffset();var a=-e;s&&(a=e),i.removeClass(t.slideVisibleClass);for(var n=0;n<i.length;n+=1){var r=i[n],o=(a+(t.centeredSlides?this.minTranslate():0)-r.swiperSlideOffset)/(r.swiperSlideSize+t.spaceBetween);if(t.watchSlidesVisibility){var l=-(a-r.swiperSlideOffset),d=l+this.slidesSizesGrid[n];(l>=0&&l<this.size||d>0&&d<=this.size||l<=0&&d>=this.size)&&i.eq(n).addClass(t.slideVisibleClass)}r.progress=s?-o:o}}},updateProgress:function(e){void 0===e&&(e=this.translate||0);var t=this.params,i=this.maxTranslate()-this.minTranslate(),s=this.progress,a=this.isBeginning,n=this.isEnd,r=a,o=n;0===i?(s=0,a=!0,n=!0):(a=(s=(e-this.minTranslate())/i)<=0,n=s>=1),l.extend(this,{progress:s,isBeginning:a,isEnd:n}),(t.watchSlidesProgress||t.watchSlidesVisibility)&&this.updateSlidesProgress(e),a&&!r&&this.emit("reachBeginning toEdge"),n&&!o&&this.emit("reachEnd toEdge"),(r&&!a||o&&!n)&&this.emit("fromEdge"),this.emit("progress",s)},updateSlidesClasses:function(){var e,t=this.slides,i=this.params,s=this.$wrapperEl,a=this.activeIndex,n=this.realIndex,r=this.virtual&&i.virtual.enabled;t.removeClass(i.slideActiveClass+" "+i.slideNextClass+" "+i.slidePrevClass+" "+i.slideDuplicateActiveClass+" "+i.slideDuplicateNextClass+" "+i.slideDuplicatePrevClass),(e=r?this.$wrapperEl.find("."+i.slideClass+'[data-swiper-slide-index="'+a+'"]'):t.eq(a)).addClass(i.slideActiveClass),i.loop&&(e.hasClass(i.slideDuplicateClass)?s.children("."+i.slideClass+":not(."+i.slideDuplicateClass+')[data-swiper-slide-index="'+n+'"]').addClass(i.slideDuplicateActiveClass):s.children("."+i.slideClass+"."+i.slideDuplicateClass+'[data-swiper-slide-index="'+n+'"]').addClass(i.slideDuplicateActiveClass));var o=e.nextAll("."+i.slideClass).eq(0).addClass(i.slideNextClass);i.loop&&0===o.length&&(o=t.eq(0)).addClass(i.slideNextClass);var l=e.prevAll("."+i.slideClass).eq(0).addClass(i.slidePrevClass);i.loop&&0===l.length&&(l=t.eq(-1)).addClass(i.slidePrevClass),i.loop&&(o.hasClass(i.slideDuplicateClass)?s.children("."+i.slideClass+":not(."+i.slideDuplicateClass+')[data-swiper-slide-index="'+o.attr("data-swiper-slide-index")+'"]').addClass(i.slideDuplicateNextClass):s.children("."+i.slideClass+"."+i.slideDuplicateClass+'[data-swiper-slide-index="'+o.attr("data-swiper-slide-index")+'"]').addClass(i.slideDuplicateNextClass),l.hasClass(i.slideDuplicateClass)?s.children("."+i.slideClass+":not(."+i.slideDuplicateClass+')[data-swiper-slide-index="'+l.attr("data-swiper-slide-index")+'"]').addClass(i.slideDuplicatePrevClass):s.children("."+i.slideClass+"."+i.slideDuplicateClass+'[data-swiper-slide-index="'+l.attr("data-swiper-slide-index")+'"]').addClass(i.slideDuplicatePrevClass))},updateActiveIndex:function(e){var t,i=this.rtl?this.translate:-this.translate,s=this.slidesGrid,a=this.snapGrid,n=this.params,r=this.activeIndex,o=this.realIndex,d=this.snapIndex,h=e;if(void 0===h){for(var p=0;p<s.length;p+=1)void 0!==s[p+1]?i>=s[p]&&i<s[p+1]-(s[p+1]-s[p])/2?h=p:i>=s[p]&&i<s[p+1]&&(h=p+1):i>=s[p]&&(h=p);n.normalizeSlideIndex&&(h<0||void 0===h)&&(h=0)}if((t=a.indexOf(i)>=0?a.indexOf(i):Math.floor(h/n.slidesPerGroup))>=a.length&&(t=a.length-1),h!==r){var c=parseInt(this.slides.eq(h).attr("data-swiper-slide-index")||h,10);l.extend(this,{snapIndex:t,realIndex:c,previousIndex:r,activeIndex:h}),this.emit("activeIndexChange"),this.emit("snapIndexChange"),o!==c&&this.emit("realIndexChange"),this.emit("slideChange")}else t!==d&&(this.snapIndex=t,this.emit("snapIndexChange"))},updateClickedSlide:function(e){var i=this.params,s=t(e.target).closest("."+i.slideClass)[0],a=!1;if(s)for(var n=0;n<this.slides.length;n+=1)this.slides[n]===s&&(a=!0);if(!s||!a)return this.clickedSlide=void 0,void(this.clickedIndex=void 0);this.clickedSlide=s,this.virtual&&this.params.virtual.enabled?this.clickedIndex=parseInt(t(s).attr("data-swiper-slide-index"),10):this.clickedIndex=t(s).index(),i.slideToClickedSlide&&void 0!==this.clickedIndex&&this.clickedIndex!==this.activeIndex&&this.slideToClickedSlide()}},v={getTranslate:function(e){void 0===e&&(e=this.isHorizontal()?"x":"y");var t=this.params,i=this.rtl,s=this.translate,a=this.$wrapperEl;if(t.virtualTranslate)return i?-s:s;var n=l.getTranslate(a[0],e);return i&&(n=-n),n||0},setTranslate:function(e,t){var i=this.rtl,s=this.params,a=this.$wrapperEl,n=this.progress,r=0,o=0;this.isHorizontal()?r=i?-e:e:o=e,s.roundLengths&&(r=Math.floor(r),o=Math.floor(o)),s.virtualTranslate||(h.transforms3d?a.transform("translate3d("+r+"px, "+o+"px, 0px)"):a.transform("translate("+r+"px, "+o+"px)")),this.translate=this.isHorizontal()?r:o;var l=this.maxTranslate()-this.minTranslate();(0===l?0:(e-this.minTranslate())/l)!==n&&this.updateProgress(e),this.emit("setTranslate",this.translate,t)},minTranslate:function(){return-this.snapGrid[0]},maxTranslate:function(){return-this.snapGrid[this.snapGrid.length-1]}},f={setTransition:function(e,t){this.$wrapperEl.transition(e),this.emit("setTransition",e,t)},transitionStart:function(e,t){void 0===e&&(e=!0);var i=this.activeIndex,s=this.params,a=this.previousIndex;s.autoHeight&&this.updateAutoHeight();var n=t;if(n||(n=i>a?"next":i<a?"prev":"reset"),this.emit("transitionStart"),e&&i!==a){if("reset"===n)return void this.emit("slideResetTransitionStart");this.emit("slideChangeTransitionStart"),"next"===n?this.emit("slideNextTransitionStart"):this.emit("slidePrevTransitionStart")}},transitionEnd:function(e,t){void 0===e&&(e=!0);var i=this.activeIndex,s=this.previousIndex;this.animating=!1,this.setTransition(0);var a=t;if(a||(a=i>s?"next":i<s?"prev":"reset"),this.emit("transitionEnd"),e&&i!==s){if("reset"===a)return void this.emit("slideResetTransitionEnd");this.emit("slideChangeTransitionEnd"),"next"===a?this.emit("slideNextTransitionEnd"):this.emit("slidePrevTransitionEnd")}}},m={slideTo:function(e,t,i,s){void 0===e&&(e=0),void 0===t&&(t=this.params.speed),void 0===i&&(i=!0);var a=this,n=e;n<0&&(n=0);var r=a.params,o=a.snapGrid,l=a.slidesGrid,d=a.previousIndex,p=a.activeIndex,c=a.rtl,u=a.$wrapperEl,v=Math.floor(n/r.slidesPerGroup);v>=o.length&&(v=o.length-1),(p||r.initialSlide||0)===(d||0)&&i&&a.emit("beforeSlideChangeStart");var f,m=-o[v];if(a.updateProgress(m),r.normalizeSlideIndex)for(var g=0;g<l.length;g+=1)-Math.floor(100*m)>=Math.floor(100*l[g])&&(n=g);if(a.initialized&&n!==p){if(!a.allowSlideNext&&m<a.translate&&m<a.minTranslate())return!1;if(!a.allowSlidePrev&&m>a.translate&&m>a.maxTranslate()&&(p||0)!==n)return!1}return f=n>p?"next":n<p?"prev":"reset",c&&-m===a.translate||!c&&m===a.translate?(a.updateActiveIndex(n),r.autoHeight&&a.updateAutoHeight(),a.updateSlidesClasses(),"slide"!==r.effect&&a.setTranslate(m),"reset"!==f&&(a.transitionStart(i,f),a.transitionEnd(i,f)),!1):(0!==t&&h.transition?(a.setTransition(t),a.setTranslate(m),a.updateActiveIndex(n),a.updateSlidesClasses(),a.emit("beforeTransitionStart",t,s),a.transitionStart(i,f),a.animating||(a.animating=!0,u.transitionEnd(function(){a&&!a.destroyed&&a.transitionEnd(i,f)}))):(a.setTransition(0),a.setTranslate(m),a.updateActiveIndex(n),a.updateSlidesClasses(),a.emit("beforeTransitionStart",t,s),a.transitionStart(i,f),a.transitionEnd(i,f)),!0)},slideNext:function(e,t,i){void 0===e&&(e=this.params.speed),void 0===t&&(t=!0);var s=this.params,a=this.animating;return s.loop?!a&&(this.loopFix(),this._clientLeft=this.$wrapperEl[0].clientLeft,this.slideTo(this.activeIndex+s.slidesPerGroup,e,t,i)):this.slideTo(this.activeIndex+s.slidesPerGroup,e,t,i)},slidePrev:function(e,t,i){void 0===e&&(e=this.params.speed),void 0===t&&(t=!0);var s=this.params,a=this.animating;return s.loop?!a&&(this.loopFix(),this._clientLeft=this.$wrapperEl[0].clientLeft,this.slideTo(this.activeIndex-1,e,t,i)):this.slideTo(this.activeIndex-1,e,t,i)},slideReset:function(e,t,i){void 0===e&&(e=this.params.speed),void 0===t&&(t=!0);return this.slideTo(this.activeIndex,e,t,i)},slideToClickedSlide:function(){var e,i=this,s=i.params,a=i.$wrapperEl,n="auto"===s.slidesPerView?i.slidesPerViewDynamic():s.slidesPerView,r=i.clickedIndex;if(s.loop){if(i.animating)return;e=parseInt(t(i.clickedSlide).attr("data-swiper-slide-index"),10),s.centeredSlides?r<i.loopedSlides-n/2||r>i.slides.length-i.loopedSlides+n/2?(i.loopFix(),r=a.children("."+s.slideClass+'[data-swiper-slide-index="'+e+'"]:not(.'+s.slideDuplicateClass+")").eq(0).index(),l.nextTick(function(){i.slideTo(r)})):i.slideTo(r):r>i.slides.length-n?(i.loopFix(),r=a.children("."+s.slideClass+'[data-swiper-slide-index="'+e+'"]:not(.'+s.slideDuplicateClass+")").eq(0).index(),l.nextTick(function(){i.slideTo(r)})):i.slideTo(r)}else i.slideTo(r)}},g={loopCreate:function(){var e=this,i=e.params,s=e.$wrapperEl;s.children("."+i.slideClass+"."+i.slideDuplicateClass).remove();var a=s.children("."+i.slideClass);if(i.loopFillGroupWithBlank){var n=i.slidesPerGroup-a.length%i.slidesPerGroup;if(n!==i.slidesPerGroup){for(var r=0;r<n;r+=1){var o=t(d.createElement("div")).addClass(i.slideClass+" "+i.slideBlankClass);s.append(o)}a=s.children("."+i.slideClass)}}"auto"!==i.slidesPerView||i.loopedSlides||(i.loopedSlides=a.length),e.loopedSlides=parseInt(i.loopedSlides||i.slidesPerView,10),e.loopedSlides+=i.loopAdditionalSlides,e.loopedSlides>a.length&&(e.loopedSlides=a.length);var l=[],h=[];a.each(function(i,s){var n=t(s);i<e.loopedSlides&&h.push(s),i<a.length&&i>=a.length-e.loopedSlides&&l.push(s),n.attr("data-swiper-slide-index",i)});for(var p=0;p<h.length;p+=1)s.append(t(h[p].cloneNode(!0)).addClass(i.slideDuplicateClass));for(var c=l.length-1;c>=0;c-=1)s.prepend(t(l[c].cloneNode(!0)).addClass(i.slideDuplicateClass))},loopFix:function(){var e,t=this.params,i=this.activeIndex,s=this.slides,a=this.loopedSlides,n=this.allowSlidePrev,r=this.allowSlideNext,o=this.snapGrid,l=this.rtl;this.allowSlidePrev=!0,this.allowSlideNext=!0;var d=-o[i]-this.getTranslate();i<a?(e=s.length-3*a+i,e+=a,this.slideTo(e,0,!1,!0)&&0!==d&&this.setTranslate((l?-this.translate:this.translate)-d)):("auto"===t.slidesPerView&&i>=2*a||i>s.length-2*t.slidesPerView)&&(e=-s.length+i+a,e+=a,this.slideTo(e,0,!1,!0)&&0!==d&&this.setTranslate((l?-this.translate:this.translate)-d));this.allowSlidePrev=n,this.allowSlideNext=r},loopDestroy:function(){var e=this.$wrapperEl,t=this.params,i=this.slides;e.children("."+t.slideClass+"."+t.slideDuplicateClass).remove(),i.removeAttr("data-swiper-slide-index")}},w={setGrabCursor:function(e){if(!h.touch&&this.params.simulateTouch){var t=this.el;t.style.cursor="move",t.style.cursor=e?"-webkit-grabbing":"-webkit-grab",t.style.cursor=e?"-moz-grabbin":"-moz-grab",t.style.cursor=e?"grabbing":"grab"}},unsetGrabCursor:function(){h.touch||(this.el.style.cursor="")}},b={appendSlide:function(e){var t=this.$wrapperEl,i=this.params;if(i.loop&&this.loopDestroy(),"object"==typeof e&&"length"in e)for(var s=0;s<e.length;s+=1)e[s]&&t.append(e[s]);else t.append(e);i.loop&&this.loopCreate(),i.observer&&h.observer||this.update()},prependSlide:function(e){var t=this.params,i=this.$wrapperEl,s=this.activeIndex;t.loop&&this.loopDestroy();var a=s+1;if("object"==typeof e&&"length"in e){for(var n=0;n<e.length;n+=1)e[n]&&i.prepend(e[n]);a=s+e.length}else i.prepend(e);t.loop&&this.loopCreate(),t.observer&&h.observer||this.update(),this.slideTo(a,0,!1)},removeSlide:function(e){var t=this.params,i=this.$wrapperEl,s=this.activeIndex;t.loop&&(this.loopDestroy(),this.slides=i.children("."+t.slideClass));var a,n=s;if("object"==typeof e&&"length"in e){for(var r=0;r<e.length;r+=1)a=e[r],this.slides[a]&&this.slides.eq(a).remove(),a<n&&(n-=1);n=Math.max(n,0)}else a=e,this.slides[a]&&this.slides.eq(a).remove(),a<n&&(n-=1),n=Math.max(n,0);t.loop&&this.loopCreate(),t.observer&&h.observer||this.update(),t.loop?this.slideTo(n+this.loopedSlides,0,!1):this.slideTo(n,0,!1)},removeAllSlides:function(){for(var e=[],t=0;t<this.slides.length;t+=1)e.push(t);this.removeSlide(e)}},y=function(){var e=o.navigator.userAgent,t={ios:!1,android:!1,androidChrome:!1,desktop:!1,windows:!1,iphone:!1,ipod:!1,ipad:!1,cordova:o.cordova||o.phonegap,phonegap:o.cordova||o.phonegap},i=e.match(/(Windows Phone);?[\s\/]+([\d.]+)?/),s=e.match(/(Android);?[\s\/]+([\d.]+)?/),a=e.match(/(iPad).*OS\s([\d_]+)/),n=e.match(/(iPod)(.*OS\s([\d_]+))?/),r=!a&&e.match(/(iPhone\sOS|iOS)\s([\d_]+)/);if(i&&(t.os="windows",t.osVersion=i[2],t.windows=!0),s&&!i&&(t.os="android",t.osVersion=s[2],t.android=!0,t.androidChrome=e.toLowerCase().indexOf("chrome")>=0),(a||r||n)&&(t.os="ios",t.ios=!0),r&&!n&&(t.osVersion=r[2].replace(/_/g,"."),t.iphone=!0),a&&(t.osVersion=a[2].replace(/_/g,"."),t.ipad=!0),n&&(t.osVersion=n[3]?n[3].replace(/_/g,"."):null,t.iphone=!0),t.ios&&t.osVersion&&e.indexOf("Version/")>=0&&"10"===t.osVersion.split(".")[0]&&(t.osVersion=e.toLowerCase().split("version/")[1].split(" ")[0]),t.desktop=!(t.os||t.android||t.webView),t.webView=(r||a||n)&&e.match(/.*AppleWebKit(?!.*Safari)/i),t.os&&"ios"===t.os){var l=t.osVersion.split("."),h=d.querySelector('meta[name="viewport"]');t.minimalUi=!t.webView&&(n||r)&&(1*l[0]==7?1*l[1]>=1:1*l[0]>7)&&h&&h.getAttribute("content").indexOf("minimal-ui")>=0}return t.pixelRatio=o.devicePixelRatio||1,t}(),x=function(e){var i=this.touchEventsData,s=this.params,a=this.touches,n=e;if(n.originalEvent&&(n=n.originalEvent),i.isTouchEvent="touchstart"===n.type,(i.isTouchEvent||!("which"in n)||3!==n.which)&&(!i.isTouched||!i.isMoved))if(s.noSwiping&&t(n.target).closest(s.noSwipingSelector?s.noSwipingSelector:"."+s.noSwipingClass)[0])this.allowClick=!0;else if(!s.swipeHandler||t(n).closest(s.swipeHandler)[0]){a.currentX="touchstart"===n.type?n.targetTouches[0].pageX:n.pageX,a.currentY="touchstart"===n.type?n.targetTouches[0].pageY:n.pageY;var r=a.currentX,o=a.currentY;if(!(y.ios&&!y.cordova&&s.iOSEdgeSwipeDetection&&r<=s.iOSEdgeSwipeThreshold&&r>=window.screen.width-s.iOSEdgeSwipeThreshold)){if(l.extend(i,{isTouched:!0,isMoved:!1,allowTouchCallbacks:!0,isScrolling:void 0,startMoving:void 0}),a.startX=r,a.startY=o,i.touchStartTime=l.now(),this.allowClick=!0,this.updateSize(),this.swipeDirection=void 0,s.threshold>0&&(i.allowThresholdMove=!1),"touchstart"!==n.type){var h=!0;t(n.target).is(i.formElements)&&(h=!1),d.activeElement&&t(d.activeElement).is(i.formElements)&&d.activeElement.blur(),h&&this.allowTouchMove&&n.preventDefault()}this.emit("touchStart",n)}}},C=function(e){var i=this.touchEventsData,s=this.params,a=this.touches,n=this.rtl,r=e;if(r.originalEvent&&(r=r.originalEvent),!i.isTouchEvent||"mousemove"!==r.type){var o="touchmove"===r.type?r.targetTouches[0].pageX:r.pageX,h="touchmove"===r.type?r.targetTouches[0].pageY:r.pageY;if(r.preventedByNestedSwiper)return a.startX=o,void(a.startY=h);if(!this.allowTouchMove)return this.allowClick=!1,void(i.isTouched&&(l.extend(a,{startX:o,startY:h,currentX:o,currentY:h}),i.touchStartTime=l.now()));if(i.isTouchEvent&&s.touchReleaseOnEdges&&!s.loop)if(this.isVertical()){if(h<a.startY&&this.translate<=this.maxTranslate()||h>a.startY&&this.translate>=this.minTranslate())return i.isTouched=!1,void(i.isMoved=!1)}else if(o<a.startX&&this.translate<=this.maxTranslate()||o>a.startX&&this.translate>=this.minTranslate())return;if(i.isTouchEvent&&d.activeElement&&r.target===d.activeElement&&t(r.target).is(i.formElements))return i.isMoved=!0,void(this.allowClick=!1);if(i.allowTouchCallbacks&&this.emit("touchMove",r),!(r.targetTouches&&r.targetTouches.length>1)){a.currentX=o,a.currentY=h;var p,c=a.currentX-a.startX,u=a.currentY-a.startY;if(void 0===i.isScrolling)this.isHorizontal()&&a.currentY===a.startY||this.isVertical()&&a.currentX===a.startX?i.isScrolling=!1:c*c+u*u>=25&&(p=180*Math.atan2(Math.abs(u),Math.abs(c))/Math.PI,i.isScrolling=this.isHorizontal()?p>s.touchAngle:90-p>s.touchAngle);if(i.isScrolling&&this.emit("touchMoveOpposite",r),"undefined"==typeof startMoving&&(a.currentX===a.startX&&a.currentY===a.startY||(i.startMoving=!0)),i.isTouched)if(i.isScrolling)i.isTouched=!1;else if(i.startMoving){this.allowClick=!1,r.preventDefault(),s.touchMoveStopPropagation&&!s.nested&&r.stopPropagation(),i.isMoved||(s.loop&&this.loopFix(),i.startTranslate=this.getTranslate(),this.setTransition(0),this.animating&&this.$wrapperEl.trigger("webkitTransitionEnd transitionend"),i.allowMomentumBounce=!1,!s.grabCursor||!0!==this.allowSlideNext&&!0!==this.allowSlidePrev||this.setGrabCursor(!0),this.emit("sliderFirstMove",r)),this.emit("sliderMove",r),i.isMoved=!0;var v=this.isHorizontal()?c:u;a.diff=v,v*=s.touchRatio,n&&(v=-v),this.swipeDirection=v>0?"prev":"next",i.currentTranslate=v+i.startTranslate;var f=!0,m=s.resistanceRatio;if(s.touchReleaseOnEdges&&(m=0),v>0&&i.currentTranslate>this.minTranslate()?(f=!1,s.resistance&&(i.currentTranslate=this.minTranslate()-1+Math.pow(-this.minTranslate()+i.startTranslate+v,m))):v<0&&i.currentTranslate<this.maxTranslate()&&(f=!1,s.resistance&&(i.currentTranslate=this.maxTranslate()+1-Math.pow(this.maxTranslate()-i.startTranslate-v,m))),f&&(r.preventedByNestedSwiper=!0),!this.allowSlideNext&&"next"===this.swipeDirection&&i.currentTranslate<i.startTranslate&&(i.currentTranslate=i.startTranslate),!this.allowSlidePrev&&"prev"===this.swipeDirection&&i.currentTranslate>i.startTranslate&&(i.currentTranslate=i.startTranslate),s.threshold>0){if(!(Math.abs(v)>s.threshold||i.allowThresholdMove))return void(i.currentTranslate=i.startTranslate);if(!i.allowThresholdMove)return i.allowThresholdMove=!0,a.startX=a.currentX,a.startY=a.currentY,i.currentTranslate=i.startTranslate,void(a.diff=this.isHorizontal()?a.currentX-a.startX:a.currentY-a.startY)}s.followFinger&&((s.freeMode||s.watchSlidesProgress||s.watchSlidesVisibility)&&(this.updateActiveIndex(),this.updateSlidesClasses()),s.freeMode&&(0===i.velocities.length&&i.velocities.push({position:a[this.isHorizontal()?"startX":"startY"],time:i.touchStartTime}),i.velocities.push({position:a[this.isHorizontal()?"currentX":"currentY"],time:l.now()})),this.updateProgress(i.currentTranslate),this.setTranslate(i.currentTranslate))}}}},T=function(e){var t=this,i=t.touchEventsData,s=t.params,a=t.touches,n=t.rtl,r=t.$wrapperEl,o=t.slidesGrid,d=t.snapGrid,h=e;if(h.originalEvent&&(h=h.originalEvent),i.allowTouchCallbacks&&t.emit("touchEnd",h),i.allowTouchCallbacks=!1,i.isTouched){s.grabCursor&&i.isMoved&&i.isTouched&&(!0===t.allowSlideNext||!0===t.allowSlidePrev)&&t.setGrabCursor(!1);var p,c=l.now(),u=c-i.touchStartTime;if(t.allowClick&&(t.updateClickedSlide(h),t.emit("tap",h),u<300&&c-i.lastClickTime>300&&(i.clickTimeout&&clearTimeout(i.clickTimeout),i.clickTimeout=l.nextTick(function(){t&&!t.destroyed&&t.emit("click",h)},300)),u<300&&c-i.lastClickTime<300&&(i.clickTimeout&&clearTimeout(i.clickTimeout),t.emit("doubleTap",h))),i.lastClickTime=l.now(),l.nextTick(function(){t.destroyed||(t.allowClick=!0)}),!i.isTouched||!i.isMoved||!t.swipeDirection||0===a.diff||i.currentTranslate===i.startTranslate)return i.isTouched=!1,void(i.isMoved=!1);if(i.isTouched=!1,i.isMoved=!1,p=s.followFinger?n?t.translate:-t.translate:-i.currentTranslate,s.freeMode){if(p<-t.minTranslate())return void t.slideTo(t.activeIndex);if(p>-t.maxTranslate())return void(t.slides.length<d.length?t.slideTo(d.length-1):t.slideTo(t.slides.length-1));if(s.freeModeMomentum){if(i.velocities.length>1){var v=i.velocities.pop(),f=i.velocities.pop(),m=v.position-f.position,g=v.time-f.time;t.velocity=m/g,t.velocity/=2,Math.abs(t.velocity)<s.freeModeMinimumVelocity&&(t.velocity=0),(g>150||l.now()-v.time>300)&&(t.velocity=0)}else t.velocity=0;t.velocity*=s.freeModeMomentumVelocityRatio,i.velocities.length=0;var w=1e3*s.freeModeMomentumRatio,b=t.velocity*w,y=t.translate+b;n&&(y=-y);var x,C=!1,T=20*Math.abs(t.velocity)*s.freeModeMomentumBounceRatio;if(y<t.maxTranslate())s.freeModeMomentumBounce?(y+t.maxTranslate()<-T&&(y=t.maxTranslate()-T),x=t.maxTranslate(),C=!0,i.allowMomentumBounce=!0):y=t.maxTranslate();else if(y>t.minTranslate())s.freeModeMomentumBounce?(y-t.minTranslate()>T&&(y=t.minTranslate()+T),x=t.minTranslate(),C=!0,i.allowMomentumBounce=!0):y=t.minTranslate();else if(s.freeModeSticky){for(var S,E=0;E<d.length;E+=1)if(d[E]>-y){S=E;break}y=-(y=Math.abs(d[S]-y)<Math.abs(d[S-1]-y)||"next"===t.swipeDirection?d[S]:d[S-1])}if(0!==t.velocity)w=n?Math.abs((-y-t.translate)/t.velocity):Math.abs((y-t.translate)/t.velocity);else if(s.freeModeSticky)return void t.slideReset();s.freeModeMomentumBounce&&C?(t.updateProgress(x),t.setTransition(w),t.setTranslate(y),t.transitionStart(!0,t.swipeDirection),t.animating=!0,r.transitionEnd(function(){t&&!t.destroyed&&i.allowMomentumBounce&&(t.emit("momentumBounce"),t.setTransition(s.speed),t.setTranslate(x),r.transitionEnd(function(){t&&!t.destroyed&&t.transitionEnd()}))})):t.velocity?(t.updateProgress(y),t.setTransition(w),t.setTranslate(y),t.transitionStart(!0,t.swipeDirection),t.animating||(t.animating=!0,r.transitionEnd(function(){t&&!t.destroyed&&t.transitionEnd()}))):t.updateProgress(y),t.updateActiveIndex(),t.updateSlidesClasses()}(!s.freeModeMomentum||u>=s.longSwipesMs)&&(t.updateProgress(),t.updateActiveIndex(),t.updateSlidesClasses())}else{for(var M=0,k=t.slidesSizesGrid[0],P=0;P<o.length;P+=s.slidesPerGroup)void 0!==o[P+s.slidesPerGroup]?p>=o[P]&&p<o[P+s.slidesPerGroup]&&(M=P,k=o[P+s.slidesPerGroup]-o[P]):p>=o[P]&&(M=P,k=o[o.length-1]-o[o.length-2]);var L=(p-o[M])/k;if(u>s.longSwipesMs){if(!s.longSwipes)return void t.slideTo(t.activeIndex);"next"===t.swipeDirection&&(L>=s.longSwipesRatio?t.slideTo(M+s.slidesPerGroup):t.slideTo(M)),"prev"===t.swipeDirection&&(L>1-s.longSwipesRatio?t.slideTo(M+s.slidesPerGroup):t.slideTo(M))}else{if(!s.shortSwipes)return void t.slideTo(t.activeIndex);"next"===t.swipeDirection&&t.slideTo(M+s.slidesPerGroup),"prev"===t.swipeDirection&&t.slideTo(M)}}}},S=function(){var e=this.params,t=this.el;if(!t||0!==t.offsetWidth){e.breakpoints&&this.setBreakpoint();var i=this.allowSlideNext,s=this.allowSlidePrev;if(this.allowSlideNext=!0,this.allowSlidePrev=!0,this.updateSize(),this.updateSlides(),e.freeMode){var a=Math.min(Math.max(this.translate,this.maxTranslate()),this.minTranslate());this.setTranslate(a),this.updateActiveIndex(),this.updateSlidesClasses(),e.autoHeight&&this.updateAutoHeight()}else this.updateSlidesClasses(),("auto"===e.slidesPerView||e.slidesPerView>1)&&this.isEnd&&!this.params.centeredSlides?this.slideTo(this.slides.length-1,0,!1,!0):this.slideTo(this.activeIndex,0,!1,!0);this.allowSlidePrev=s,this.allowSlideNext=i}},E=function(e){this.allowClick||(this.params.preventClicks&&e.preventDefault(),this.params.preventClicksPropagation&&this.animating&&(e.stopPropagation(),e.stopImmediatePropagation()))};var M={init:!0,direction:"horizontal",touchEventsTarget:"container",initialSlide:0,speed:300,iOSEdgeSwipeDetection:!1,iOSEdgeSwipeThreshold:20,freeMode:!1,freeModeMomentum:!0,freeModeMomentumRatio:1,freeModeMomentumBounce:!0,freeModeMomentumBounceRatio:1,freeModeMomentumVelocityRatio:1,freeModeSticky:!1,freeModeMinimumVelocity:.02,autoHeight:!1,setWrapperSize:!1,virtualTranslate:!1,effect:"slide",breakpoints:void 0,spaceBetween:0,slidesPerView:1,slidesPerColumn:1,slidesPerColumnFill:"column",slidesPerGroup:1,centeredSlides:!1,slidesOffsetBefore:0,slidesOffsetAfter:0,normalizeSlideIndex:!0,watchOverflow:!1,roundLengths:!1,touchRatio:1,touchAngle:45,simulateTouch:!0,shortSwipes:!0,longSwipes:!0,longSwipesRatio:.5,longSwipesMs:300,followFinger:!0,allowTouchMove:!0,threshold:0,touchMoveStopPropagation:!0,touchReleaseOnEdges:!1,uniqueNavElements:!0,resistance:!0,resistanceRatio:.85,watchSlidesProgress:!1,watchSlidesVisibility:!1,grabCursor:!1,preventClicks:!0,preventClicksPropagation:!0,slideToClickedSlide:!1,preloadImages:!0,updateOnImagesReady:!0,loop:!1,loopAdditionalSlides:0,loopedSlides:null,loopFillGroupWithBlank:!1,allowSlidePrev:!0,allowSlideNext:!0,swipeHandler:null,noSwiping:!0,noSwipingClass:"swiper-no-swiping",noSwipingSelector:null,passiveListeners:!0,containerModifierClass:"swiper-container-",slideClass:"swiper-slide",slideBlankClass:"swiper-slide-invisible-blank",slideActiveClass:"swiper-slide-active",slideDuplicateActiveClass:"swiper-slide-duplicate-active",slideVisibleClass:"swiper-slide-visible",slideDuplicateClass:"swiper-slide-duplicate",slideNextClass:"swiper-slide-next",slideDuplicateNextClass:"swiper-slide-duplicate-next",slidePrevClass:"swiper-slide-prev",slideDuplicatePrevClass:"swiper-slide-duplicate-prev",wrapperClass:"swiper-wrapper",runCallbacksOnInit:!0},k={update:u,translate:v,transition:f,slide:m,loop:g,grabCursor:w,manipulation:b,events:{attachEvents:function(){var e=this.params,t=this.touchEvents,i=this.el,s=this.wrapperEl;this.onTouchStart=x.bind(this),this.onTouchMove=C.bind(this),this.onTouchEnd=T.bind(this),this.onClick=E.bind(this);var a="container"===e.touchEventsTarget?i:s,n=!!e.nested;if(h.touch||!h.pointerEvents&&!h.prefixedPointerEvents){if(h.touch){var r=!("touchstart"!==t.start||!h.passiveListener||!e.passiveListeners)&&{passive:!0,capture:!1};a.addEventListener(t.start,this.onTouchStart,r),a.addEventListener(t.move,this.onTouchMove,h.passiveListener?{passive:!1,capture:n}:n),a.addEventListener(t.end,this.onTouchEnd,r)}(e.simulateTouch&&!y.ios&&!y.android||e.simulateTouch&&!h.touch&&y.ios)&&(a.addEventListener("mousedown",this.onTouchStart,!1),d.addEventListener("mousemove",this.onTouchMove,n),d.addEventListener("mouseup",this.onTouchEnd,!1))}else a.addEventListener(t.start,this.onTouchStart,!1),d.addEventListener(t.move,this.onTouchMove,n),d.addEventListener(t.end,this.onTouchEnd,!1);(e.preventClicks||e.preventClicksPropagation)&&a.addEventListener("click",this.onClick,!0),this.on("resize observerUpdate",S)},detachEvents:function(){var e=this.params,t=this.touchEvents,i=this.el,s=this.wrapperEl,a="container"===e.touchEventsTarget?i:s,n=!!e.nested;if(h.touch||!h.pointerEvents&&!h.prefixedPointerEvents){if(h.touch){var r=!("onTouchStart"!==t.start||!h.passiveListener||!e.passiveListeners)&&{passive:!0,capture:!1};a.removeEventListener(t.start,this.onTouchStart,r),a.removeEventListener(t.move,this.onTouchMove,n),a.removeEventListener(t.end,this.onTouchEnd,r)}(e.simulateTouch&&!y.ios&&!y.android||e.simulateTouch&&!h.touch&&y.ios)&&(a.removeEventListener("mousedown",this.onTouchStart,!1),d.removeEventListener("mousemove",this.onTouchMove,n),d.removeEventListener("mouseup",this.onTouchEnd,!1))}else a.removeEventListener(t.start,this.onTouchStart,!1),d.removeEventListener(t.move,this.onTouchMove,n),d.removeEventListener(t.end,this.onTouchEnd,!1);(e.preventClicks||e.preventClicksPropagation)&&a.removeEventListener("click",this.onClick,!0),this.off("resize observerUpdate",S)}},breakpoints:{setBreakpoint:function(){var e=this.activeIndex,t=this.loopedSlides;void 0===t&&(t=0);var i=this.params,s=i.breakpoints;if(s&&(!s||0!==Object.keys(s).length)){var a=this.getBreakpoint(s);if(a&&this.currentBreakpoint!==a){var n=a in s?s[a]:this.originalParams,r=i.loop&&n.slidesPerView!==i.slidesPerView;l.extend(this.params,n),l.extend(this,{allowTouchMove:this.params.allowTouchMove,allowSlideNext:this.params.allowSlideNext,allowSlidePrev:this.params.allowSlidePrev}),this.currentBreakpoint=a,r&&(this.loopDestroy(),this.loopCreate(),this.updateSlides(),this.slideTo(e-t+this.loopedSlides,0,!1)),this.emit("breakpoint",n)}}},getBreakpoint:function(e){if(e){var t=!1,i=[];Object.keys(e).forEach(function(e){i.push(e)}),i.sort(function(e,t){return parseInt(e,10)-parseInt(t,10)});for(var s=0;s<i.length;s+=1){var a=i[s];a>=o.innerWidth&&!t&&(t=a)}return t||"max"}}},checkOverflow:{checkOverflow:function(){var e=this.isLocked;this.isLocked=1===this.snapGrid.length,this.allowTouchMove=!this.isLocked,e&&e!==this.isLocked&&(this.isEnd=!1,this.navigation.update())}},classes:{addClasses:function(){var e=this.classNames,t=this.params,i=this.rtl,s=this.$el,a=[];a.push(t.direction),t.freeMode&&a.push("free-mode"),h.flexbox||a.push("no-flexbox"),t.autoHeight&&a.push("autoheight"),i&&a.push("rtl"),t.slidesPerColumn>1&&a.push("multirow"),y.android&&a.push("android"),y.ios&&a.push("ios"),(h.pointerEvents||h.prefixedPointerEvents)&&a.push("wp8-"+t.direction),a.forEach(function(i){e.push(t.containerModifierClass+i)}),s.addClass(e.join(" "))},removeClasses:function(){var e=this.$el,t=this.classNames;e.removeClass(t.join(" "))}},images:{loadImage:function(e,t,i,s,a,n){var r;function l(){n&&n()}e.complete&&a?l():t?((r=new o.Image).onload=l,r.onerror=l,s&&(r.sizes=s),i&&(r.srcset=i),t&&(r.src=t)):l()},preloadImages:function(){var e=this;function t(){void 0!==e&&null!==e&&e&&!e.destroyed&&(void 0!==e.imagesLoaded&&(e.imagesLoaded+=1),e.imagesLoaded===e.imagesToLoad.length&&(e.params.updateOnImagesReady&&e.update(),e.emit("imagesReady")))}e.imagesToLoad=e.$el.find("img");for(var i=0;i<e.imagesToLoad.length;i+=1){var s=e.imagesToLoad[i];e.loadImage(s,s.currentSrc||s.getAttribute("src"),s.srcset||s.getAttribute("srcset"),s.sizes||s.getAttribute("sizes"),!0,t)}}}},P={},L=function(e){function i(){for(var s,a,n,r=[],o=arguments.length;o--;)r[o]=arguments[o];1===r.length&&r[0].constructor&&r[0].constructor===Object?a=r[0]:(s=(n=r)[0],a=n[1]);a||(a={}),a=l.extend({},a),s&&!a.el&&(a.el=s),e.call(this,a),Object.keys(k).forEach(function(e){Object.keys(k[e]).forEach(function(t){i.prototype[t]||(i.prototype[t]=k[e][t])})});var d=this;void 0===d.modules&&(d.modules={}),Object.keys(d.modules).forEach(function(e){var t=d.modules[e];if(t.params){var i=Object.keys(t.params)[0],s=t.params[i];if("object"!=typeof s)return;if(!(i in a&&"enabled"in s))return;!0===a[i]&&(a[i]={enabled:!0}),"object"!=typeof a[i]||"enabled"in a[i]||(a[i].enabled=!0),a[i]||(a[i]={enabled:!1})}});var p=l.extend({},M);d.useModulesParams(p),d.params=l.extend({},p,P,a),d.originalParams=l.extend({},d.params),d.passedParams=l.extend({},a);var c=t(d.params.el);if(s=c[0]){if(c.length>1){var u=[];return c.each(function(e,t){var s=l.extend({},a,{el:t});u.push(new i(s))}),u}s.swiper=d,c.data("swiper",d);var v,f,m=c.children("."+d.params.wrapperClass);return l.extend(d,{$el:c,el:s,$wrapperEl:m,wrapperEl:m[0],classNames:[],slides:t(),slidesGrid:[],snapGrid:[],slidesSizesGrid:[],isHorizontal:function(){return"horizontal"===d.params.direction},isVertical:function(){return"vertical"===d.params.direction},rtl:"horizontal"===d.params.direction&&("rtl"===s.dir.toLowerCase()||"rtl"===c.css("direction")),wrongRTL:"-webkit-box"===m.css("display"),activeIndex:0,realIndex:0,isBeginning:!0,isEnd:!1,translate:0,progress:0,velocity:0,animating:!1,allowSlideNext:d.params.allowSlideNext,allowSlidePrev:d.params.allowSlidePrev,touchEvents:(v=["touchstart","touchmove","touchend"],f=["mousedown","mousemove","mouseup"],h.pointerEvents?f=["pointerdown","pointermove","pointerup"]:h.prefixedPointerEvents&&(f=["MSPointerDown","MSPointerMove","MSPointerUp"]),{start:h.touch||!d.params.simulateTouch?v[0]:f[0],move:h.touch||!d.params.simulateTouch?v[1]:f[1],end:h.touch||!d.params.simulateTouch?v[2]:f[2]}),touchEventsData:{isTouched:void 0,isMoved:void 0,allowTouchCallbacks:void 0,touchStartTime:void 0,isScrolling:void 0,currentTranslate:void 0,startTranslate:void 0,allowThresholdMove:void 0,formElements:"input, select, option, textarea, button, video",lastClickTime:l.now(),clickTimeout:void 0,velocities:[],allowMomentumBounce:void 0,isTouchEvent:void 0,startMoving:void 0},allowClick:!0,allowTouchMove:d.params.allowTouchMove,touches:{startX:0,startY:0,currentX:0,currentY:0,diff:0},imagesToLoad:[],imagesLoaded:0}),d.useModules(),d.params.init&&d.init(),d}}e&&(i.__proto__=e),i.prototype=Object.create(e&&e.prototype),i.prototype.constructor=i;var s={extendedDefaults:{configurable:!0},defaults:{configurable:!0},Class:{configurable:!0},$:{configurable:!0}};return i.prototype.slidesPerViewDynamic=function(){var e=this.params,t=this.slides,i=this.slidesGrid,s=this.size,a=this.activeIndex,n=1;if(e.centeredSlides){for(var r,o=t[a].swiperSlideSize,l=a+1;l<t.length;l+=1)t[l]&&!r&&(n+=1,(o+=t[l].swiperSlideSize)>s&&(r=!0));for(var d=a-1;d>=0;d-=1)t[d]&&!r&&(n+=1,(o+=t[d].swiperSlideSize)>s&&(r=!0))}else for(var h=a+1;h<t.length;h+=1)i[h]-i[a]<s&&(n+=1);return n},i.prototype.update=function(){var e=this;e&&!e.destroyed&&(e.updateSize(),e.updateSlides(),e.updateProgress(),e.updateSlidesClasses(),e.params.freeMode?(t(),e.params.autoHeight&&e.updateAutoHeight()):(("auto"===e.params.slidesPerView||e.params.slidesPerView>1)&&e.isEnd&&!e.params.centeredSlides?e.slideTo(e.slides.length-1,0,!1,!0):e.slideTo(e.activeIndex,0,!1,!0))||t(),e.emit("update"));function t(){var t=e.rtl?-1*e.translate:e.translate,i=Math.min(Math.max(t,e.maxTranslate()),e.minTranslate());e.setTranslate(i),e.updateActiveIndex(),e.updateSlidesClasses()}},i.prototype.init=function(){this.initialized||(this.emit("beforeInit"),this.params.breakpoints&&this.setBreakpoint(),this.addClasses(),this.params.loop&&this.loopCreate(),this.updateSize(),this.updateSlides(),this.params.watchOverflow&&this.checkOverflow(),this.params.grabCursor&&this.setGrabCursor(),this.params.preloadImages&&this.preloadImages(),this.params.loop?this.slideTo(this.params.initialSlide+this.loopedSlides,0,this.params.runCallbacksOnInit):this.slideTo(this.params.initialSlide,0,this.params.runCallbacksOnInit),this.attachEvents(),this.initialized=!0,this.emit("init"))},i.prototype.destroy=function(e,t){void 0===e&&(e=!0),void 0===t&&(t=!0);var i=this,s=i.params,a=i.$el,n=i.$wrapperEl,r=i.slides;i.emit("beforeDestroy"),i.initialized=!1,i.detachEvents(),s.loop&&i.loopDestroy(),t&&(i.removeClasses(),a.removeAttr("style"),n.removeAttr("style"),r&&r.length&&r.removeClass([s.slideVisibleClass,s.slideActiveClass,s.slideNextClass,s.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-slide-index").removeAttr("data-swiper-column").removeAttr("data-swiper-row")),i.emit("destroy"),Object.keys(i.eventsListeners).forEach(function(e){i.off(e)}),!1!==e&&(i.$el[0].swiper=null,i.$el.data("swiper",null),l.deleteProps(i)),i.destroyed=!0},i.extendDefaults=function(e){l.extend(P,e)},s.extendedDefaults.get=function(){return P},s.defaults.get=function(){return M},s.Class.get=function(){return e},s.$.get=function(){return t},Object.defineProperties(i,s),i}(p),z={name:"device",proto:{device:y},static:{device:y}},I={name:"support",proto:{support:h},static:{support:h}},O=function(){return{isSafari:(e=o.navigator.userAgent.toLowerCase(),e.indexOf("safari")>=0&&e.indexOf("chrome")<0&&e.indexOf("android")<0),isUiWebView:/(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(o.navigator.userAgent)};var e}(),A={name:"browser",proto:{browser:O},static:{browser:O}},D={name:"resize",create:function(){var e=this;l.extend(e,{resize:{resizeHandler:function(){e&&!e.destroyed&&e.initialized&&(e.emit("beforeResize"),e.emit("resize"))},orientationChangeHandler:function(){e&&!e.destroyed&&e.initialized&&e.emit("orientationchange")}}})},on:{init:function(){o.addEventListener("resize",this.resize.resizeHandler),o.addEventListener("orientationchange",this.resize.orientationChangeHandler)},destroy:function(){o.removeEventListener("resize",this.resize.resizeHandler),o.removeEventListener("orientationchange",this.resize.orientationChangeHandler)}}},B={func:o.MutationObserver||o.WebkitMutationObserver,attach:function(e,t){void 0===t&&(t={});var i=this,s=new(0,B.func)(function(e){e.forEach(function(e){i.emit("observerUpdate",e)})});s.observe(e,{attributes:void 0===t.attributes||t.attributes,childList:void 0===t.childList||t.childList,characterData:void 0===t.characterData||t.characterData}),i.observer.observers.push(s)},init:function(){if(h.observer&&this.params.observer){if(this.params.observeParents)for(var e=this.$el.parents(),t=0;t<e.length;t+=1)this.observer.attach(e[t]);this.observer.attach(this.$el[0],{childList:!1}),this.observer.attach(this.$wrapperEl[0],{attributes:!1})}},destroy:function(){this.observer.observers.forEach(function(e){e.disconnect()}),this.observer.observers=[]}},$={name:"observer",params:{observer:!1,observeParents:!1},create:function(){l.extend(this,{observer:{init:B.init.bind(this),attach:B.attach.bind(this),destroy:B.destroy.bind(this),observers:[]}})},on:{init:function(){this.observer.init()},destroy:function(){this.observer.destroy()}}},H={handle:function(e){var t=e;t.originalEvent&&(t=t.originalEvent);var i=t.keyCode||t.charCode;if(!this.allowSlideNext&&(this.isHorizontal()&&39===i||this.isVertical()&&40===i))return!1;if(!this.allowSlidePrev&&(this.isHorizontal()&&37===i||this.isVertical()&&38===i))return!1;if(!(t.shiftKey||t.altKey||t.ctrlKey||t.metaKey||d.activeElement&&d.activeElement.nodeName&&("input"===d.activeElement.nodeName.toLowerCase()||"textarea"===d.activeElement.nodeName.toLowerCase()))){if(this.params.keyboard.onlyInViewport&&(37===i||39===i||38===i||40===i)){var s=!1;if(this.$el.parents("."+this.params.slideClass).length>0&&0===this.$el.parents("."+this.params.slideActiveClass).length)return;var a=o.innerWidth,n=o.innerHeight,r=this.$el.offset();this.rtl&&(r.left-=this.$el[0].scrollLeft);for(var l=[[r.left,r.top],[r.left+this.width,r.top],[r.left,r.top+this.height],[r.left+this.width,r.top+this.height]],h=0;h<l.length;h+=1){var p=l[h];p[0]>=0&&p[0]<=a&&p[1]>=0&&p[1]<=n&&(s=!0)}if(!s)return}this.isHorizontal()?(37!==i&&39!==i||(t.preventDefault?t.preventDefault():t.returnValue=!1),(39===i&&!this.rtl||37===i&&this.rtl)&&this.slideNext(),(37===i&&!this.rtl||39===i&&this.rtl)&&this.slidePrev()):(38!==i&&40!==i||(t.preventDefault?t.preventDefault():t.returnValue=!1),40===i&&this.slideNext(),38===i&&this.slidePrev()),this.emit("keyPress",i)}},enable:function(){this.keyboard.enabled||(t(d).on("keydown",this.keyboard.handle),this.keyboard.enabled=!0)},disable:function(){this.keyboard.enabled&&(t(d).off("keydown",this.keyboard.handle),this.keyboard.enabled=!1)}},N={name:"keyboard",params:{keyboard:{enabled:!1,onlyInViewport:!0}},create:function(){l.extend(this,{keyboard:{enabled:!1,enable:H.enable.bind(this),disable:H.disable.bind(this),handle:H.handle.bind(this)}})},on:{init:function(){this.params.keyboard.enabled&&this.keyboard.enable()},destroy:function(){this.keyboard.enabled&&this.keyboard.disable()}}};var V={lastScrollTime:l.now(),event:o.navigator.userAgent.indexOf("firefox")>-1?"DOMMouseScroll":function(){var e="onwheel"in d;if(!e){var t=d.createElement("div");t.setAttribute("onwheel","return;"),e="function"==typeof t.onwheel}return!e&&d.implementation&&d.implementation.hasFeature&&!0!==d.implementation.hasFeature("","")&&(e=d.implementation.hasFeature("Events.wheel","3.0")),e}()?"wheel":"mousewheel",normalize:function(e){var t=0,i=0,s=0,a=0;return"detail"in e&&(i=e.detail),"wheelDelta"in e&&(i=-e.wheelDelta/120),"wheelDeltaY"in e&&(i=-e.wheelDeltaY/120),"wheelDeltaX"in e&&(t=-e.wheelDeltaX/120),"axis"in e&&e.axis===e.HORIZONTAL_AXIS&&(t=i,i=0),s=10*t,a=10*i,"deltaY"in e&&(a=e.deltaY),"deltaX"in e&&(s=e.deltaX),(s||a)&&e.deltaMode&&(1===e.deltaMode?(s*=40,a*=40):(s*=800,a*=800)),s&&!t&&(t=s<1?-1:1),a&&!i&&(i=a<1?-1:1),{spinX:t,spinY:i,pixelX:s,pixelY:a}},handle:function(e){var t=e,i=this,s=i.params.mousewheel;t.originalEvent&&(t=t.originalEvent);var a=0,n=i.rtl?-1:1,r=V.normalize(t);if(s.forceToAxis)if(i.isHorizontal()){if(!(Math.abs(r.pixelX)>Math.abs(r.pixelY)))return!0;a=r.pixelX*n}else{if(!(Math.abs(r.pixelY)>Math.abs(r.pixelX)))return!0;a=r.pixelY}else a=Math.abs(r.pixelX)>Math.abs(r.pixelY)?-r.pixelX*n:-r.pixelY;if(0===a)return!0;if(s.invert&&(a=-a),i.params.freeMode){var d=i.getTranslate()+a*s.sensitivity,h=i.isBeginning,p=i.isEnd;if(d>=i.minTranslate()&&(d=i.minTranslate()),d<=i.maxTranslate()&&(d=i.maxTranslate()),i.setTransition(0),i.setTranslate(d),i.updateProgress(),i.updateActiveIndex(),i.updateSlidesClasses(),(!h&&i.isBeginning||!p&&i.isEnd)&&i.updateSlidesClasses(),i.params.freeModeSticky&&(clearTimeout(i.mousewheel.timeout),i.mousewheel.timeout=l.nextTick(function(){i.slideReset()},300)),i.emit("scroll",t),i.params.autoplay&&i.params.autoplayDisableOnInteraction&&i.stopAutoplay(),0===d||d===i.maxTranslate())return!0}else{if(l.now()-i.mousewheel.lastScrollTime>60)if(a<0)if(i.isEnd&&!i.params.loop||i.animating){if(s.releaseOnEdges)return!0}else i.slideNext(),i.emit("scroll",t);else if(i.isBeginning&&!i.params.loop||i.animating){if(s.releaseOnEdges)return!0}else i.slidePrev(),i.emit("scroll",t);i.mousewheel.lastScrollTime=(new o.Date).getTime()}return t.preventDefault?t.preventDefault():t.returnValue=!1,!1},enable:function(){if(!V.event)return!1;if(this.mousewheel.enabled)return!1;var e=this.$el;return"container"!==this.params.mousewheel.eventsTarged&&(e=t(this.params.mousewheel.eventsTarged)),e.on(V.event,this.mousewheel.handle),this.mousewheel.enabled=!0,!0},disable:function(){if(!V.event)return!1;if(!this.mousewheel.enabled)return!1;var e=this.$el;return"container"!==this.params.mousewheel.eventsTarged&&(e=t(this.params.mousewheel.eventsTarged)),e.off(V.event,this.mousewheel.handle),this.mousewheel.enabled=!1,!0}},G={update:function(){var e=this.params.navigation;if(!this.params.loop){var t=this.navigation,i=t.$nextEl,s=t.$prevEl;s&&s.length>0&&(this.isBeginning?s.addClass(e.disabledClass):s.removeClass(e.disabledClass),s[this.params.watchOverflow&&this.isLocked?"addClass":"removeClass"](e.lockClass)),i&&i.length>0&&(this.isEnd?i.addClass(e.disabledClass):i.removeClass(e.disabledClass),i[this.params.watchOverflow&&this.isLocked?"addClass":"removeClass"](e.lockClass))}},init:function(){var e,i,s=this,a=s.params.navigation;(a.nextEl||a.prevEl)&&(a.nextEl&&(e=t(a.nextEl),s.params.uniqueNavElements&&"string"==typeof a.nextEl&&e.length>1&&1===s.$el.find(a.nextEl).length&&(e=s.$el.find(a.nextEl))),a.prevEl&&(i=t(a.prevEl),s.params.uniqueNavElements&&"string"==typeof a.prevEl&&i.length>1&&1===s.$el.find(a.prevEl).length&&(i=s.$el.find(a.prevEl))),e&&e.length>0&&e.on("click",function(e){e.preventDefault(),s.isEnd&&!s.params.loop||s.slideNext()}),i&&i.length>0&&i.on("click",function(e){e.preventDefault(),s.isBeginning&&!s.params.loop||s.slidePrev()}),l.extend(s.navigation,{$nextEl:e,nextEl:e&&e[0],$prevEl:i,prevEl:i&&i[0]}))},destroy:function(){var e=this.navigation,t=e.$nextEl,i=e.$prevEl;t&&t.length&&(t.off("click"),t.removeClass(this.params.navigation.disabledClass)),i&&i.length&&(i.off("click"),i.removeClass(this.params.navigation.disabledClass))}},R={update:function(){var e=this.rtl,i=this.params.pagination;if(i.el&&this.pagination.el&&this.pagination.$el&&0!==this.pagination.$el.length){var s,a=this.virtual&&this.params.virtual.enabled?this.virtual.slides.length:this.slides.length,n=this.pagination.$el,r=this.params.loop?Math.ceil((a-2*this.loopedSlides)/this.params.slidesPerGroup):this.snapGrid.length;if(this.params.loop?((s=Math.ceil((this.activeIndex-this.loopedSlides)/this.params.slidesPerGroup))>a-1-2*this.loopedSlides&&(s-=a-2*this.loopedSlides),s>r-1&&(s-=r),s<0&&"bullets"!==this.params.paginationType&&(s=r+s)):s=void 0!==this.snapIndex?this.snapIndex:this.activeIndex||0,"bullets"===i.type&&this.pagination.bullets&&this.pagination.bullets.length>0){var o,l,d,h=this.pagination.bullets;if(i.dynamicBullets&&(this.pagination.bulletSize=h.eq(0)[this.isHorizontal()?"outerWidth":"outerHeight"](!0),n.css(this.isHorizontal()?"width":"height",this.pagination.bulletSize*(i.dynamicMainBullets+4)+"px"),i.dynamicMainBullets>1&&void 0!==this.previousIndex&&(s>this.previousIndex&&this.pagination.dynamicBulletIndex<i.dynamicMainBullets-1?this.pagination.dynamicBulletIndex+=1:s<this.previousIndex&&this.pagination.dynamicBulletIndex>0&&(this.pagination.dynamicBulletIndex-=1)),o=s-this.pagination.dynamicBulletIndex,d=((l=o+(i.dynamicMainBullets-1))+o)/2),h.removeClass(i.bulletActiveClass+" "+i.bulletActiveClass+"-next "+i.bulletActiveClass+"-next-next "+i.bulletActiveClass+"-prev "+i.bulletActiveClass+"-prev-prev "+i.bulletActiveClass+"-main"),n.length>1)h.each(function(e,a){var n=t(a),r=n.index();r===s&&n.addClass(i.bulletActiveClass),i.dynamicBullets&&(r>=o&&r<=l&&n.addClass(i.bulletActiveClass+"-main"),r===o&&n.prev().addClass(i.bulletActiveClass+"-prev").prev().addClass(i.bulletActiveClass+"-prev-prev"),r===l&&n.next().addClass(i.bulletActiveClass+"-next").next().addClass(i.bulletActiveClass+"-next-next"))});else if(h.eq(s).addClass(i.bulletActiveClass),i.dynamicBullets){for(var p=h.eq(o),c=h.eq(l),u=o;u<=l;u+=1)h.eq(u).addClass(i.bulletActiveClass+"-main");p.prev().addClass(i.bulletActiveClass+"-prev").prev().addClass(i.bulletActiveClass+"-prev-prev"),c.next().addClass(i.bulletActiveClass+"-next").next().addClass(i.bulletActiveClass+"-next-next")}if(i.dynamicBullets){var v=Math.min(h.length,i.dynamicMainBullets+4),f=(this.pagination.bulletSize*v-this.pagination.bulletSize)/2-d*this.pagination.bulletSize,m=e?"right":"left";h.css(this.isHorizontal()?m:"top",f+"px")}}if("fraction"===i.type&&(n.find("."+i.currentClass).text(s+1),n.find("."+i.totalClass).text(r)),"progressbar"===i.type){var g=(s+1)/r,w=g,b=1;this.isHorizontal()||(b=g,w=1),n.find("."+i.progressbarFillClass).transform("translate3d(0,0,0) scaleX("+w+") scaleY("+b+")").transition(this.params.speed)}"custom"===i.type&&i.renderCustom?(n.html(i.renderCustom(this,s+1,r)),this.emit("paginationRender",this,n[0])):this.emit("paginationUpdate",this,n[0]),n[this.params.watchOverflow&&this.isLocked?"addClass":"removeClass"](i.lockClass)}},render:function(){var e=this.params.pagination;if(e.el&&this.pagination.el&&this.pagination.$el&&0!==this.pagination.$el.length){var t=this.virtual&&this.params.virtual.enabled?this.virtual.slides.length:this.slides.length,i=this.pagination.$el,s="";if("bullets"===e.type){for(var a=this.params.loop?Math.ceil((t-2*this.loopedSlides)/this.params.slidesPerGroup):this.snapGrid.length,n=0;n<a;n+=1)e.renderBullet?s+=e.renderBullet.call(this,n,e.bulletClass):s+="<"+e.bulletElement+' class="'+e.bulletClass+'"></'+e.bulletElement+">";i.html(s),this.pagination.bullets=i.find("."+e.bulletClass)}"fraction"===e.type&&(s=e.renderFraction?e.renderFraction.call(this,e.currentClass,e.totalClass):'<span class="'+e.currentClass+'"></span> / <span class="'+e.totalClass+'"></span>',i.html(s)),"progressbar"===e.type&&(s=e.renderProgressbar?e.renderProgressbar.call(this,e.progressbarFillClass):'<span class="'+e.progressbarFillClass+'"></span>',i.html(s)),"custom"!==e.type&&this.emit("paginationRender",this.pagination.$el[0])}},init:function(){var e=this,i=e.params.pagination;if(i.el){var s=t(i.el);0!==s.length&&(e.params.uniqueNavElements&&"string"==typeof i.el&&s.length>1&&1===e.$el.find(i.el).length&&(s=e.$el.find(i.el)),"bullets"===i.type&&i.clickable&&s.addClass(i.clickableClass),s.addClass(i.modifierClass+i.type),"bullets"===i.type&&i.dynamicBullets&&(s.addClass(""+i.modifierClass+i.type+"-dynamic"),e.pagination.dynamicBulletIndex=0,i.dynamicMainBullets<1&&(i.dynamicMainBullets=1)),i.clickable&&s.on("click","."+i.bulletClass,function(i){i.preventDefault();var s=t(this).index()*e.params.slidesPerGroup;e.params.loop&&(s+=e.loopedSlides),e.slideTo(s)}),l.extend(e.pagination,{$el:s,el:s[0]}))}},destroy:function(){var e=this.params.pagination;if(e.el&&this.pagination.el&&this.pagination.$el&&0!==this.pagination.$el.length){var t=this.pagination.$el;t.removeClass(e.hiddenClass),t.removeClass(e.modifierClass+e.type),this.pagination.bullets&&this.pagination.bullets.removeClass(e.bulletActiveClass),e.clickable&&t.off("click","."+e.bulletClass)}}},F={LinearSpline:function(e,t){var i,s,a,n,r,o=function(e,t){for(s=-1,i=e.length;i-s>1;)e[a=i+s>>1]<=t?s=a:i=a;return i};return this.x=e,this.y=t,this.lastIndex=e.length-1,this.interpolate=function(e){return e?(r=o(this.x,e),n=r-1,(e-this.x[n])*(this.y[r]-this.y[n])/(this.x[r]-this.x[n])+this.y[n]):0},this},getInterpolateFunction:function(e){this.controller.spline||(this.controller.spline=this.params.loop?new F.LinearSpline(this.slidesGrid,e.slidesGrid):new F.LinearSpline(this.snapGrid,e.snapGrid))},setTranslate:function(e,t){var i,s,a=this,n=a.controller.control;function r(e){var t=e.rtl&&"horizontal"===e.params.direction?-a.translate:a.translate;"slide"===a.params.controller.by&&(a.controller.getInterpolateFunction(e),s=-a.controller.spline.interpolate(-t)),s&&"container"!==a.params.controller.by||(i=(e.maxTranslate()-e.minTranslate())/(a.maxTranslate()-a.minTranslate()),s=(t-a.minTranslate())*i+e.minTranslate()),a.params.controller.inverse&&(s=e.maxTranslate()-s),e.updateProgress(s),e.setTranslate(s,a),e.updateActiveIndex(),e.updateSlidesClasses()}if(Array.isArray(n))for(var o=0;o<n.length;o+=1)n[o]!==t&&n[o]instanceof L&&r(n[o]);else n instanceof L&&t!==n&&r(n)},setTransition:function(e,t){var i,s=this,a=s.controller.control;function n(t){t.setTransition(e,s),0!==e&&(t.transitionStart(),t.$wrapperEl.transitionEnd(function(){a&&(t.params.loop&&"slide"===s.params.controller.by&&t.loopFix(),t.transitionEnd())}))}if(Array.isArray(a))for(i=0;i<a.length;i+=1)a[i]!==t&&a[i]instanceof L&&n(a[i]);else a instanceof L&&t!==a&&n(a)}},X={makeElFocusable:function(e){return e.attr("tabIndex","0"),e},addElRole:function(e,t){return e.attr("role",t),e},addElLabel:function(e,t){return e.attr("aria-label",t),e},disableEl:function(e){return e.attr("aria-disabled",!0),e},enableEl:function(e){return e.attr("aria-disabled",!1),e},onEnterKey:function(e){var i=this.params.a11y;if(13===e.keyCode){var s=t(e.target);this.navigation&&this.navigation.$nextEl&&s.is(this.navigation.$nextEl)&&(this.isEnd&&!this.params.loop||this.slideNext(),this.isEnd?this.a11y.notify(i.lastSlideMessage):this.a11y.notify(i.nextSlideMessage)),this.navigation&&this.navigation.$prevEl&&s.is(this.navigation.$prevEl)&&(this.isBeginning&&!this.params.loop||this.slidePrev(),this.isBeginning?this.a11y.notify(i.firstSlideMessage):this.a11y.notify(i.prevSlideMessage)),this.pagination&&s.is("."+this.params.pagination.bulletClass)&&s[0].click()}},notify:function(e){var t=this.a11y.liveRegion;0!==t.length&&(t.html(""),t.html(e))},updateNavigation:function(){if(!this.params.loop){var e=this.navigation,t=e.$nextEl,i=e.$prevEl;i&&i.length>0&&(this.isBeginning?this.a11y.disableEl(i):this.a11y.enableEl(i)),t&&t.length>0&&(this.isEnd?this.a11y.disableEl(t):this.a11y.enableEl(t))}},updatePagination:function(){var e=this,i=e.params.a11y;e.pagination&&e.params.pagination.clickable&&e.pagination.bullets&&e.pagination.bullets.length&&e.pagination.bullets.each(function(s,a){var n=t(a);e.a11y.makeElFocusable(n),e.a11y.addElRole(n,"button"),e.a11y.addElLabel(n,i.paginationBulletMessage.replace(/{{index}}/,n.index()+1))})},init:function(){this.$el.append(this.a11y.liveRegion);var e,t,i=this.params.a11y;this.navigation&&this.navigation.$nextEl&&(e=this.navigation.$nextEl),this.navigation&&this.navigation.$prevEl&&(t=this.navigation.$prevEl),e&&(this.a11y.makeElFocusable(e),this.a11y.addElRole(e,"button"),this.a11y.addElLabel(e,i.nextSlideMessage),e.on("keydown",this.a11y.onEnterKey)),t&&(this.a11y.makeElFocusable(t),this.a11y.addElRole(t,"button"),this.a11y.addElLabel(t,i.prevSlideMessage),t.on("keydown",this.a11y.onEnterKey)),this.pagination&&this.params.pagination.clickable&&this.pagination.bullets&&this.pagination.bullets.length&&this.pagination.$el.on("keydown","."+this.params.pagination.bulletClass,this.a11y.onEnterKey)},destroy:function(){var e,t;this.a11y.liveRegion&&this.a11y.liveRegion.length>0&&this.a11y.liveRegion.remove(),this.navigation&&this.navigation.$nextEl&&(e=this.navigation.$nextEl),this.navigation&&this.navigation.$prevEl&&(t=this.navigation.$prevEl),e&&e.off("keydown",this.a11y.onEnterKey),t&&t.off("keydown",this.a11y.onEnterKey),this.pagination&&this.params.pagination.clickable&&this.pagination.bullets&&this.pagination.bullets.length&&this.pagination.$el.off("keydown","."+this.params.pagination.bulletClass,this.a11y.onEnterKey)}},j={init:function(){if(this.params.history){if(!o.history||!o.history.pushState)return this.params.history.enabled=!1,void(this.params.hashNavigation.enabled=!0);var e=this.history;e.initialized=!0,e.paths=j.getPathValues(),(e.paths.key||e.paths.value)&&(e.scrollToSlide(0,e.paths.value,this.params.runCallbacksOnInit),this.params.history.replaceState||o.addEventListener("popstate",this.history.setHistoryPopState))}},destroy:function(){this.params.history.replaceState||o.removeEventListener("popstate",this.history.setHistoryPopState)},setHistoryPopState:function(){this.history.paths=j.getPathValues(),this.history.scrollToSlide(this.params.speed,this.history.paths.value,!1)},getPathValues:function(){var e=o.location.pathname.slice(1).split("/").filter(function(e){return""!==e}),t=e.length;return{key:e[t-2],value:e[t-1]}},setHistory:function(e,t){if(this.history.initialized&&this.params.history.enabled){var i=this.slides.eq(t),s=j.slugify(i.attr("data-history"));o.location.pathname.includes(e)||(s=e+"/"+s);var a=o.history.state;a&&a.value===s||(this.params.history.replaceState?o.history.replaceState({value:s},null,s):o.history.pushState({value:s},null,s))}},slugify:function(e){return e.toString().toLowerCase().replace(/\s+/g,"-").replace(/[^\w-]+/g,"").replace(/--+/g,"-").replace(/^-+/,"").replace(/-+$/,"")},scrollToSlide:function(e,t,i){if(t)for(var s=0,a=this.slides.length;s<a;s+=1){var n=this.slides.eq(s);if(j.slugify(n.attr("data-history"))===t&&!n.hasClass(this.params.slideDuplicateClass)){var r=n.index();this.slideTo(r,e,i)}}else this.slideTo(0,e,i)}},Y={setTranslate:function(){var e,i=this.$el,s=this.$wrapperEl,a=this.slides,n=this.width,r=this.height,o=this.rtl,l=this.size,d=this.params.cubeEffect,h=this.isHorizontal(),p=this.virtual&&this.params.virtual.enabled,c=0;d.shadow&&(h?(0===(e=s.find(".swiper-cube-shadow")).length&&(e=t('<div class="swiper-cube-shadow"></div>'),s.append(e)),e.css({height:n+"px"})):0===(e=i.find(".swiper-cube-shadow")).length&&(e=t('<div class="swiper-cube-shadow"></div>'),i.append(e)));for(var u=0;u<a.length;u+=1){var v=a.eq(u),f=u;p&&(f=parseInt(v.attr("data-swiper-slide-index"),10));var m=90*f,g=Math.floor(m/360);o&&(m=-m,g=Math.floor(-m/360));var w=Math.max(Math.min(v[0].progress,1),-1),b=0,y=0,x=0;f%4==0?(b=4*-g*l,x=0):(f-1)%4==0?(b=0,x=4*-g*l):(f-2)%4==0?(b=l+4*g*l,x=l):(f-3)%4==0&&(b=-l,x=3*l+4*l*g),o&&(b=-b),h||(y=b,b=0);var C="rotateX("+(h?0:-m)+"deg) rotateY("+(h?m:0)+"deg) translate3d("+b+"px, "+y+"px, "+x+"px)";if(w<=1&&w>-1&&(c=90*f+90*w,o&&(c=90*-f-90*w)),v.transform(C),d.slideShadows){var T=h?v.find(".swiper-slide-shadow-left"):v.find(".swiper-slide-shadow-top"),S=h?v.find(".swiper-slide-shadow-right"):v.find(".swiper-slide-shadow-bottom");0===T.length&&(T=t('<div class="swiper-slide-shadow-'+(h?"left":"top")+'"></div>'),v.append(T)),0===S.length&&(S=t('<div class="swiper-slide-shadow-'+(h?"right":"bottom")+'"></div>'),v.append(S)),T.length&&(T[0].style.opacity=Math.max(-w,0)),S.length&&(S[0].style.opacity=Math.max(w,0))}}if(s.css({"-webkit-transform-origin":"50% 50% -"+l/2+"px","-moz-transform-origin":"50% 50% -"+l/2+"px","-ms-transform-origin":"50% 50% -"+l/2+"px","transform-origin":"50% 50% -"+l/2+"px"}),d.shadow)if(h)e.transform("translate3d(0px, "+(n/2+d.shadowOffset)+"px, "+-n/2+"px) rotateX(90deg) rotateZ(0deg) scale("+d.shadowScale+")");else{var E=Math.abs(c)-90*Math.floor(Math.abs(c)/90),M=1.5-(Math.sin(2*E*Math.PI/360)/2+Math.cos(2*E*Math.PI/360)/2),k=d.shadowScale,P=d.shadowScale/M,L=d.shadowOffset;e.transform("scale3d("+k+", 1, "+P+") translate3d(0px, "+(r/2+L)+"px, "+-r/2/P+"px) rotateX(-90deg)")}var z=O.isSafari||O.isUiWebView?-l/2:0;s.transform("translate3d(0px,0,"+z+"px) rotateX("+(this.isHorizontal()?0:c)+"deg) rotateY("+(this.isHorizontal()?-c:0)+"deg)")},setTransition:function(e){var t=this.$el;this.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e),this.params.cubeEffect.shadow&&!this.isHorizontal()&&t.find(".swiper-cube-shadow").transition(e)}},q=[z,I,A,D,$,N,{name:"mousewheel",params:{mousewheel:{enabled:!1,releaseOnEdges:!1,invert:!1,forceToAxis:!1,sensitivity:1,eventsTarged:"container"}},create:function(){l.extend(this,{mousewheel:{enabled:!1,enable:V.enable.bind(this),disable:V.disable.bind(this),handle:V.handle.bind(this),lastScrollTime:l.now()}})},on:{init:function(){this.params.mousewheel.enabled&&this.mousewheel.enable()},destroy:function(){this.mousewheel.enabled&&this.mousewheel.disable()}}},{name:"navigation",params:{navigation:{nextEl:null,prevEl:null,hideOnClick:!1,disabledClass:"swiper-button-disabled",hiddenClass:"swiper-button-hidden",lockClass:"swiper-button-lock"}},create:function(){l.extend(this,{navigation:{init:G.init.bind(this),update:G.update.bind(this),destroy:G.destroy.bind(this)}})},on:{init:function(){this.navigation.init(),this.navigation.update()},toEdge:function(){this.navigation.update()},fromEdge:function(){this.navigation.update()},destroy:function(){this.navigation.destroy()},click:function(e){var i=this.navigation,s=i.$nextEl,a=i.$prevEl;!this.params.navigation.hideOnClick||t(e.target).is(a)||t(e.target).is(s)||(s&&s.toggleClass(this.params.navigation.hiddenClass),a&&a.toggleClass(this.params.navigation.hiddenClass))}}},{name:"pagination",params:{pagination:{el:null,bulletElement:"span",clickable:!1,hideOnClick:!1,renderBullet:null,renderProgressbar:null,renderFraction:null,renderCustom:null,type:"bullets",dynamicBullets:!1,dynamicMainBullets:1,bulletClass:"swiper-pagination-bullet",bulletActiveClass:"swiper-pagination-bullet-active",modifierClass:"swiper-pagination-",currentClass:"swiper-pagination-current",totalClass:"swiper-pagination-total",hiddenClass:"swiper-pagination-hidden",progressbarFillClass:"swiper-pagination-progressbar-fill",clickableClass:"swiper-pagination-clickable",lockClass:"swiper-pagination-lock"}},create:function(){l.extend(this,{pagination:{init:R.init.bind(this),render:R.render.bind(this),update:R.update.bind(this),destroy:R.destroy.bind(this),dynamicBulletIndex:0}})},on:{init:function(){this.pagination.init(),this.pagination.render(),this.pagination.update()},activeIndexChange:function(){this.params.loop?this.pagination.update():void 0===this.snapIndex&&this.pagination.update()},snapIndexChange:function(){this.params.loop||this.pagination.update()},slidesLengthChange:function(){this.params.loop&&(this.pagination.render(),this.pagination.update())},snapGridLengthChange:function(){this.params.loop||(this.pagination.render(),this.pagination.update())},destroy:function(){this.pagination.destroy()},click:function(e){this.params.pagination.el&&this.params.pagination.hideOnClick&&this.pagination.$el.length>0&&!t(e.target).hasClass(this.params.pagination.bulletClass)&&this.pagination.$el.toggleClass(this.params.pagination.hiddenClass)}}},{name:"controller",params:{controller:{control:void 0,inverse:!1,by:"slide"}},create:function(){l.extend(this,{controller:{control:this.params.controller.control,getInterpolateFunction:F.getInterpolateFunction.bind(this),setTranslate:F.setTranslate.bind(this),setTransition:F.setTransition.bind(this)}})},on:{update:function(){this.controller.control&&this.controller.spline&&(this.controller.spline=void 0,delete this.controller.spline)},resize:function(){this.controller.control&&this.controller.spline&&(this.controller.spline=void 0,delete this.controller.spline)},observerUpdate:function(){this.controller.control&&this.controller.spline&&(this.controller.spline=void 0,delete this.controller.spline)},setTranslate:function(e,t){this.controller.control&&this.controller.setTranslate(e,t)},setTransition:function(e,t){this.controller.control&&this.controller.setTransition(e,t)}}},{name:"a11y",params:{a11y:{enabled:!1,notificationClass:"swiper-notification",prevSlideMessage:"Previous slide",nextSlideMessage:"Next slide",firstSlideMessage:"This is the first slide",lastSlideMessage:"This is the last slide",paginationBulletMessage:"Go to slide {{index}}"}},create:function(){var e=this;l.extend(e,{a11y:{liveRegion:t('<span class="'+e.params.a11y.notificationClass+'" aria-live="assertive" aria-atomic="true"></span>')}}),Object.keys(X).forEach(function(t){e.a11y[t]=X[t].bind(e)})},on:{init:function(){this.params.a11y.enabled&&(this.a11y.init(),this.a11y.updateNavigation())},toEdge:function(){this.params.a11y.enabled&&this.a11y.updateNavigation()},fromEdge:function(){this.params.a11y.enabled&&this.a11y.updateNavigation()},paginationUpdate:function(){this.params.a11y.enabled&&this.a11y.updatePagination()},destroy:function(){this.params.a11y.enabled&&this.a11y.destroy()}}},{name:"history",params:{history:{enabled:!1,replaceState:!1,key:"slides"}},create:function(){l.extend(this,{history:{init:j.init.bind(this),setHistory:j.setHistory.bind(this),setHistoryPopState:j.setHistoryPopState.bind(this),scrollToSlide:j.scrollToSlide.bind(this),destroy:j.destroy.bind(this)}})},on:{init:function(){this.params.history.enabled&&this.history.init()},destroy:function(){this.params.history.enabled&&this.history.destroy()},transitionEnd:function(){this.history.initialized&&this.history.setHistory(this.params.history.key,this.activeIndex)}}},{name:"effect-cube",params:{cubeEffect:{slideShadows:!0,shadow:!0,shadowOffset:20,shadowScale:.94}},create:function(){l.extend(this,{cubeEffect:{setTranslate:Y.setTranslate.bind(this),setTransition:Y.setTransition.bind(this)}})},on:{beforeInit:function(){if("cube"===this.params.effect){this.classNames.push(this.params.containerModifierClass+"cube"),this.classNames.push(this.params.containerModifierClass+"3d");var e={slidesPerView:1,slidesPerColumn:1,slidesPerGroup:1,watchSlidesProgress:!0,resistanceRatio:0,spaceBetween:0,centeredSlides:!1,virtualTranslate:!0};l.extend(this.params,e),l.extend(this.originalParams,e)}},setTranslate:function(){"cube"===this.params.effect&&this.cubeEffect.setTranslate()},setTransition:function(e){"cube"===this.params.effect&&this.cubeEffect.setTransition(e)}}}];return void 0===L.use&&(L.use=L.Class.use,L.installModule=L.Class.installModule),L.use(q),L});
//# sourceMappingURL=swiper.min.js.map

;
function animate(options) {
  var start = performance.now();
  requestAnimationFrame(function animate(time) {
    var timeFraction = (time - start) / options.duration;
    if (timeFraction > 1) timeFraction = 1;

    var progress = options.timing(timeFraction);
    options.draw(progress);

    if (timeFraction < 1) {
      requestAnimationFrame(animate);
    }
    else {
      helper.runCallback(options.callback);
    }
  });
}

var zoomOut = function(options) {
  animate({
    duration: options.duration || 300,
    timing: function(timeFraction) {
      return (1 - Math.cos(timeFraction * Math.PI)) / 2;
    },
    draw: function(progress) {
      var transform = 'scale(' + (1 - progress * 0.1).toFixed(4) + ')';
      options.el.style.transform = transform;
      options.el.style.webkitTransform = transform;
    },
    callback: options.callback
  });
}



var zoomIn = function(options) {
  animate({
    duration: options.duration || 300,
    timing: function(timeFraction) {
      return (1 - Math.cos(timeFraction * Math.PI)) / 2;
    },
    draw: function(progress) {
      var transform = 'scale(' + (0.9 + progress * 0.1).toFixed(4) + ')';
      options.el.style.transform = transform;
      options.el.style.webkitTransform = transform;
    },
    callback: options.callback
  });
}


var rotator = function(el, defaultConfig) {
  var __swiper;
  var axis = 'horizontal';

  var init = function(direction, callback) {
    axis = direction || 'horizontal';

    var swiper = getSwiper();

    var config = Object.assign({}, defaultConfig, {
      direction: axis,
      mousewheel: false,
      keyboard: false,
      allowTouchMove: false,
      init: false,
      initialSlide: swiper ? swiper.activeIndex : 0
    });

    swiper = new Swiper(el, config);
    swiper.once('init', function() {
      setSwiper(swiper);
      setTimeout(function() {
        helper.runCallback(callback);
      }, 50)
    });

    swiper.init();
  }

  var getSwiper = function() {
    return __swiper;
  }

  var setSwiper = function(swiper) {
    __swiper = swiper
  }

  var destroy = function(callback) {
    var swiper = getSwiper();

    swiper.once('destroy', callback);
    swiper.destroy(false, true);
  }

  var rotate = function(changeMethod, callback) {
    var swiper = getSwiper();

    swiper.once('slideChangeTransitionEnd', callback);

    swiper[changeMethod](500);
  }

  init();

  return {
    rotate: function (direction, callback) {
      var newAxis, changeMethod = 'slidePrev';

      switch(direction) {
        case 'top':
          newAxis = 'vertical';
          changeMethod = 'slidePrev';
          break;

        case 'right':
          newAxis = 'horizontal';
          changeMethod = 'slideNext';
          break;

        case 'bottom':
          newAxis = 'vertical';
          changeMethod = 'slideNext';
          break;

        default:
        case 'left':
          newAxis = 'horizontal';
          changeMethod = 'slidePrev';
          break;
      }

      var doRotate = function() {
        rotate(changeMethod, callback);
      }

      if (newAxis != axis) {
        destroy(function(activeIndex) {
          init(newAxis, doRotate, activeIndex);
        });
      }
      else {
        doRotate();
      }
    },

    addSlide: function(page, position, callback) {
      var appendMethod;
      var slide = makeEl('div', tpl__rotatorSlide());
      slide.appendChild(page);

      switch(position) {
        case 'top':
        case 'left':
          appendMethod = 'prependSlide';
          break;
        case 'right':
        case 'bottom':
          appendMethod = 'appendSlide';
          break;
      }

      var swiper = getSwiper();
      swiper[appendMethod](slide);

      swiper.once('update', callback);

      swiper.update();
    },

    remove: function(page, callback) {
      callback = callback || function(){};
      var swiper = getSwiper();


      for (var i = 0; i < swiper.slides.length; i++) {
        if (swiper.slides[i] === page.parentNode) {
          swiper.removeSlide(i);
          swiper.once('update', callback);
          swiper.update();
          break;
        }
      }
    }
  }
}
;
;
var helper = {};


/*
  "Безопасный" запуск callback.
*/

helper.runCallback = function(callback) {
  if (typeof callback === 'function') {
    callback.apply(callback, Array.prototype.slice.call(arguments, 1));
  }
}


/*
  Экранирование символов
*/

helper.escapeHtml = function(text) {
  var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };

  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}


/*
  Определение браузера
*/

helper.detectBrowser = function(){
  var ua = navigator.userAgent, tem,
  M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];

  if (/trident/i.test(M[1])) {
    tem =  /\brv[ :]+(\d+)/g.exec(ua) || [];

    return 'Internet Explorer ' + (tem[1] || '');
  }

  if (M[1] === 'Chrome') {
    tem = ua.match(/\b(OPR|Edge)\/(\d+)/);

    if (tem != null) {
      return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
  }

  M = M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];

  if ((tem = ua.match(/version\/(\d+)/i)) != null) {
    M.splice(1, 1, tem[1]);
  }

  return M.join(' ');
}


/*
  Загрузка картинок
*/

helper.isImageLoaded = function(elImg) {
  return !(!elImg.complete || (typeof elImg.naturalWidth !== "undefined" && elImg.naturalWidth === 0));
}

helper.loadImage = function(src, callback) {
  var elImg;

  if (src instanceof Element) {
    if (src.tagName.toLowerCase() === 'img') {
      elImg = src;
    }
    else {
      var backgroundImage = this.style.backgroundImage;
      if (! backgroundImage) return;

      src = backgroundImage.slice(4, -1).replace(/"/g, "");
    }
  }

  if (typeof src === 'string') {
    elImg = document.createElement('img');
    elImg.src = src;
  }

  if (elImg) {
    var handler = function() {
      callback();
      this.removeEventListener('load', handler);
    }

    elImg.addEventListener('load', handler);

    if (helper.isImageLoaded(elImg)) {
      callback();
    }
  }
}
;
;
(function(window){
    window.performance = (window.performance || {
        offset: Date.now(),
        now: function now(){
            return Date.now() - this.offset;
        }
    });
}(window));

if (!Object.assign) {
  Object.defineProperty(Object, 'assign', {
    enumerable: false,
    configurable: true,
    writable: true,
    value: function(target) {
      'use strict';
      if (target === undefined || target === null) {
        throw new TypeError('Cannot convert first argument to object');
      }

      var to = Object(target);
      for (var i = 1; i < arguments.length; i++) {
        var nextSource = arguments[i];
        if (nextSource === undefined || nextSource === null) {
          continue;
        }
        nextSource = Object(nextSource);

        var keysArray = Object.keys(Object(nextSource));
        for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
          var nextKey = keysArray[nextIndex];
          var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
          if (desc !== undefined && desc.enumerable) {
            to[nextKey] = nextSource[nextKey];
          }
        }
      }
      return to;
    }
  });
}

if (!Array.prototype.find) {
  Array.prototype.find = function(predicate) {
    if (this === null) {
      throw new TypeError('Array.prototype.find called on null or undefined');
    }
    if (typeof predicate !== 'function') {
      throw new TypeError('predicate must be a function');
    }
    var list = Object(this);
    var length = list.length >>> 0;
    var thisArg = arguments[1];
    var value;

    for (var i = 0; i < length; i++) {
      value = list[i];
      if (predicate.call(thisArg, value, i, list)) {
        return value;
      }
    }
    return undefined;
  };
}
;
;
window.onload = function() {
  setTimeout(function() {
    window.onpopstate = function() {
      window.location.reload();
    }
  }, 0);
}

var callbacksChain = function() {
  var list = [];

  return {
    add: function(callback) {
      if (callback instanceof Array ) {
        callback.forEach(function(item) {
          this.add(item);
        });
        return;
      }

      if (typeof callback !== 'function') return;

      list.push(callback);
    },
    run: function(callback) {
      list.reverse().reduce(function(a, func) {
        return function() {
          func(a);
        }
      }, callback)();
    }
  }
}

inputFillCheck = function(querySelector) {
  [].forEach.call(document.querySelectorAll(querySelector), function(inputEl) {
    inputEl.addEventListener( 'focus', onInputFocus );
    inputEl.addEventListener( 'blur', onInputBlur );
    inputEl.addEventListener( 'change', onInputChange );

    onInputChange({target:inputEl});
  });

  function onInputFocus( ev ) {
    ev.target.parentNode.classList.add('input-item--filled');
    ev.target.parentNode.classList.add('input-item--focus');
  }

  function onInputBlur( ev ) {
    ev.target.parentNode.classList.remove('input-item--focus');
    if (ev.target.value.trim() === '') {
      ev.target.parentNode.classList.remove('input-item--filled');
    }
  }

  function onInputChange( ev ) {
    if (ev.target.value.trim() !== '') {
      ev.target.parentNode.classList.add('input-item--filled');
    }
  }
}



var findObjectPosition = function(obj) {
  var curtop = 0;
  if (obj.offsetParent) {
    do {
      curtop += obj.offsetTop;
    } while (obj = obj.offsetParent);

    return [curtop];
  }
}

var makeEl = function(tagName, HTMLContent) {
  var el = document.createElement(tagName);
  el.innerHTML = HTMLContent;
  return el.firstChild;
}
;
;
/*
  Страница услуг.
*/

var tpl__servicePage = function(data, menu) {
  return (
    '<article class="services-page js-page" style="color: ' + data.color + '">' +
      tpl__header() +
      '<div class="container">' +
        '<div class="services-page__container swiper-container js-page-cube">' +
          '<div class="swiper-wrapper js-scroll-container">' +
            '<section class="services-page__row swiper-slide">' +
              '<div class="services-page__content">' +
                tpl__rowHead(data.title, data.text) +
              '</div>' +
            '</section>' +

            data.rows.reduce(function(a, item) {
              return a + (
                '<section class="services-page__row swiper-slide">' +
                  '<div class="services-page__content">' +
                    tpl__row(item) +
                  '</div>' +
                '</section>'
              );
            }, '') +

            '<section class="services-page__row swiper-slide">' +
              '<div class="services-page__content">' +
                tpl__serviceMenu(menu, data.slug) +
              '</div>' +
            '</section>' +

            '<section class="services-page__row services-page__row--contacts swiper-slide js-contacts">' +
              '<div class="services-page__content">' +
                tpl__contacts(core) +
              '</div>' +
            '</section>' +

          '</div>' +
        '</div>' +
      '</div>' +
    '</article>'
  );
}


/*
  Шапка страницы услуг.
*/

var tpl__header = function() {
  return (
    '<div class="header">' +
      '<div class="container">' +
        '<div class="header__layout g-flex g-flex--justify">' +
          '<div class="header__logo">' +
            '<a class="js-inner-link" href="/">' +
              '<svg class="logo">' +
                '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-logo"></use>' +
              '</svg>' +
            '</a>' +
          '</div>' +
          '<div class="header__menu-button">' +
            tpl__menuBtn() +
          '</div>' +
        '</div>' +
      '</div>' +
    '</div>'
  );
}


/*
  Первый блок страниц услуг
*/

var tpl__rowHead = function(title, text) {
  return (
    '<div class="row-head">' +
      '<div class="container">' +
        '<h1 class="row-head__title title">' +
          title +
        '</h1>' +
        '<div class="row-head__text text">' +
          text +
        '</div>' +
      '</div>' +
    '</div>'
  );
}


/*
  Блок страниц услуг с картинками
*/

var tpl__row = function(data) {
  return (
    '<div class="row row--' + data.imagePosition + '">' +
      '<div class="row__layout">' +
        '<div class="row__image-wrap">' +
          '<img class="row__image image-loader js-image-loader" data-src="' + data.image + '">' +
        '</div>' +
        '<div class="row__content">' +
          '<div class="row-content">' +
            (data.label ? '<div class="row-content__label">' + data.label + '</div>' : '') +
            '<h2 class="row-content__title title">' + data.title + '</h2>' +
            '<div class="row-content__text text">' + data.text + '</div>' +
            (data.link ?
              ('<div class="row-content__link">' +
                '<a class="link text" target="_blank" href="' + data.link + '">' +
                  data.prettyLink +
                '</a>' +
              '</div>') :
              ('<div class="row-content__button">' +
                '<span class="btn js-order-btn" data-id="' + data.id + '">' +
                  '<span class="btn__label">Заказать</span>' +
                '</span>' +
              '</div>')
            ) +
          '</div>' +
        '</div>' +
      '</div>' +
    '</div>'
  );
}


/*
  Блок - меню услуг
*/

var tpl__serviceMenu = function(menu, doNotUseSlug) {
  return (
    '<div class="row-head">' +
      '<div class="container">' +
        '<h2 class="services-menu__title title">Все услуги</h2>' +
        '<div class="services-menu__layout g-flex">' +
          menu.reduce(function(a, item) {
            if (item.slug === doNotUseSlug) return a;

            return a + (
              '<div class="services-menu__item">' +
                '<a class="image-link js-inner-link" href="' + item.slug + '">' +
                  '<img class="image-link__image" src="' + item.image + '" alt="' + helper.escapeHtml(item.title) + '">' +
                  '<div class="image-link__title">' +
                    item.title +
                  '</div>' +
                '</a>' +
              '</div>'
            );
          }, '') +
        '</div>' +
      '</div>' +
    '</div>'
  );
}


/*
  Блок контактов. Включает в себя шаблон формы.
*/

var tpl__contacts = function(core) {
  return (
    '<div class="contacts" itemscope itemtype="http://schema.org/Organization">' +
      '<div class="container">' +
        '<h2 class="contacts__title title">Контакты</h2>' +
        '<div class="contacts__layout">' +
          '<div class="contacts-layout g-flex">' +
            '<div class="contacts-layout__item">' +
              '<div class="contacts-item">' +
                '<div class="contacts-item__icon">' +
                  '<svg>' +
                    '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-phone"></use>' +
                  '</svg>' +
                '</div>' +
                '<a class="contacts-item__data" href="tel:"' + core.siteData('phone').replace(/\s/gi, "") + '" itemprop="telephone">' +
                  core.siteData('phone').replace(/\s/gi, "&thinsp;") +
                '</a>' +
              '</div>' +
            '</div>' +

            '<div class="contacts-layout__item">' +
              '<div class="contacts-item">' +
                '<div class="contacts-item__icon">' +
                  '<svg>' +
                    '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-message"></use>' +
                  '</svg>' +
                '</div>' +
                '<a class="contacts-item__data" href="mailto:"' + core.siteData('email') + '" itemprop="email">' +
                  core.siteData('email') +
                '</a>' +
              '</div>' +
            '</div>' +
          '</div>' +
        '</div>' +

        '<div class="contacts__form">' +
          tpl__form(core) +
        '</div>' +

        '<div class="contacts__address">' +
          '<div class="contacts-adress" itemscope itemprop="address" itemtype="http://schema.org/PostalAddress">' +
            '<span itemprop="addressLocality">' + core.siteData('addressLocality') + '</span>' +
            '<span itemprop="streetAddress">' + core.siteData('streetAddress') + '</span>' +
          '</div>' +
        '</div>' +
      '</div>' +
    '</div>'
  );
}


/*
  Шаблон формы заказа.
*/

var tpl__form = function(core) {
  return (
    '<form class="order-form js-form" action="" method="POST">' +
      '<input class="js-service-id" type="hidden" name="service">' +
      '<h3 class="order-form__title">Начать работу</h3>' +
      '<div class="order-form__info">Оставьте свои данные и наш менеджер свяжется с Вами</div>' +
      '<div class="order-form__layout g-flex">' +
        '<div class="order-form__cell">' +
          '<div class="order-form__item">' +
            '<label class="input-item">' +
              '<input class="input-item__input js-input" id="order-form-name" type="text" name="name" minlength="2" maxlength="50" required>' +
              '<label class="input-item__label" for="order-form-name">Ваше имя</label>' +
              '<span class="input-item__error"></span>' +
            '</label>' +
          '</div>' +

          '<div class="order-form__item">' +
            '<label class="input-item">' +
              '<input class="input-item__input js-input" id="order-form-email" type="text" name="email" minlength="6" maxlength="50" required>' +
              '<label class="input-item__label" for="order-form-email">E-mail</label>' +
              '<span class="input-item__error"></span>' +
            '</label>' +
          '</div>' +
        '</div>' +

        '<div class="order-form__cell">' +
          '<label class="input-item">' +
            '<textarea class="input-item__textarea js-input" id="order-form-comment" name="comment" maxlength="2048"></textarea>' +
            '<label class="input-item__label" for="order-form-comment">Комментарий</label>' +
            '<span class="input-item__error"></span>' +
          '</label>' +
        '</div>' +
      '</div>' +

      '<div class="order-form__layout g-flex">' +
        '<div class="order-form__cell order-form__cell--btn-left">' +
          '<button class="btn-lighted" type="submit">' +
            '<span class="btn-lighted__label">отправить данные</span>' +
          '</button>' +
        '</div>' +

        '<div class="order-form__cell order-form__cell--btn-right">' +
          '<a class="btn-empty" href="' + core.siteData('brief') + '" target="_blank">' +
            '<span class="btn-empty__label">скачать бриф</span>' +
          '</a>' +
        '</div>' +
      '</div>' +
    '</form>'
  );
}


/*
  Кнопка меню.
*/

var tpl__menuBtn = function() {
  return (
    '<div class="menu-btn js-menu-btn">' +
      'M <span class="menu-btn__stairs">E</span> N U' +
    '</div>'
  );
}


/*
  Шаблон главной страниц.
*/

var tpl__homepage = function(data, menu) {
  return (
    '<div class="homepage js-page">' +
      tpl__servicesHomeNav(menu) +
      '<div class="container">' +
        '<div class="homepage__layout">' +
          '<div class="homepage__content">' +
            '<div class="homepage__head">' +
              '<div class="homepage-head">' +
                '<div class="homepage-head__agency">' +
                  data.agency +
                '</div>' +
                '<div class="homepage-head__name js-site-name">' +
                  data.title +
                '</div>' +
                '<div class="homepage-head__slogan">' +
                  data.slogan +
                '</div>' +
              '</div>' +
            '</div>' +

            '<div class="homepage__slider-menu">' +
              tpl__portfolioMenu(data.menu) +
            '</div>' +

            '<div class="homepage__menu-btn">' +
              tpl__menuBtn() +
            '</div>' +
          '</div>' +
        '</div>' +
      '</div>' +
    '</div>'
  );
}


/*
  Навигация на главной странице.
*/

var tpl__servicesHomeNav = function(menu) {
  return (
    '<div class="nav services-nav js-services-nav">' +
      menu.reduce(function(a, item) {
        return a + (
          '<div class="services-nav__item services-nav__item--' + item.position + '">' +
            '<a class="services-nav__link js-inner-link" href="' + item.slug + '">' +
              item.title +
            '</a>' +
          '</div>'
        );
      }, '') +
    '</div>'
  );
}


/*
  Ссылки портфолио на главной.
*/

var tpl__portfolioMenu = function(menu) {
  return (
    '<div class="portfolio-menu">' +
      menu.reduce(function(a, item) {
        return a + (
          '<div class="portfolio-menu__item">' +
            '<a class="portfolio-menu__link js-bg-item" href="' + item.link + '" data-id="' + item.id + '" target="_blank">' +
              item.title +
            '</a>' +
          '</div>'
        );
      }, '') +
    '</div>'
  );
}


var tpl__rotatorSlide = function() {
  return '<div class="pages-rotator__page swiper-slide"></div>';
}
;
;
'use strict';


/*
  Ядрышко.
*/

var core = {
  isReady: false,

  canChangePage: false,

  initChain: new callbacksChain(),

  __siteData: {},

  homepageController: false,
  currentPageController: false,

  dataUrls: {
    'config': '/data/config.json',
    'pages': '/data/pages.json',
  },

  browser: helper.detectBrowser(),
}


/*
  Получение различной информации сайта/компании.
*/

core.siteData = function(identif) {
  return core.__siteData[identif];
}


/*
  Мероприятия. Точнее события.
*/

core.events = {
  __list: {},

  on: function(eventName, fn) {
    var _ = this;

    if (typeof eventName === 'string' && typeof fn === 'function') {
      if (! _.__list[eventName]) {
        _.__list[eventName] = [];
      }

      _.__list[eventName].push(fn);

      return function() {
        var index = _.__list[eventName].indexOf(fn);
        delete _.__list[eventName][index];

        if (_.__list[eventName].length === 0) {
          _.__list[eventName].splice(index, 1);
        }
        else {
          _.__list[eventName].sort();
        }
      }
    }
  },

  one: function(eventName, fn) {
    var destroyer = this.on(eventName, function() {
      helper.runCallback(fn);
      destroyer();
    });
  },

  trigger: function() {
    var _ = this;

    var eventName = arguments[0],
      args = Array.prototype.slice.call(arguments, 1);

    if (eventName in _.__list && 'length' in _.__list[eventName]) {
      _.__list[eventName].forEach(function(e) {
        e.apply(e, args);
      });
    }
  }
}


/*
  Хранилище.
*/

core.storage = {
  __data: {},
  __keys: {},

  isAvailable: 'localStorage' in window && window['localStorage'] !== null,

  add: function(identif, data) {
    this.__data[identif] = data;

    if (!this.isAvailable) return;
    if (typeof data === 'function') return;

    if (typeof data !== 'string') {
      data = JSON.stringify(data);
    }

    localStorage.setItem(identif, JSON.stringify(data));
  },

  get: function(identif) {
    if (identif in this.__data) {
      return ((typeof this.__data[identif] === 'string') ?
        this.__data[identif] :
        JSON.parse(JSON.stringify(this.__data[identif]))
      );
    }

    if (!this.isAvailable) return;

    var data = localStorage.getItem(identif);

    try {
      data = JSON.parse(data);
    } catch(e){}

    this.__data[identif] = data;

    return data;
  },


  /*
    Достаем данные из хранилища.
    Если данных нет, или хранилище не доступно - используем getDataFunc для получения данных.
  */

  wrapGetter: function(identif, getDataFunc, callback) {
    var _ = this;

    var data = this.get(identif);

    if (data) {
      helper.runCallback(callback, data);
    }
    else {
      try {
        getDataFunc(function(data) {
          _.add(identif, data);
          helper.runCallback(callback, data);
        });
      }
      catch(e) {
        console.log(e);
      }
    }
  }
}


/*
  Запрос на сервер.
*/

core.request = function(url, dataType, callback) {
  var request = new XMLHttpRequest();

  request.open('POST', url, true);
  request.setRequestHeader("X-Requested-With", "XMLHttpRequest");

  request.onload = function(){
    if (this.status >= 200 && this.status < 400) {
      var responseData = this.responseText;

      if (dataType === 'json') {
        responseData = JSON.parse(responseData);
      }

      helper.runCallback(callback, responseData);
    } else {
      throw new Error(this.statusText);
    }
  }

  request.send();
}


/*
  Получение контроллеров для страниц.
  Таким образом, для разных страниц будут свои подключаемые скрипты.
*/

core.controllers = {
  __list: {},

  add: function(identif, controller) {
    this.__list[identif] = controller;
  },

  get: function(identif) {
    if (identif in this.__list) {
      return this.__list[identif];
    }

    return false;
  }
}


/*
  Роуты.
  Здесь хранятся зависимости - какой контроллер, на какой странице использовать.
*/

core.routes = {
  __list: {},

  add: function(path, controllerIdentif) {
    this.__list[path] = controllerIdentif;
  },

  get: function(path) {
    if (path in this.__list) {
      return this.__list[path];
    }

    return false;
  }
}


/*
  Эвенты срабатывающие при готовности ядра.
*/

core.ready = function (callback) {
  if (core.isReady) {
    helper.runCallback(callback);
  }
  else {
    core.events.one('ready', callback);
  }
}


/*
  Получение контроллера для урла.
*/

core.getPageController = function(url) {
  var controller = core.controllers.get(core.routes.get(getPath(url)));

  if (controller) {
    return function(data) {
      return new controller(data, core);
    }
  }

  return false;
}

core.controllerExist = function(url) {
  return !! core.controllers.get(core.routes.get(getPath(url)));
}

var getPath = function(url) {
  var l = document.location;
  return (url ? url.replace(l.origin, '') : l.pathname).replace('.html', '');
}


/*
  Загрузка данных с сервера.
*/

var downloadData = function(identif, callback) {
  core.request(core.dataUrls[identif], 'json', callback);
}


/*
  Поиск данных в теле страницы.
*/

var findDataOnPage = function(identif) {
  if (!identif) false;

  return window['__' + identif] || false;
}


/*
  Загрузка данных.
*/

core.loadData = function(identif, callback) {
  var data;

  if (data = findDataOnPage(identif)) {
    helper.runCallback(callback, data);
  }
  else {
    downloadData(identif, callback);
  }
}


/*
  Для ie отключаем кубический эффект - не тянет.
*/

core.swiperConfig = function() {
  var config = {
    direction: 'vertical',
    mousewheel: true,
    pagination: false,
  }

  var browser = core.browser.toLowerCase();
  // В сафари и ie - некорректно работает кубический эффектус
  if (browser.indexOf('internet explorer') === -1 && browser.indexOf('safari') === -1) {
    config.effect = 'cube';
    config.cubeEffect = {
      shadow: false,
      slideShadows: false,
    };
  }

  return config;
}


/*
  Изменение мета-данных страницы.
*/

core.setMeta = function (metaData) {
  var meta = document.getElementsByTagName("meta");
  var name, property;

  for (var i = 0; i < meta.length; i++) {
    name = meta[i].name.toLowerCase();
    if (name in metaData) {
      meta[i].content = metaData[name];
    }

    property = meta[i].attributes.property;

    if (property) {
      property = property.name.toLowerCase();
      if (property in metaData) {
        meta[i].content = metaData[property];
      }
    }
  }

  var title = document.getElementsByTagName("title")[0];

  if ('title' in metaData) {
    title.innerHTML = metaData['title'];
  }
}


/*
  Изменение url страницы.
*/

core.changeUrl = (function() {
  var isAvailable = !!(window.history && history.pushState);

  return function(url) {
    if (url !== window.location.href) {
      if (isAvailable) {
        history.pushState({}, '', url);
      } else {
        window.location.href = url;
      }
    }
  }
}());


/*
  Инициализация поворачивателя.
*/

core.initRotator = function() {
  core.rotator = new rotator(document.querySelector('.js-pages-rotator'), core.swiperConfig());
}


/*
  Запуск инициализации.
  Выполняем последовательно.
  В скорости не должны потерять, а так - проще контролировать процесс.
*/

core.init = function() {
  core.initRotator();

  core.initChain.run(function() {
    core.isReady = true;
    core.events.trigger('ready');
  });
}


/*********

  Инициализация ядра.

*********/


/*
  Чистим хранилище, если данные сайта были изменены.
*/

core.initChain.add(function(callback) {
  var cacheId = findDataOnPage('cacheId');

  if (cacheId && core.storage.isAvailable) {
    if (cacheId !== core.storage.get('cacheId')) {
      localStorage.clear();
    }
  }

  helper.runCallback(callback);
});


/*
  Добавление контроллеров и роутов.
  Происходит асинхронно, т.к. скрипты ниже сразу могут быть не видны.
*/

core.initChain.add(function(callback) {
  setTimeout(function() {
    /*
      Регистрируем имеющиеся контроллеры.
    */
    core.controllers.add('home', homepageController);
    core.controllers.add('services', servicesPageController);

    core.routes.add('/', 'home');

    helper.runCallback(callback);
  }, 1);
});


/*
  Загрузка конфига.
*/

core.initChain.add(function(callback) {
  var dataIdentif = 'siteData';

  core.storage.wrapGetter(dataIdentif, function(callback){
    core.loadData(dataIdentif, callback);
  }, function(data) {
    core.__siteData = data;

    callback();
  });
});


/*
  Загрузка данных о страницах.
*/

core.initChain.add(function(callback) {
  var dataIdentif = 'pages';

  core.storage.wrapGetter(dataIdentif, function(c) {
    core.loadData(dataIdentif, c);
  }, function(data) {
    data.forEach(function(el) {
      if (el.slug !== '/') {
        core.routes.add(el.slug, 'services');
      }
    });

    callback();
  });
});


/*
  Запускаем велосипед.
*/

var getPageData = function(url) {
  return core.storage.get('pages').find(function(item) {
    return item.slug === url;
  });
}

core.ready(function() {
  var url = document.location.pathname.replace('.html', '');
  core.menu.init();

  if (!core.controllerExist(url)) {
    return;
  }

  core.currentPageController = core.getPageController(url)(getPageData(url));

  core.currentPageController.init(document.querySelector('.js-page'));
  core.currentPageController.ready();

  core.initHomePage(function() {
    core.canChangePage = true;
  });
});

document.addEventListener('DOMContentLoaded', function() {
  core.init();
});



/*
  Инициализация внутренней навигации.
*/

core.initInnerLinks = function(container) {
  var links = container.querySelectorAll('.js-inner-link');
  [].forEach.call(links, function(el) {
    if (el.classList.contains('is-ready')) return;

    el.addEventListener('click', function(e) {
      e.preventDefault();

      core.changePage(this.href);
    });

    el.classList.add('is-ready');
  });
}




/*
  Меню
*/

core.getMenu = function() {
  return core.storage.get('pages').reduce(function(result, item) {
    if (item.useInMenu) {
      result.push({
        slug: item.slug,
        image: item.servicesMenuImage,
        title: item.title,
        position: item.pagePosition,
        menuLabel: item.menuLabel
      });
    }

    return result;
  }, []);
}


/*
  Блокирование окна на время анимации.
*/

var blockWindow = function() {
  document.querySelector('html, body').classList.add('is-blocked');
  core.canChangePage = false;
}

var unblockWindow = function() {
  document.querySelector('html, body').classList.remove('is-blocked');
  core.canChangePage = true;
}


core.menu = {
  elMenu: null,
  init: function() {
    var _ = this;
    _.elMenu = document.querySelector('.js-main-menu');
    var elClose = document.querySelector('.js-menu-close');

    if (!(_.elMenu && elClose)) return;

    elClose.addEventListener('click', _.close.bind(_));

    _.initMenuBtn();
    _.initMenuLinks();
  },

  initMenuBtn: function(container) {
    var _ = this;

    container = container || document;
    var elOpenMenu = container.querySelector('.js-menu-btn');

    if (!elOpenMenu) return;

    elOpenMenu.addEventListener('click', _.open.bind(_));
  },

  initMenuLinks: function() {
    var _ = this;

    [].forEach.call(document.querySelectorAll('.js-menu-link'), function(el){
      el.addEventListener('click', function(e) {
        e.preventDefault();

        var url = this.href;
        _.close(function() {
          core.changePage(url);
        });
      });
    });
  },

  open: function() {
    this.elMenu.classList.add('is-showed');
  },

  close: function(callback) {
    var _ = this;

    var handler = function() {
      helper.runCallback(callback);
      _.elMenu.removeEventListener('transitionend', handler);
    }

    _.elMenu.addEventListener('transitionend', handler);
    _.elMenu.classList.remove('is-showed');
  }
}


/*
  Получение обратного направления.
*/

var invertDirection = function(position) {
  var result;

  switch(position) {
    case 'top':
        result = 'bottom';
      break;

    case 'right':
        result = 'left';
      break;

    case 'bottom':
        result = 'top';
      break;

    case 'left':
    default:
        result = 'right';
      break;
  }

  return result;
}


/*
  Для красивого отображения поворота, нам нужна готовая главная страница
*/

core.initHomePage = function(callback) {
  if (core.homepageController) {
    helper.runCallback(callback);
    return;
  }

  var currentPageData = core.currentPageController.getPageData();
  if (currentPageData.slug === '/') {
    core.homepageController = core.currentPageController;
    helper.runCallback(callback);
    return;
  }

  var homepageData = getPageData('/');
  core.homepageController = core.getPageController('/')(homepageData);
  var position = invertDirection(currentPageData.pagePosition);

  core.homepageController.makePage();
  core.homepageController.init();
  core.rotator.addSlide(core.homepageController.getPageEl(), position, callback);
}


/*
  Смена страницы.
*/

core.changePage = function(url) {
  var newPageData, newController;
  if (!core.canChangePage) return;

  if (url.indexOf(window.location.origin) !== 0) {
    window.location.href = url;
  }

  var pathname = getPath(url);

  if (pathname === window.location.pathname) return;

  core.canChangePage = false;

  if (pathname === '/' && core.homepageController) {
    newPageData = core.homepageController.getPageData();
    newController = core.homepageController;
  }
  else {
    if (!core.controllerExist(pathname)) {
      window.location.href = pathname;
      return;
    }

    newPageData = getPageData(pathname);

    newController = core.getPageController(pathname)(newPageData);
  }

  var currentPageData = core.currentPageController.getPageData();

  var animationChain = new callbacksChain();

  var elScaler = document.querySelector('.js-page-scaler');


  /*
    Блокируем экран.
    Показываем главную страницу, если она была скрыта.
  */

  animationChain.add(function(callback) {
    blockWindow();
    core.homepageController.getPageEl().classList.remove('is-hidden');
    helper.runCallback(callback);
  });


  /*
    Отдаляем куб.
  */

  animationChain.add(function(callback) {
    zoomOut({
      el: elScaler,
      callback: callback
    })
  });


  /*
    Если конечная страница - не главная, то создаем ее.
  */

  if (newPageData.slug !== '/') {
    animationChain.add(function(callback) {
      var page = newController.makePage();

      core.rotator.addSlide(page, newPageData.pagePosition, function() {
        newController.init();
        helper.runCallback(callback);
      });
    });
  }


  /*
    Меняем урл
  */

  animationChain.add(function(callback) {
    core.changeUrl(url);
    helper.runCallback(callback);
  });


  /*
    Если текущая страница - не главная, надо для начала повернуть на главную, при любой конечной странице.
    Затем удаляем текущую.
  */

  if (currentPageData.slug !== '/') {
    var direction = invertDirection(currentPageData.pagePosition);

    animationChain.add(function(callback) {
      core.rotator.rotate(direction, callback);
    });

    animationChain.add(function(callback) {
      core.rotator.remove(core.currentPageController.getPageEl(), callback);
    });
  }


  /*
    Если конечная страница - не главная - вертим на нее.
  */

  if (newPageData.slug !== '/') {
    animationChain.add(function(callback) {
      core.rotator.rotate(newPageData.pagePosition, callback)
    });
  }


  /*
    Приближаем куб обратно.
  */

  animationChain.add(function(callback) {
    zoomIn({
      el: elScaler,
      callback: callback
    });
  });

  /*
    Разбираемся к чему пришли.
  */

  animationChain.run(function(){
    newController.ready();

    core.currentPageController = newController;
    core.initHomePage(function() {
      unblockWindow();

      if (newPageData.slug !== '/') {
        core.homepageController.getPageEl().classList.add('is-hidden');
      }
    });
  })
}
;
;
var homepageController = function(data, core) {
  var page;

  var currentId;
  var canLoadImages = true;
  var images = [];
  var elColorChanger;


  /*
    Формирование функции смены слайда.
  */

  var slideChanger = function(item) {
    return function() {
      currentId = item.id;
      changeLetterColor(item.color);

      helper.loadImage(item.image, function() {
        if (item.id === currentId) {
          page.style.backgroundImage = 'url(' + item.image + ')';
        }
      });
    }
  }


  /*
    Изменение цвета буквы.
  */

  var initColorChanger = function() {
    elColorChanger = page.querySelector('.js-color-changer');
    var elSiteName = page.querySelector('.js-site-name');
    if (!elSiteName) return;

    elColorChanger = document.createElement('span');
    elColorChanger.innerText = elSiteName.innerText.slice(0, 1);
    elSiteName.innerText = elSiteName.innerText.slice(1);

    elSiteName.insertBefore(elColorChanger, elSiteName.firstChild);
  }

  var changeLetterColor = function(color) {
    if (elColorChanger) {
      elColorChanger.style.color = color;
    }
  }


  /*
    Инициализация слайдера
  */

  var initPortfolioSlider = function(page) {
    canLoadImages = true;

    var sliderControls = page.querySelectorAll('.js-bg-item');

    [].forEach.call(sliderControls, function(el, index) {
      var id = el.getAttribute('data-id');

      var item = data.menu.find(function(item) {
        return item.id == id;
      });

      var onHover = slideChanger(item);

      el.addEventListener('mouseover', function(e) {
        onHover();

        [].forEach.call(sliderControls, function(ele) {
          ele.classList.remove('is-active');
        });

        el.classList.add('is-active');
      });

      if (index === 0) {
        el.classList.add('is-active');
        onHover();
      }

      addImageToChain(item.image);
    });

    loadImagesChain();
  }


  /*
    Дозагрузка картинок слайдера.
  */

  var addImageToChain = function(src) {
    var imageNotExist = ! images.find(function(item) {
      return item.src === src;
    });

    if (imageNotExist) {
      images.push({
        loaded: false,
        src: src,
      });
    }
  }

  var loadImagesChain = function() {
    if (!canLoadImages) return;

    var image = images.find(function(item){
      return !item.loaded;
    });

    if (image) {
      var elImg = document.createElement('img');
      elImg.src = image.src;

      helper.loadImage(elImg, function() {
        image.loaded = 1;
        image.el = elImg;
        loadImagesChain();
      });
    }
  }


  /*
    Красивое появление ссылок на другие страницы
  */

  var initServicesNav = function() {
    var elServiceNav = page.querySelector('.js-services-nav');

    if (elServiceNav) {
      elServiceNav.classList.add('is-ready');
    }
  }


  /*
    Инициализация домашней страницы
  */

  var init = function(p) {
    page = page || p;

    initColorChanger(page);
    initPortfolioSlider(page);
    core.initInnerLinks(page);
    core.menu.initMenuBtn(page);
  }


  /*
    Вызывается как только страница должна быть
  */

  var ready = function() {
    initServicesNav(page);
  }


  /*
    Создание страницы
  */

  var makePage = function() {
    page = makeEl('div', tpl__homepage(data, core.getMenu()));

    return page;
  }


  /*
    Разрушение страницы
  */

  var destroyPage = function(callback) {
    canLoadImages = false;

    helper.runCallback(callback);
  }

  return {
    init: init,
    ready: ready,
    getPageData: function() {
      return data;
    },
    getPageEl: function() {
      return page;
    },
    makePage: makePage,
    destroyPage: destroyPage,
  }
}
;
;
var servicesPageController = function (data, core) {
  var page;
  var swiperHandler;

  var init = function(p) {
    page = page || p;
    if (! page) return;

    var links = page.querySelectorAll('.js-inner-link');

    swiperHandler = new swiperController(page.querySelector('.js-page-cube'));
    loadImages();
    initOrderBtns();
    inputFillCheck('.js-input');
    core.initInnerLinks(page);
    core.menu.initMenuBtn(page);
  }


  /*
    Загрузка изображений.
  */

  var loadImages = function() {
    [].forEach.call(page.querySelectorAll('.js-image-loader'), function(el) {
      if (el.classList.contains('is-loaded')) return;
      var imageSrc = el.getAttribute('data-src');
      helper.loadImage(imageSrc, function() {
        el.src = imageSrc;
        el.classList.add('is-loaded');
        el.removeAttribute('data-src');
      });
    });
  }


  /*
    Переключение между блоками страницы.
  */

  var swiperController = function(el) {
    var swiper = false;
    var timeout;
    var breakpoint = 768;

    var init = function() {
      if (swiper) return;

      swiper = new Swiper(el, core.swiperConfig());
      el.classList.add('is-active');
      core.initInnerLinks(page);
    }

    var destroy = function() {
      if (!swiper) return;

      swiper.destroy(true, true);
      el.classList.remove('is-active');
      swiper = false;
    }

    var checkInitIsNeed = function() {
      (window.innerWidth < breakpoint  ? destroy() : init());
    }

    var onResize = function() {
      timeout = setTimeout(function(){
        checkInitIsNeed();
      }, 50);
    }

    window.addEventListener('resize', onResize);
    checkInitIsNeed();

    return {
      destroy: function() {
        window.removeEventListener('resize', onResize);
        clearTimeout(timeout);
        destroy();
      },
      isOn: function() {
        return !! swiper;
      },
      moveTo: function(el) {
        if (!swiper) return;
        var founded = false;

        for (var i = 0; i < swiper.slides.length; i++) {
          if (swiper.slides[i] === el) {
            founded = true;
            break;
          }
        }

        if (founded) {
          swiper.slideTo(i, 700);
        }
      }
    }
  }


  var initOrderBtns = function() {
    var elContacts = page.querySelector('.js-contacts');
    var elInput = page.querySelector('.js-service-id');

    [].forEach.call(page.querySelectorAll('.js-order-btn'), function(el) {
      el.addEventListener('click', function() {
        if (!page) return;
        elInput.value = el.getAttribute('data-id');

        swiperHandler.isOn() ?
          swiperHandler.moveTo(elContacts) :
          scrollToElem(elContacts);
      });
    });
  }

  var scrollToElem = function(el) {
    var scrollContainer = page.querySelector('.js-scroll-container');
    var start = scrollContainer.scrollTop;
    var fin = findObjectPosition(el);
    var diff = Math.abs(fin - start - 60);

    animate({
      duration: 700,
      timing: function(timeFraction) {
        return (1 - Math.cos(timeFraction * Math.PI)) / 2;
      },
      draw: function(progress) {
        scrollContainer.scrollTop = start + diff * progress;
      }
    });
  }


  /*
    Разрушение страницы.
  */

  var destroyPage = function(callback) {
    swiperHandler.destroy();
    helper.runCallback(callback);
  }


  /*
    Делаем ссылку красивой.
  */

  function prettyLink(link) {
    link = link.replace('http://', '').replace('/', '');
    return link.charAt(0).toUpperCase() + link.slice(1);
  }


  /*
    Формирование страницы.
  */

  var makePage = function() {
    data.rows.forEach(function(item) {
      if (!item.link) return;
      item.prettyLink = prettyLink(item.link);
    });

    page = makeEl('div', tpl__servicePage(data, core.getMenu()));

    return page;
  }

  return {
    init: init,
    ready: function(){},
    getPageData: function() {
      return data;
    },
    getPageEl: function() {
      return page;
    },
    makePage: makePage,
    destroyPage: destroyPage,
  }
}
;