'use strict';
const data = require('../data/pages.json');

function prettyLink(link) {
  link = link.replace('http://', '').replace('/', '');
  return link.charAt(0).toUpperCase() + link.slice(1);
}

module.exports = {
  getData: function() {
    return data;
  },

  getPage: function(slug) {
    let page = data.find(function(item) {
      return (item.slug.indexOf(slug) !== -1);
    });

    page.rows.forEach(function(item) {
      if (!item.link) return;
      item.prettyLink = prettyLink(item.link);
    });

    return page;
  },

  getMenu: function(doNotUseSlug) {
    return data.reduce(function(result, item) {
      if (item.slug !== '/' && item.slug.indexOf(doNotUseSlug) === -1) {
        result.push({
          slug: item.slug,
          image: item.servicesMenuImage,
          title: item.title,
          position: item.pagePosition,
          menuLabel: item.menuLabel
        });
      }

      return result;
    }, []);
  },

  getHomepageData: function() {
    return data[0];
  }
};