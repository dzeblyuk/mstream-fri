const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
let PATHS = {
    base: path.join(__dirname + '/'),
    source: path.join(__dirname, '/pug/'),
    build: path.resolve(__dirname + '../..'),
}

const fs = require('fs');
const files = fs.readdirSync(PATHS.source);

let plugins = [];

for (let i = 0; i < files.length; i++) {
  let filename = files[i];

  if (filename.indexOf('.pug') !== -1 && filename.indexOf('main') !== 0) {
    filename = filename.replace('.pug', '');
    plugins.push(new HtmlWebpackPlugin({
      filename: filename + ".html",
      template: PATHS.source + filename + '.pug',
    }));
  }
}

module.exports = {
    entry: PATHS.base + 'index.js',
    output: {
        path: PATHS.build,
        filename: '[name].js'
    },
    plugins: plugins,
    module: {
        rules: [
            {
                test: /\.pug$/,
                loader: 'pug-loader',
                options: {
                    pretty: true
                }
            }
        ]
    }
};