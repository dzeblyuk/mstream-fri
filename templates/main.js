/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

const data = __webpack_require__(1);

function prettyLink(link) {
  link = link.replace('http://', '').replace('/', '');
  return link.charAt(0).toUpperCase() + link.slice(1);
}

module.exports = {
  getData: function() {
    return data;
  },

  getPage: function(slug) {
    let page = data.find(function(item) {
      return (item.slug.indexOf(slug) !== -1);
    });

    page.rows.forEach(function(item) {
      if (!item.link) return;
      item.prettyLink = prettyLink(item.link);
    });

    return page;
  },

  getMenu: function(doNotUseSlug) {
    return data.reduce(function(result, item) {
      if (item.slug !== '/' && item.slug.indexOf(doNotUseSlug) === -1) {
        result.push({
          slug: item.slug,
          image: item.servicesMenuImage,
          title: item.title,
          position: item.pagePosition,
          menuLabel: item.menuLabel
        });
      }

      return result;
    }, []);
  },

  getHomepageData: function() {
    return data[0];
  }
};

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = [{"slug":"/","agency":"Маркетинговое агенство","title":"M-Stream","slogan":"Ваш товар - наши решения","menu":[{"id":13,"title":"Lead Ocean","color":"#ffcc01","image":"/assets/images/homepage/lead-ocean.jpg","link":"#"},{"id":19,"title":"АС - Щит","color":"#0353e3","image":"/assets/images/homepage/shield.jpg","link":"#"},{"id":15,"title":"Elma Dental","color":"#00d500","image":"/assets/images/homepage/elma.jpg","link":"#"},{"id":16,"title":"English Family","color":"#003aad","image":"/assets/images/homepage/english-family.jpg","link":"#"},{"id":17,"title":"Greedy Gamers","color":"#ff0705","image":"/assets/images/homepage/greedy-gamers.jpg","link":"#"}]},{"slug":"/branding","color":"#ff0203","title":"Brandinggggg","text":"“По внешнему виду не судят только самые непроницательные люди” - справедливо отметил Оскар Уайлд. Считывать и анализировать визуальную информацию – базисная установка психики человека. Именно поэтому высококлассный брендинг – необходимая составляющая успеха.","servicesMenuImage":"/assets/images/branding/branding-service-menu.jpg","menuLabel":"Who We Are","pagePosition":"bottom","useInMenu":true,"rows":[{"id":1,"label":"Launched to market","title":"Фирменный стиль","text":"“В человеке все должно быть прекрасно…” – завещал классик. А в бренде, если он претендует занять место в классике маркетинга и подавно. Айдентика – формула, состоящая из множества элементов, эффективно воплощающая бизнес-задачи и маркетинговые амбиции. В условиях интенсивной конкуренции лицо продукта – прямой путь к опознанию и закреплению в памяти – выходит на первый план. Эксперты маркетинга, брендинга и дизайна агентства M-Stream создадут для вас персональный облик бренда, выгодно выделяющий его на фоне однообразия.","image":"/assets/images/branding/rows/identity.jpg","imagePosition":"left"},{"id":2,"label":"Launched to market","title":"Брендбук","text":"“У нас было: множество продающих текстов, лучший на рынке логотип, уникальная айдентика, гипнослоган и эффективный диджитал. Единственное, что нас беспокоило – это брендбук. Без него мы чувствовали себя как с комплектом мебели IKEA без инструкции.” Знакомая ситуация? Брендбук – священное писание, семейный альбом и паспорт торговой марки. Задача брендбука от M-Stream – задать верный и продуктивный курс всем элементам вашего фирменного стиля. Обеспечить их работу в синергии и на результат.","image":"/assets/images/branding/rows/brandbook.jpg","imagePosition":"right"},{"id":3,"label":"Launched to market","title":"Логотип","text":"Сегодня каждый третий легко отличит гербы домов из “Игры престолов”. Каждый из них моментально рождает ассоциации, а вслед за ними и характерные эмоции. Логотип - высший пилотаж связи с потребителем. Причем и для непосредственного продукта, и для создателей. Если вдуматься, миссия вложить легенду, корпоративные ценности и концепцию бренда в лого кажется невыполнимой. Принимая вызов, M-Stream реализует яркое и действенное позиционирование бренда, глубокую смысловую подоплеку и символичность – три главных составляющих успеха подлинного логотипа.","image":"/assets/images/branding/rows/logo.jpg","imagePosition":"left"},{"id":4,"label":"Launched to market","title":"Ребрендинг","text":"Признанным чемпионом по ребрендингу значится Coca-Cola. Каждые 10 лет это выражается в пересмотре оформления логотипа, формы тары и прочей айдентики. Но без радикальщины. И что мы видим? Бренд признан самым дорогим в мире – это раз, 94% жителей планеты хоть однажды слышали о торговой марке – это два, каждую секунду выпивают около 8000 стаканов Колы – это три. Впечатляет, не так ли? Ребрендинг – новая успешная жизнь вашего бизнес-лица. Создавайте его с M-Stream  – мы знаем как удержать и повышать актуальность бренда.","image":"/assets/images/branding/rows/rebranding.jpg","imagePosition":"right"}]},{"slug":"/marketing","color":"#006193","title":"Digital Marketing","text":"Необходимы эффективные решения в рекламе продукта среди растущей интернет-аудитории? Вы обратились по адресу.","servicesMenuImage":"/assets/images/marketing/marketing-service-menu.jpg","menuLabel":"Who We Are","pagePosition":"right","useInMenu":true,"rows":[{"id":5,"label":"Launched to market","title":"SEO","text":"Иногда это сложно понять. Ваш продукт или услуга качественнее, обходит поделки конкурентов по всем статьям, но бизнес будто уперся в стену. Просто потенциальный клиент о нем пока не знает. Помогите ему найти именно то, что он хочет, ищет среди тысяч предложений – ваше уникальное решение. Команда M-Stream эффективно применяет уникальные и классические стратегии поискового продвижения, чтобы спрос встретил достойное предложение – именно ваше.","image":"/assets/images/marketing/rows/seo.jpg","imagePosition":"left"},{"id":6,"label":"Launched to market","title":"SMM","text":"Из всей активности онлайн современный человек уделяет около 30% соцсетям. И сейчас, когды вы читаете это, величина растет с настораживающей скоростью. Дальновидные предприниматели активно используют SMM, монополизируя целые ниши. Не пора ли использовать социальные платформы на полную мощность? Подразделение SMM компании M-Stream использует хирургически точные тактики, что обеспечивает быстрое и непосредственное преобразования пользователя в потребителя вашего продукта.","image":"/assets/images/marketing/rows/smm.jpg","imagePosition":"right"},{"id":7,"label":"Launched to market","title":"SERM","text":"Успех на рынке  – прежде всего уникальный качественный товар. Но не все готовы играть по правилам. Конкуренты кладут деньги, силы и здоровье, чтобы за неимением достойного продукта нанести репутационные потери вашему. SERM-профи M-Stream владеют результативными техниками как устранения вредоносных происков конкурентов, так и профилактических приемов репутационного менеджмента. Застрахуйте честное имя вашего продукта – это бесценный капитал.","image":"/assets/images/marketing/rows/serm.jpg","imagePosition":"left"},{"id":8,"label":"Launched to market","title":"Контекстная реклама","text":"Итак, вы вложили немалые средства в продвижении продукта, а результат не оправдал ожидания. Как же повысить конверсию, не опустошив рекламный бюджет? Отвечают специалисты по контекстной рекламе агентства M-Stream. Во-первых, это один из наиболее экономически рациональных видов продвижения. Во-вторых, контекстная реклама работает точно на вашу целевую аудиторию и оплачивается по факту вовлечения. Наконец, это кросс-платформенный маневр, эффективный везде, где предусмотрена строка поиска.","image":"/assets/images/marketing/rows/context.jpg","imagePosition":"right"}]},{"slug":"/development","color":"#ffcc02","title":"Web development","text":"Продукт актуального и качественного веб-программирования – это Альфа и Омега прогрессирующего бизнеса. Но сайт не только повышает доход и развивает ваше дело. Есть у него еще одна функция, о которой мало кто говорит. Им можно гордиться. Хотите и эту бонусную опцию? Веб-студия M-Stream предлагает ее в стандартном пакете.","servicesMenuImage":"/assets/images/development/development-service-menu.jpg","menuLabel":"Who We Are","pagePosition":"left","useInMenu":true,"rows":[{"id":9,"label":"Launched to market","title":"Landing page","text":"Стремитесь продать благородные стейки вегетарианцам? Необходимо сбыть элитный алкоголь обществу борьбы за трезвость, а смартфон на ОС Android идейному яблочнику? Даже для таких нетривиальных задач есть решение – посадочная страница. Это флагманская боевая единица успешных продаж. Веб-студия M-Stream создает как классические лендинги, так и уникальные проекты, согласно нетиповым пожеланиям партнера. Используйте оптимальный альянс визуальной коммуникации и функционала, таким образом уникальные преимущества вашего продукта превратят посетителя в покупателя.","image":"/assets/images/development/rows/landing.jpg","imagePosition":"left"},{"id":10,"label":"Launched to market","title":"Корпоративные сайты","text":"Современные реалии непреклонны: присутствие в бизнесе определяется его наличием в интернете. Успешной, перспективной компании, если в ее планы входит дальнейшее развитие, необходим качественный корпоративный сайт. Причем акцент делается именно на кондиции ресурса. Его наличие не обсуждается. Эргономичная, информативная и стильная резиденция вашего дела в сети – это опора и один из ключевых козырей прогресса. И здесь знают толк в создании таких проектов. Создавайте, укрепляйте и совершенствуйте корпоративный сайт с M-Stream. Ведь это  исходная платформа растущего бизнеса.","image":"/assets/images/development/rows/corporate.jpg","imagePosition":"right"},{"id":11,"label":"Launched to market","title":"Высоконагруженные проекты","text":"Создание масштабного, многоуровневого сервиса или портала – дело не просто сложное, а очень сложное. Главное и зачастую неразрешимое затруднение – конфликт требований маркетолога и технической реализации. Феншуй в данном случае сродни краснокнижной редкости. Команда M-Stream создает высоконагруженные проекты только в условиях продуктивной диффузии всех отделов и на полной дистанции. Сотрудники агентства вовлечены в процессы друг друга, оптимально сочетая функциональную ценность и маркетинговую эффективность.  Доверьте разработку самых амбициозных проектов веб-студии M-Stream, ведь наши цели совпадают.","image":"/assets/images/development/rows/high-load.jpg","imagePosition":"left"},{"id":12,"label":"Launched to market","title":"Web-приложения","text":"Известно ли вам, что создание веб-приложения – задача гораздо более щепетильная и сложная, чем сваять целый сайт? Корректная архитектура, надежное взаимодействие элементов и согласованность процессов одновременно – напрямую определяют эффективный функционал конечного продукта. Добавляют тонуса и индивидуальные пожелания партнера. Веб-студия M-Stream располагает инструментарием для разработки как типовых веб-приложений, так и совершенно специфических, с тонкими настройками под цели и задачи целевого проекта. Создавайте эффективные инструменты ведения бизнеса вместе с M-Stream – будьте всегда в тренде.","image":"/assets/images/development/rows/applications.jpg","imagePosition":"right"}]},{"slug":"/portfolio","color":"#00cf00","title":"Portfolio","text":"Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона.","servicesMenuImage":"/assets/images/portfolio/portfolio-service-menu.jpg","menuLabel":"Who We Are","pagePosition":"top","useInMenu":true,"rows":[{"id":13,"label":"Launched to market","title":"Lead ocean","text":"CPI-сети – один из популярных видов бизнеса в интернете. Средства его продвижения давно использованы. Но отступать было некуда - позади репутация и контракт. Напряженный мозговой штурм привел к простому и верному решению. Родилась “астрономическая концепция”. Космос – это бесконечность. В первую очередь – перспектив.","image":"/assets/images/portfolio/rows/lead-ocean.jpg","imagePosition":"right","link":"http://leadOcean.com/"},{"id":14,"label":"Launched to market","title":"Адвокатское Бюро","text":"Існує багато варіацій уривків з Lorem Ipsum, але більшість з них зазнала певних змін на кшталт жартівливих вставок або змішування слів, які навіть не виглядають правдоподібно. Якщо ви збираєтесь більшість з них зазнала певних змін вставок.","image":"/assets/images/portfolio/rows/voloshin.jpg","imagePosition":"left","link":"http://voloshin.com/"},{"id":15,"label":"Launched to market","title":"Elma dental","text":"Існує багато варіацій уривків з Lorem Ipsum, але більшість з них зазнала певних змін на кшталт жартівливих вставок або змішування слів, які навіть не виглядають правдоподібно. Якщо ви збираєтесь використовувати Lorem Ipsum, ви маєте упевнитись в тому, що всередині тексту не приховано нічого, що","image":"/assets/images/portfolio/rows/elma.jpg","imagePosition":"right","link":"http://elma.com/"},{"id":16,"label":"Launched to market","title":"English family","text":"Існує багато варіацій уривків з Lorem Ipsum, але більшість з них зазнала певних змін на кшталт жартівливих вставок або змішування слів, які навіть не виглядають правдоподібно. Якщо ви збираєтесь використовувати Lorem Ipsum, ви маєте упевнитись в тому, що всередині тексту не приховано нічого, що","image":"/assets/images/portfolio/rows/english-family.jpg","imagePosition":"left","link":"http://englishfamily.com/"},{"id":17,"label":"Launched to market","title":"Greedy Gamers","text":"Існує багато варіацій уривків з Lorem Ipsum, але більшість з них зазнала певних змін на кшталт жартівливих вставок або змішування слів, які навіть не виглядають правдоподібно. Якщо ви збираєтесь використовувати Lorem Ipsum, ви маєте упевнитись в тому, що всередині тексту не приховано нічого, що","image":"/assets/images/portfolio/rows/greedy-gamers.jpg","imagePosition":"right","link":"http://greedygamers.com/"},{"id":18,"label":"Launched to market","title":"Away","text":"Існує багато варіацій уривків з Lorem Ipsum, але більшість з них зазнала певних змін на кшталт жартівливих вставок або змішування слів, які навіть не виглядають правдоподібно. Якщо ви збираєтесь використовувати Lorem Ipsum, ви маєте упевнитись в тому, що всередині тексту не приховано нічого, що","image":"/assets/images/portfolio/rows/away.jpg","imagePosition":"left","link":"http://away.com/"},{"id":19,"label":"Launched to market","title":"АС - Щит","text":"Існує багато варіацій уривків з Lorem Ipsum, але більшість з них зазнала певних змін на кшталт жартівливих вставок або змішування слів, які навіть не виглядають правдоподібно. Якщо ви збираєтесь використовувати Lorem Ipsum, ви маєте упевнитись в тому, що всередині тексту не приховано нічого, що","image":"/assets/images/portfolio/rows/shield.jpg","imagePosition":"right","link":"http://shield.com/"}]}]

/***/ })
/******/ ]);